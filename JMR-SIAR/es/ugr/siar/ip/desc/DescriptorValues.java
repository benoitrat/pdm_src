/**
 * 
 */
package es.ugr.siar.ip.desc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.ugr.siar.db.MySQL;
import es.ugr.siar.tools.IndexedGeneric;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 17 janv. 08
 *
 */
public class DescriptorValues {
	public enum DescriptorType{
		I_M7SCD,I_M7CSD,I_M7DCD,I_M7EHD,I_M7HTD,C_EXF;
		public int getVisualDescType() {
			switch(this) {
			case I_M7CSD: return VisualDescriptor.DESC_MPEG7_CSD;
			case I_M7SCD: return VisualDescriptor.DESC_MPEG7_SCD;
			case I_M7EHD: return VisualDescriptor.DESC_MPEG7_EHD;
			case I_M7DCD: return VisualDescriptor.DESC_MPEG7_DOM;
			case I_M7HTD: return VisualDescriptor.DESC_MPEG7_HTD;
			default: 	  return VisualDescriptor.TYPE_CUSTOM;
			}
		}
		public String getAcronyms(int nb) {
			if(nb==3) {
				switch(this) {
				case I_M7CSD: return "CSD";
				case I_M7SCD: return "SCD";
				case I_M7EHD: return "EHD";
				case I_M7DCD: return "DOM";
				case I_M7HTD: return "HTD";
				case C_EXF	: return "EXF";
				}
			}
			return "";
		}

		public static int getIndex(DescriptorType descType, DescriptorType[] descTypes) {
			//Get the Descriptor Index
			int descInd=-1;

			for(int i=0;i<descTypes.length;i++) {
				if(descType == descTypes[i]) 
					descInd = i;
			}
			if(descInd==-1) System.err.println("This descriptor is not setted in the dataManager");
			return descInd;
		}

	};

	protected DescriptorType type;
	protected Integer photoID;
	protected Map<String,Double> params;
	protected List<IndexedGeneric> values;

	public DescriptorValues() {
		params= new HashMap<String, Double>();
		values= new ArrayList<IndexedGeneric>();
	}


	public Integer getPhotoID() {
		return photoID;
	}
	public DescriptorType getType() {
		return type;
	}
	public Map<String, Double> getParams() {
		return params;
	}

	public void setPhotoID(Integer photoID) {
		this.photoID = photoID;
	}
	public void setType(DescriptorType type) {
		this.type = type;
	}

	public void addParam(String key,int val) {
		params.put(key, new Double(val));
	}

	public void addParam(String key,float val) {
		params.put(key, new Double(val));
	}

	public void addParam(String key,double val) {
		params.put(key, new Double(val));
	}

	public void addValue(String key,int val) {
		values.add(new IndexedGeneric<Integer>(Integer.TYPE.hashCode(),key,new Integer(val)));
	}

	public void addValue(String key,float val) {
		values.add(new IndexedGeneric<Float>(Float.TYPE.hashCode(),key,new Float(val)));
	}

	public void addValue(String key,double val) {
		values.add(new IndexedGeneric<Double>(Double.TYPE.hashCode(),key,new Double(val)));
	}

	public Double[] getValuesInDouble() {
		Double[] rv = new Double[values.size()];
		for(int i=0;i<values.size();i++) {
			Object o = values.get(i).getGenericObject();
			if(o instanceof Double)
				rv[i] = (Double)o;
			else if(o instanceof Integer) 
				rv[i] = ((Integer)o).doubleValue();
			else if(o instanceof Float) 
				rv[i] = ((Float)o).doubleValue();
		}
		return rv;
	}

	public String[] getValuesHeader() {
		String[] rv = new String[values.size()];
		for(int i=0;i<values.size();i++) {
			rv[i] = values.get(i).toString();
		}
		return rv;
	}

	public int getNofAttributes() {
		return values.size();
	}

	public static DescriptorValues setDescValuesFromMySQL(MySQL db,int photoID,DescriptorType descType) {

		switch(descType) {
		case I_M7CSD:
		case I_M7EHD:
		case I_M7SCD:
		case I_M7HTD:
			VisualDescriptor vDesc = VisualDescriptor.getInstance(descType.getVisualDescType());
			if(vDesc.exist(photoID, db, true)) {
				vDesc.fromMySQL(photoID, db);
				return vDesc.getValues();
			}
			return null;
		case C_EXF:
			ExifDescriptor cDesc = new ExifDescriptor();
			if(cDesc.fromMySQL(photoID, db)) {
				return cDesc.getValues();
			}
			return null;
		default:
			return null;

		}
	}
}
