/**
 * 
 */
package es.ugr.siar.ip.desc;

import java.awt.color.ColorSpace;

import es.ugr.siar.ip.ColorConvertTools;
import es.ugr.siar.ip.ImageJMR;
import es.ugr.siar.ip.colspace.ColorSpaceJMR;

/**
 * The abstract <code>superclass</code> that manipulate {@link ImageJMR} as input for descriptor using color.
 * 
 * <p>
 * Using this class as <code>superclass</code> permit to check if the {@link ColorSpace} or
 * {@link ColorSpaceJMR} of the input image is correct using the {@link #checkColorSpace(ColorSpace)}.
 * If it is not this class implement method such {@link #convertImg(ImageJMR)} to obtain an input image
 * in the correct format for our descriptor.
 * </p>
 * 
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @since 1.0, 29 nov. 07
 *
 */
public abstract class ColorDescriptor extends VisualDescriptor {

	/** The color space type that the <code>subclass</code> descriptor need as 
	 * input for the {@link #extract(ImageJMR)} method */
	protected int colorSpaceType;

	/**
	 * The constructor.
	 * 
	 * @see VisualDescriptor#VisualDescriptor(int, String, float)
	 * @param colorSpaceType	look at {@link #colorSpaceType}
	 */
	protected ColorDescriptor(int type, String name, float version, int colorSpaceType) {
		super(type, name, version);
		this.colorSpaceType = colorSpaceType;
	}


	/**
	 * Check if the {@link ColorSpace} of the input image is correct for the <code>subclass</code> descriptor.
	 * 
	 * <p>In the case that more than one {@link ColorSpace} is acceptable for the 
	 * subclass descriptor, this method should be override in the <code>subclass</code>.
	 * </p> 
	 * 
	 * @param	 cS 	The ColorSpace that is used as input image.
	 * @return			True if we use the correct ColorSpace.
	 */
	protected boolean checkColorSpace(ColorSpace cS) {
		return (cS.getType() == colorSpaceType);
	}


	/* (non-Javadoc)
	 * @see es.ugr.siar.ip.desc.VisualDescriptor#convertImg(es.ugr.siar.ip.ImageJMR)
	 */
	protected ImageJMR convertImg(ImageJMR imSrc) {
		return ColorConvertTools.colorConvertOp(imSrc,ColorSpaceJMR.getInstance(colorSpaceType));
	}

}
