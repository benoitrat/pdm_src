package es.ugr.siar.ip.desc;

/**
 * Class used by {@link MPEG7HomogeneousTexture}.
 * 
 * <p>
 * This class maybe used to describe a block in {@link MPEG7HomogeneousTexture}. 
 * A block is the intersection of a Sector (Orientation=i) and a Track (Scale=j)
 * </p> 
 * 
 * <p>
 * <p>Title: Busca Imegenes</p>
 * <p>Description: Aplicacion de busqueda de imegenes basada en MPEG-7</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * </p>
 * @author Jose Manuel Soto Hidalgo
 * @version 1.0
 */

public class Sensor {
  float o_sup;
  float o_inf;
  float s_sup;
  float s_inf;
  int nPtos;
  float peso;
  double media;
  double desv;

  public Sensor() {
    o_sup = 0;
    o_inf = 0;
    s_sup = 0;
    s_inf = 0;
    nPtos = 0;
    peso = 0;
    media = 0;
    desv = 0;
  }
}

