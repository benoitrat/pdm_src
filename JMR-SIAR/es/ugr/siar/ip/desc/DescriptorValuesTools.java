/**
 * 
 */
package es.ugr.siar.ip.desc;

import weka.core.Attribute;
import weka.core.FastVector;

/**
 * @author  RAT Benoît <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 5 feb. 08
 *
 */
public class DescriptorValuesTools {
	
	
	public static FastVector getFVInstance(DescriptorValues dV) {
		FastVector atts = new FastVector();
		String[] attrHeader = dV.getValuesHeader();
		
		switch(dV.getType()) {
		case I_M7CSD:
		case I_M7EHD:
		case I_M7SCD:
			for(int i=0;i<dV.getNofAttributes();i++) {
				atts.addElement(new Attribute(attrHeader[i]));
			}
			break;
		case C_EXF:
			ExifDescriptor eDesc = new ExifDescriptor();
			String[] nominals=null;
			// Setup attribute using the descriptor values attribute headers.
			for(int j=0;j<dV.getNofAttributes();j++) {
				nominals=eDesc.getNominals(j);
				if(nominals==null)
					atts.addElement(new Attribute(attrHeader[j]));
				else {
					//Building nominals
					FastVector nominalAttr = new FastVector();
					for(int k=0;k<nominals.length;k++) {
						nominalAttr.addElement(nominals[k]);
					}
					atts.addElement(new Attribute(attrHeader[j],nominalAttr));
				}
			}
			break;
		}
		return atts;
	}
	
	
	//TODO: Create a method that only add at the end of one 

}
