/**
 * 
 */
package es.ugr.siar.ip.desc;

import java.io.Serializable;

import es.ugr.siar.db.MySQL;
import es.ugr.siar.ip.ImageJMR;
import es.ugr.siar.ip.colspace.ColorSpaceJMR;

/**
 * The abstract <code>superclass</code> that permit to manipulate descriptors 
 * with {@link ImageJMR} as input.
 * 
 * 
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @since 25 nov. 07
 *
 */
public abstract class VisualDescriptor implements Serializable, Descriptor2MySQL {
	
	/** Tag for custom descriptor */
	public static final int TYPE_CUSTOM = -1;
	
	/** [Texture] Tag for Homegeneous Texture Descriptors */
	public static final int DESC_MPEG7_HTD = 0;
	/** [Texture] Tag for Edge Histograms Descriptors */
	public static final int DESC_MPEG7_EHD = 1;
	/** [Color] Tag for MPEG7 Scalable color descriptor */
	public static final int DESC_MPEG7_SCD = 2; 
	/**	[Color] Tag for MPEG7 Color Structure Descriptor */
	public static final int DESC_MPEG7_CSD = 3; 
	/**	[Color] Tag for MPEG7 Dominant Color Descriptor */
	public static final int DESC_MPEG7_DOM = 4; 
	/** [Color] Tag for MPEG7 Color Layout Descriptors */
	public static final int DESC_MPEG7_CL = 5;
	/** [Color] Tag for Moment descriptors */
	public static final int TYPE_COL_MOMENT = 7;
	/** [Color] Tag for Histograms descriptors */
	public static final int TYPE_COL_HISTO = 8;
	/** [Texture] Tag for Moment descriptors */
	public static final int TYPE_TEX_MOMENT = 9;
	
	/** An array that show the descriptor with correct implementation */
	public static final boolean [] IMPLEMENTED_DESC = {false,true,true,true};

	/** The number of descriptor actually implemented */
	public static int NOF_DESCRIPTORS=4;
	
	
	/** Type set by Tag Descriptor */
	protected int type;
	
	/** Name of the descriptor */
	protected String name;
	
	/** 
	 * Version of the descriptor:
	 * If the compute method is recall and version is newer the FeatureVec is override 
	 */
	protected float version;
	
	
	protected VisualDescriptor(int type, String name, float version) {
		this.type=type;
		this.name=name;
		this.version=version;
	}
	
	
	public static VisualDescriptor getInstance(int typeDesc) {
		switch(typeDesc) {
//		case TYPE_COL_MOMENT:
//			return new DescriptorXXX(); 
//		case TYPE_COL_HISTO:
//			return new DescriptorXXX(); 
		case DESC_MPEG7_SCD:
			return new MPEG7ScalableColor(); 
		case DESC_MPEG7_CSD:
			return new MPEG7ColorStructure();
//		case DESC_MPEG7_DOM:
//			return new DescriptorXXX();  
//		case DESC_MPEG7_CL:
//			return new DescriptorXXX(); 
//		case TYPE_TEX_MOMENT: 
//			return new DescriptorXXX(); 
//		case DESC_MPEG7_HTD:
//			return new DescriptorXXX(); 
		case DESC_MPEG7_EHD:
			return new MPEG7EdgeHistogram(); 
		case TYPE_CUSTOM: 
		default:
			return null;
		}
	}
	
	public static VisualDescriptor getInstance(int typeDesc,ImageJMR im) {
		switch(typeDesc) {
//		case TYPE_COL_MOMENT:
//			return new DescriptorXXX(); 
//		case TYPE_COL_HISTO:
//			return new DescriptorXXX(); 
		case DESC_MPEG7_SCD:
			return new MPEG7ScalableColor(im); 
		case DESC_MPEG7_CSD:
			return new MPEG7ColorStructure(im);
//		case DESC_MPEG7_DOM:
//			return new DescriptorXXX();  
//		case DESC_MPEG7_CL:
//			return new DescriptorXXX(); 
//		case TYPE_TEX_MOMENT: 
//			return new DescriptorXXX(); 
//		case DESC_MPEG7_HTD:
//			return new DescriptorXXX(); 
		case DESC_MPEG7_EHD:
			return new MPEG7EdgeHistogram(im); 
		case TYPE_CUSTOM: 
		default:
			return null;
		}
	} 
	

	/**
	 * Extract the descriptor feature vector from an image
	 *  
	 * @param 	im 		a {@link ImageJMR} with the correct {@link ColorSpaceJMR} for this {@link VisualDescriptor}   
	 */
	public abstract void extract(ImageJMR im);
//	TODO think about the name of the extract()/compute()/calculated()
	
	
	/**
	 * Compare the value between two {@link VisualDescriptor} and
	 * return a similarity result not normalized.
	 *  
	 * @param desc Another Visual Descripto of the same type
	 * @return the similarity measure between to FeatureVec
	 */
	public abstract float compare(VisualDescriptor desc);
	
	
	/**
	 * Sum a {@link VisualDescriptor} give in parameters another one.
	 * 
	 * <p>
	 * This method can be used to accumulate feature vectors and if we combined
	 * it with {@link VisualDescriptor#multiply(float)} we can obtain a {@link VisualDescriptor} 
	 * model to define an image.
	 * </p>
	 * 
	 * @param desc A visual descriptor of the same type.
	 */
	public abstract void sum(VisualDescriptor desc);
	
	
	/**
	 * This method multiply all values in the feature vector by a factor.
	 * 
	 * @param factor	the factor that multiply all values in the feature vector. 
	 */
	public abstract void multiply(float factor);
		
	
	
	/**
	 * This method only check if the image is in the appropriate format
	 * for extracting the feature vector of the descriptors.
	 *   
	 * @param 	im 	An image with its associated ColorSpace and SampleModel
	 * @return 	the condition if its ColorSpace and SampleModel are appropriate for this descriptor.
	 */
	protected abstract boolean checkImage(ImageJMR im);
	
	
	/**
	 * Convert all type of {@link ImageJMR} in the correct format for the descriptor.
	 * 
	 * <p>This method need to be implemented (override) in the <code>subclass</code> to convert all 
	 * type of source image in the format that the descriptor need.</p>
	 * 
	 * @param 	imSrc	the source {@link ImageJMR} in wrong format or ColorSpace
	 * @return			the correct {@link ImageJMR} for the descriptor.
	 */
	protected abstract ImageJMR convertImg(ImageJMR imSrc);	
	
	/**
	 * It permit to overwrite previous values computed with this algorithm if it has changed
	 * @return the ID  number has a float with the version of the algorithm used.
	 */
	public float getVersion() {
		return this.version;
	}
	
	public static String getSmallName(int typeDesc) {
		switch(typeDesc) {
		case TYPE_COL_MOMENT: 	return "TYPE_COL_MOMENT";
		case TYPE_COL_HISTO: 	return "TYPE_COL_HISTO";
		case DESC_MPEG7_SCD: 	return "MPEG7 SCD";
		case DESC_MPEG7_CSD:	return "MPEG7 CSD";
		case DESC_MPEG7_DOM:	return "MPEG7 DOM";
		case DESC_MPEG7_CL: 	return "MPEG7 CL";
		case TYPE_TEX_MOMENT: 	return "TYPE TEX MOMENT"; 
		case DESC_MPEG7_HTD: 	return "MPEG7 HTD";
		case DESC_MPEG7_EHD: 	return "MPEG7 EHD";
		case TYPE_CUSTOM:  
		default:
			return "TYPE_CUSTOM";
		}
	}
	
	public static String getAcronyms(int descType) {
		switch(descType) {
		case TYPE_COL_MOMENT: 	return "TCM";
		case TYPE_COL_HISTO: 	return "TCH";
		case DESC_MPEG7_SCD: 	return "SCD";
		case DESC_MPEG7_CSD:	return "CSD";
		case DESC_MPEG7_DOM:	return "DCD";
		case DESC_MPEG7_CL: 	return "CLD";
		case TYPE_TEX_MOMENT: 	return "TTM"; 
		case DESC_MPEG7_HTD: 	return "HTD";
		case DESC_MPEG7_EHD: 	return "EHD";
		case TYPE_CUSTOM:  
		default:
			return "TYPE_CUSTOM";
		}
	}
	public String getName() {
		return name;
	}
	
	public int getType() {
		return type;
	}
	
	
//	TODO think about the name of the extract()/compute()/calculated()
	public abstract boolean isComputed();
	
	public abstract DescriptorValues getValues();
	
	
	//------------------------ SQL ----------------------
	
	
	public boolean exist(int ID, MySQL db, boolean checkVersion) {
		return exist(ID,db,checkVersion,false);
	}
	
	
	
	/* (non-Javadoc)
	 * @see es.ugr.siar.ip.desc.Descriptor2MySQL#exist(int, es.ugr.siar.db.MySQL, boolean)
	 */
	public boolean exist(int ID, MySQL db, boolean checkVersion, boolean message) {
		boolean isOK = false;
		String sql = "SELECT `Photo_ID`, `Version` FROM "+getTableName()+" ";
		sql += "WHERE `Photo_ID` ="+ID;
		if(db.queryOneRowResult(sql)) {
			//Useless check but we do it...
			if(ID == db.getValueInt("Photo_ID")){
				//Check also the version of this descriptors.
				if(!checkVersion) isOK=true;
				else if(version <=  (float)db.getValueDouble("Version")) {
					isOK = true;
					if(message) System.out.println(ID+" exist in database "+db.toString());
				}
			}
		}
		//Close the result set for the next query.
		db.closeResultSet();
		return isOK;
	}
	
	/* (non-Javadoc)
	 * @see es.ugr.siar.ip.desc.Descriptor2MySQL#replaceHeader()
	 */
	public String replaceHeader() {
		String sql1="", sql2="", coma="";
		String [] paramName = getSQLParamNames(); 
		for(int i=0;i<paramName.length;i++) {
			if(i != 0) coma=",";
			sql1 += coma+"`"+paramName[i]+"`";
			sql2+= coma+"?";
		}
		return "REPLACE INTO "+getTableName()+" ("+sql1+") VALUES ("+sql2+");";
	}
	
	public String[] toStrVec() {
		return toStrVec(false);
	}
	abstract public String[] toStrVec(boolean header);
		
}
