/**
 * 
 */
package es.ugr.siar.ip.desc;

import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;

import es.ugr.siar.ip.ColorConvertTools;
import es.ugr.siar.ip.ImageJMR;
import es.ugr.siar.ip.colspace.ColorSpaceJMR;

/**
 * The abstract <code>superclass</code> that manipulate {@link ImageJMR} as input for descriptor using texture.
 * 
 * <p>
 * Using this class as super class permit to check if the {@link ColorSpace} or
 * {@link ColorSpaceJMR} of the input image is correct using the {@link #checkColorSpace(ColorSpace)}.
 * If it is not this class implement method such {@link #convertImg(ImageJMR)} to obtain an input image
 * in the correct format for our descriptor.
 * </p>
 *    
 * 
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @since 1.0, 29 nov. 07
 * @see ColorDescriptor which have equivalent method.
 *
 */
public abstract class TextureDescriptor extends VisualDescriptor {

	/** 
	 * This boolean let us know if the descriptor can used only image with one intensity channel
	 * or image that have 3 channels knowing that the first one represent the intensity.     
	 * */
	boolean useOneChannel;

	/**
	 * The constructor.
	 * 
	 * @see VisualDescriptor#VisualDescriptor(int, String, float)
	 * @param useOneChannel	look at {@link #useOneChannel}.
	 */
	protected TextureDescriptor(int type, String name, float version, boolean useOneChannel) {
		super(type, name, version);
		this.useOneChannel = useOneChannel;
	}


	/**
	 * Check the colorSpace of the input image.
	 * 
	 * <p>
	 * The color space must be {@link ColorSpace#CS_GRAY} if we need to use only one channel.
	 * or it can be for example {@link ColorSpaceJMR#CS_YCbCr} that have intensity value on the first channel.
	 * <br/>( {@link ColorSpaceJMR#CS_Lab} & {@link ColorSpaceJMR#CS_Luv} need to be check 
	 * before their implementing them.
	 * </p>
	 * 
	 * 
	 * @param 	cS 	The ColorSpace used	
	 * @return		A true value if the input image use a correct color Space.
	 */
	protected boolean checkColorSpace(ColorSpace cS) {
		if(useOneChannel)
			return (cS.getType() == ColorSpaceJMR.CS_GRAY);
		else
			return (cS.getType() == ColorSpaceJMR.CS_YCbCr);
	}


	/** 
	 * Convert Image in {@link ColorSpaceJMR#CS_YCbCr} if they are in RGB format.
	 * 
	 * <p>
	 * This method can be improve to deal with different input image color space:<ul>
	 * <li>HSI : Take only the I (intensity) channel and return a {@link BufferedImage#TYPE_BYTE_GRAY} image.</li>
	 * <li>HMMD: Use the D (diff) channel and return {@link BufferedImage#TYPE_BYTE_GRAY} image</li>
	 * <li>CIELab,CIELuv: Don't transform them and deal with the L (luminace) channel</li>
	 * <li>...</li></ul>
	 * </p> 
	 * 
	 * @see es.ugr.siar.ip.desc.VisualDescriptor#convertImg(es.ugr.siar.ip.ImageJMR)
	 */
	protected ImageJMR convertImg(ImageJMR imSrc) {
		ImageJMR dst = ColorConvertTools.colorConvertOp(imSrc,ColorSpaceJMR.getInstance(ColorSpaceJMR.CS_YCbCr));
		return new ImageJMR(dst.getLayeredByteImages()[0]);		
	}

}
