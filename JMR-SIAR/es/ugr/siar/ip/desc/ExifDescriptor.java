/**
 * 
 */
package es.ugr.siar.ip.desc;

import java.sql.ResultSet;
import es.ugr.siar.db.MySQL;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 11 janv. 08
 *
 */
public class ExifDescriptor implements Descriptor2MySQL {

	public static enum ExifParam {
		CM,SW,DoY,MoY,HoD,FiB,FF,FR,FM,FFu,ReM,EpT,EpM,EpP,WB,FN,ISO,FL,
		SD,SDR,AP,SS,CAP,CSS,MM,SCT,LS;

		public String toString() {
			switch(this) {
			case CM : return "CamModel";
			case SW : return "Software";
			
			case DoY: return "DayOfYear";
			case MoY: return "MonthOfYear";
			case HoD: return "HourOfDay";
			
			case FiB: return "FlashInByte";
			case FF	: return "FlashFired";		//BinaryMode
			case FR : return "FlashReturn";		//NominalMode
			case FM : return "FlashMode";		//NominalMode
			case FFu: return "FlashFunction";	//BinaryMode
			case ReM: return "RedEyesMode";		//BinaryMode
			
			case EpT: return "ExpTime";
			case EpM: return "ExpMode"; 	//NominalMode (Not reallyUseFull)
			case EpP: return "ExpProgra"; //NominalMode
			case CSS: return "ComputedShutterSpeed";
			case SS	: return "ShutterSpeed";
			
			case FN : return "F-Number";
			case AP : return "Aperture";
			case CAP: return "ComputedAperture";
			
			case WB : return "WhiteBalance"; 	//BinaryMode
			case LS	: return "LightSource";		//NominalMode
			case ISO: return "ISOSpeed";
			case MM	: return "MeteringMode";     //NominalMode
			
			case FL : return "FocalLength";		//Not really usefull
			case SD : return "SubjectDistance";	//Not usefull at all
			case SDR: return "SubDistRange";  //NominalMode
			case SCT: return "SceneCapType"; //NominalMode

			
			default : return "Unkown"; 
			}
		}

		public String[] getNominals() {
			String[] rv=null;
			switch(this) {
			case FF:
				rv=new String[]{"NotFired","Fired"};
				break;
			case FR:
				rv=new String[]{"NoStrobeReturn","Reserved","StrobeReturn","StrobeReturnLighDetected"};
				break;
			case FM:
				rv=new String[]{"Unknown","On","Off","Auto"};
				break;
			case ReM:
				rv=new String[]{"Off","On"};
				break;
			case LS:
				rv=new String[]{"Unknown","Daylight","Fluorescent","Tungsten","Flash",
						"FineWeather","CloudyWeather","Shade",
						"D5700-7100K","N4600-400K","W3900-4500K","WW3200-3700K",
						"A","B","C","D65","D75","D50","ISO_StudioTungsten"};
				break;
			case EpP:
				rv=new String[] {"NotDefined","Manual","Normal","AperturePriority","ShutterPriority",
						"Creative","Action","Portrait","Landscape"};
//				5 = Creative program (biased toward depth of field)
//				6 = Action program (biased toward fast shutter speed)
//				7 = Portrait mode (for closeup photos with the background out of focus)
//				8 = Landscape mode (for landscape photos with the background in focus)
				break;
			case EpM:
				rv=new String[]{"Auto","Manual","AutoBracket"};
				break;
			case WB:
				rv=new String[]{"Auto","Manual"};
				break;
			case MM:
				rv=new String[]{"Unknown","Average","CenterWeightedAverage",
						"Spot","MultiSpot","Pattern","Partial"};
				break;
			case SDR:
				rv=new String[]{"Unknown","Macro","CloseView","DistantView"};
				break;
			case SCT:
				rv=new String[]{"Standard","Landscape","Portrait","NightScene"};
				break;
			}	
			return rv;
		}
		
		public String getNominal(int id) {
			return this.getNominals()[id];
		}

	};

	Double[] paramsData;
	ExifParam[] paramsUsed;


	public ExifDescriptor() {

		paramsUsed=new ExifParam[]{ExifParam.MoY,ExifParam.HoD,	//Temporal Tags
				ExifParam.FF,ExifParam.FM,ExifParam.ReM,		//Flash tags
				ExifParam.CSS,ExifParam.EpM,ExifParam.EpP, 		//Exposure tags
				ExifParam.CAP,ExifParam.MM,						//Brightness1
				ExifParam.ISO,ExifParam.WB,ExifParam.LS, 		//Brigthness2
				ExifParam.FL,ExifParam.SDR,ExifParam.SCT}; 		//SceneType
	}

	private String param2MySQL(ExifParam ep) {
		String txt="";
		switch(ep) {
		case DoY: txt=" DAYOFYEAR( DateTime ) "; 
		break;
		case MoY: txt=" DAYOFYEAR( DateTime ) /31 "; 
		break;
		case HoD: txt=" TIME_TO_SEC( TIME( DateTime ) ) /3600 "; 
		break;
		case FiB: txt=" FlashOn "; 
		break;
		case FF	: txt=" FlashOn & 0x01 "; 
		break;
		case FR : txt=" FlashOn >> 1 & 0x03 ";
		break;
		case FM : txt=" FlashOn >> 3 & 0x03 ";
		break;
		case FFu: txt=" FlashOn >> 5 & 0x01 ";	
		break;
		case ReM: txt=" FlashOn >> 6 & 0x01 "; 
		break;
		case CAP: txt=" TRUNCATE(2 * LOG2( Fnumber ),3) ";
		break;
		case CSS: txt=" TRUNCATE(-LOG2( ExpTime ),3) "; 
		break;
		//Value that are the same than the MySQL table
		case ISO:	
		case FL :
		case MM:
		case WB:
		case EpP:
		case EpM:
		case LS:	
		case SDR:
		case SCT:
			txt=""; 
			break;
			//Value that are not used in the MySQL table
		default:
			return "";
		}
		return txt+" "+ep.toString()+" ";
	}


	/* (non-Javadoc)
	 * @see es.ugr.siar.ip.desc.Descriptor2MySQL#createTable()
	 */
	public String createTable() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see es.ugr.siar.ip.desc.Descriptor2MySQL#exist(int, es.ugr.siar.db.MySQL, boolean)
	 */
	public boolean exist(int ID, MySQL db, boolean checkVersion) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see es.ugr.siar.ip.desc.Descriptor2MySQL#fromMySQL(int, es.ugr.siar.db.MySQL)
	 */
	public boolean fromMySQL(int ID, MySQL db) {
		String sql="SELECT ";
		for(ExifParam ep : paramsUsed) {
			sql+=param2MySQL(ep)+",";
		}
		//Remove the last coma
		sql=sql.substring(0, sql.length()-1);
		sql+=" FROM ExifData WHERE ExifData.Photo_ID ="+ID;
		paramsData = new Double[paramsUsed.length];

		boolean state = db.queryOneRowResult(sql);	
		if(!state) return state;
		for(int i=0;i<paramsUsed.length;i++) {
			paramsData[i] = db.getValueDoubleOrNaN(paramsUsed[i].toString());
			String[] nominals = paramsUsed[i].getNominals();
			if(nominals != null) {
				if(paramsData[i] < 0 || paramsData[i] >= nominals.length) 
					paramsData[i]=Double.NaN;
			}
		}

		return state;
	}

	/* (non-Javadoc)
	 * @see es.ugr.siar.ip.desc.Descriptor2MySQL#fromMySQL(java.sql.ResultSet)
	 */
	public boolean fromMySQL(ResultSet result) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see es.ugr.siar.ip.desc.Descriptor2MySQL#getSQLParamNames()
	 */
	public String[] getSQLParamNames() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see es.ugr.siar.ip.desc.Descriptor2MySQL#getTableName()
	 */
	public String getTableName() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see es.ugr.siar.ip.desc.Descriptor2MySQL#replaceHeader()
	 */
	public String replaceHeader() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see es.ugr.siar.ip.desc.Descriptor2MySQL#toMySQL(int, es.ugr.siar.db.MySQL)
	 */
	public boolean toMySQL(int ID, MySQL db) {
		// TODO Auto-generated method stub
		return false;
	}


	/* (non-Javadoc)
	 * @see es.ugr.siar.ip.desc.VisualDescriptor#toStrLine(boolean)
	 */
	public String[] toStrVec(boolean header) {
		String [] str = new String[paramsData.length];
		for(int i=0;i<paramsData.length;i++)
			if(header) 	str[i]=paramsUsed[i].toString();
			else 		str[i]=""+paramsData[i];
		return str;
	}


	public DescriptorValues getValues() {
		DescriptorValues dV = new DescriptorValues();
		dV.setType(DescriptorValues.DescriptorType.C_EXF);
		for(int i=0;i<paramsData.length;i++) {
			dV.addValue(paramsUsed[i].toString(),paramsData[i]);
		}
		return dV;
	}


	public String[] getNominals(int id) {
		return paramsUsed[id].getNominals();
	}



	public String toString() {
		String str = "";
		for(int i=0;i<paramsData.length;i++) {
			str +=paramsUsed[i].name()+"=";
			if(paramsUsed[i].getNominals() == null)
				str +=paramsData[i]+", ";
			else
				str+=paramsUsed[i].getNominal(paramsData[i].intValue())+", ";
		}
		return str.substring(0,str.length()-2);
	}



}
