/**
 * 
 */
package es.ugr.siar.ip;


import java.io.File;
import java.util.Iterator;

import com.drew.imaging.jpeg.JpegMetadataReader;
import com.drew.imaging.jpeg.JpegProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.ExifDirectory;

/**
 * @author neub
 * @see <a href="http://www.drewnoakes.com/code/exif/sampleUsage.html>com.drew.metadata</a>
 */
public class ExifFactory {

	
	public static String toString(File f) {
		String str ="";
		try {
			Metadata metadata = JpegMetadataReader.readMetadata(f);
//			//iterate through EXIF directory 
				Directory exifDir = metadata.getDirectory(ExifDirectory.class);
				Iterator tags = exifDir.getTagIterator(); 
				while (tags.hasNext()) {
					Tag tag = (Tag)tags.next();
					str +=tag+"\n";
					// use Tag.toString()
				}
		} catch (JpegProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
}
