/**
 * 
 */
package es.ugr.siar.ml.weka;

import weka.classifiers.Evaluation;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 18 janv. 08
 *
 */
public class StatsStructure {

	protected String className;
	protected double cci;
	protected double tp;
	protected double fp;
	protected double recall;
	protected double precision;
	protected double fMeasure;
	protected double kappa;

	public StatsStructure(Evaluation eval) {
		cci=eval.pctCorrect()/100;
		tp=eval.truePositiveRate(1);
		fp=eval.falsePositiveRate(1);
		recall=eval.recall(1);
		precision=eval.precision(1);
		fMeasure=eval.fMeasure(1);
		kappa = eval.kappa();
	}
	
	public StatsStructure() {
		cci=0;
		tp=0;
		fp=0;
		recall=0;
		precision=0;
		fMeasure=0;
		kappa=0;
	}
	
	public String formatPercentage(double x) {
		return new my.PrintfFormat("%2.1f").sprintf(100*x);
	}
	
	public String cci() {
		return formatPercentage(cci);
	}
	public String tp0() {
		return formatPercentage(1-fp);
	}
	
	public String tp1() {
		return formatPercentage(tp);
	}
	
	public String fp1() {
		return formatPercentage(fp);
	}
	public String fp0() {
		return formatPercentage(1-tp);
	}
	public String fMeasure() {
		return formatPercentage(fMeasure);
	}
	
	public String kappa() {
		return formatPercentage(kappa);
	}
	
	public String[] getValues() {
		String [] v = new String[4];
		v[0]=tp1();
		v[1]=fp1();
		v[2]=fMeasure();
		v[3]=kappa();
		return v;
	}
	public static String[] getValuesHeader() {
		String [] v = new String[4];
		v[0]="\\acro{TP}";
		v[1]="\\acro{FP}";
		v[2]="$F_1$";
		v[3]="{\\large $\\kappa$}";
		return v;
	}
	
	public String getClassName() {
		return className;
	}
	
	public void setClassName(String className) {
		this.className=className;
	}
	
	public void plusEqual(StatsStructure ss){
		this.cci+=ss.cci;
		this.tp+=ss.tp;
		this.fp+=ss.fp;
		this.recall+=ss.recall;
		this.precision+=ss.precision;
		this.fMeasure+=ss.fMeasure;
		this.kappa+=ss.kappa;
	}
	
	public void divideBy(double k) {
		cci/=k;
		tp/=k;
		fp/=k;
		recall/=k;
		precision/=k;
		fMeasure/=k;
		kappa/=k;
	}
	
	
}
