/**
 * 
 */
package es.ugr.siar.ml.weka.trainerxp;

import weka.attributeSelection.AttributeEvaluator;
import weka.attributeSelection.AttributeSelection;
import weka.attributeSelection.ChiSquaredAttributeEval;
import weka.attributeSelection.Ranker;
import weka.core.Instances;
import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.weka.cptinst.CptInstProba;
import es.ugr.siar.ml.weka.cptinst.CptInstances;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 22 janv. 08
 *
 */
public class CptsTrainerXpMixedProba extends CptsTrainerXpGlobal {

	/**
	 * 
	 */
	public CptsTrainerXpMixedProba() {
		super(SetsFileTools.readGlobalClassNames(),new DescriptorType[]{
			DescriptorType.I_M7CSD, DescriptorType.I_M7SCD,
			DescriptorType.I_M7EHD,DescriptorType.C_EXF});
	}

	public CptsTrainerXpMixedProba(DescriptorType[] descTypes) {
		super(SetsFileTools.readGlobalClassNames(),SetsFileTools.readGlobalClassNames(),descTypes);
	}

	public CptsTrainerXpMixedProba(String[] cptsNames,String[] globalNames,DescriptorType[] descTypes){
		super(cptsNames,globalNames,descTypes);
	}
	
	
	
	
	
	
	
	/** 
	 * This is the loading for getting a FullProba cptInstance.
	 *  
	 * @see es.ugr.siar.ml.weka.trainerxp.CptsTrainerXpGlobal#getCptInstances(java.lang.String, es.ugr.siar.ip.desc.DescriptorValues.DescriptorType[], es.ugr.siar.ml.SetsFileTools.SetType)
	 */
	protected CptInstances getCptInstances(String cptName, DescriptorType[] descTypes,SetsFileTools.SetType setType) {
		String[] globalNamesTmp = getGlobalsNames(cptName);
		return new CptInstProba(cptName,globalNamesTmp,descTypes,setType);
	}
	


	public void best10AttrInLatex() {

		int nofAttrMax=10;
		String[][] tab = new String[cptsNames.length][nofAttrMax]; 

		for(int i=0;i<cptsNames.length;i++) {
			AttributeSelection filter = new AttributeSelection();
			Ranker ranker =  new Ranker();
			AttributeEvaluator attrEval = new ChiSquaredAttributeEval();
			//AttributeEvaluator attrEval = new GainRatioAttributeEval();

			loadTraining(cptsNames[i], instOnlyFF);
			Instances instTrain = cptInstTrain.getInstances();
			try {

				attrEval.buildEvaluator(instTrain);
				ranker.setNumToSelect(10);
				filter.setEvaluator(attrEval);
				filter.setSearch(ranker);
				filter.SelectAttributes(instTrain);
				double[][] rankAttr = filter.rankedAttributes();
				for(int k=0;k<nofAttrMax;k++) {
					tab[i][k]= attr2latex(instTrain.attribute((int) rankAttr[k][0]).name());
				}
				//System.out.println(filter.toResultsString());	
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		int nofCol=6;
		int nofRow=(int)Math.ceil(cptsNames.length/(double)nofCol);
		for(int i=0;i<nofRow;i++)
			latexFormating(tab,i*nofCol,Math.min(cptsNames.length,(i+1)*nofCol));
	}

	protected void latexFormating(String[][] tab,int start, int end) {
		//Latex Formating
		String headers="\\begin{tabular}{ ";
		String colNames="\\hline \n";
		String lines="";

		for(int k=-1;k<tab[0].length;k++) {
			for(int i=start;i<end;i++) {
				if(k<0) {
					headers+="| c ";
					if(i>start) colNames+="&";
					colNames+=cptsNames[i]+" ";
				}
				else{
					if(i>start) lines+="&";
					lines+=tab[i][k]+" ";
				}
			}
			if(k>=0) lines+="\\\\\n";
		}
		headers+="|} \n";
		colNames+="\\\\ \\hline \n";
		String bottom="\\hline \\end{tabular} \n";

		String print="";
		if(start==0) print+=headers;
		print+=colNames+lines;
		if(end == cptsNames.length) print+=bottom;
		System.out.println(print);
	}

	protected String attr2latex(String attr) {
		if(attr.charAt(3)=='_') {
			String rv = "$\\phi_{\\acro{"+attr.substring(0,3)+"}}|";
			rv+="\\text{"+attr.substring(10,13)+"}$";
			return rv;
		}
		else {
			if(attr.equals("ComputedShutterSpeed"))
				attr="\\acro{CSS}";
			else if(attr.equals("ComputedAperture"))
				attr="\\acro{CAP}";
			else if(attr.equals("SceneCapType"))
				attr="\\acro{SCT}";
			else if(attr.equals("MonthOfYear"))
				attr="\\acro{MoY}";
			else if(attr.equals("HourOfDay"))
				attr="\\acro{HoD}";
			else if(attr.equals("FocalLength"))
				attr="\\acro{FL}";
			else if(attr.equals("FlashFired"))
				attr="\\acro{FFi}";
			return attr;
		}

	}
	
}
