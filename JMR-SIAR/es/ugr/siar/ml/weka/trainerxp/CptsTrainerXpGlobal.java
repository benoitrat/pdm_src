/**
 * 
 */
package es.ugr.siar.ml.weka.trainerxp;

import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.weka.StatsStructure;
import es.ugr.siar.ml.weka.WekaTools;
import es.ugr.siar.ml.weka.cptcls.CptClassifier;
import es.ugr.siar.ml.weka.cptcls.CptClassifierGlobalDesc;
import es.ugr.siar.ml.weka.cptinst.CptInstances;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 8 févr. 08
 *
 */
abstract public class CptsTrainerXpGlobal extends CptsTrainerXp {

	/**
	 * @param cptsNames
	 * @param descTypes
	 */
	public CptsTrainerXpGlobal(String[] cptsNames, DescriptorType[] descTypes) {
		super(cptsNames, descTypes);
	}

	/**
	 * @param cptsNames
	 * @param globalNames
	 * @param descTypes
	 */
	public CptsTrainerXpGlobal(String[] cptsNames, String[] globalNames,
			DescriptorType[] descTypes) {
		super(cptsNames, globalNames, descTypes);
	}
	
	
	// -------------------------------------------------------
	

	@Override
	public void makeInstances(String[] cptsNames,boolean overWrite) {
		for(String cptName : cptsNames) {
			CptInstances cptInstTrain = getCptInstances(cptName,descTypes,SetsFileTools.SetType.TRAIN);
			cptInstTrain.setOverWritable(overWrite);
			cptInstTrain.loadInstances();
			CptInstances cptInstTest = getCptInstances(cptName,descTypes,SetsFileTools.SetType.TEST);
			cptInstTest.setOverWritable(overWrite);
			cptInstTest.loadInstances();
		}
	}
	
	public void makeFilteredInstances(DescriptorType[] keepOnlyDesc) {
		boolean overWrite = isInstOverwrite();
		for(String cptName : cptsNames) {
			
			CptInstances cptInstTrain = getCptInstances(cptName,descTypes,SetsFileTools.SetType.TRAIN);
			cptInstTrain.setOverWritable(overWrite);
			cptInstTrain.loadInstances();
			cptInstTrain.keepOnlyDesc(keepOnlyDesc);
			cptInstTrain.saveInstancesInArff();
			
			CptInstances cptInstTest = getCptInstances(cptName,descTypes,SetsFileTools.SetType.TEST);
			cptInstTest.setOverWritable(overWrite);
			cptInstTest.loadInstances();
			cptInstTest.keepOnlyDesc(keepOnlyDesc);
			cptInstTest.saveInstancesInArff();
		}
		
	}
	
	// ------------------------------------------------------
	
	public void loadTesting(String cptName,boolean onlyFF) {
		cptInstTest = loadSet(cptName,SetsFileTools.SetType.TEST,onlyFF);
	}

	public void loadTraining(String cptName,boolean onlyFF) {
		cptInstTrain = loadSet(cptName,SetsFileTools.SetType.TRAIN,onlyFF);
	}
	
	protected CptInstances loadSet(String cptName, SetsFileTools.SetType setType,boolean onlyFF) {
		CptInstances cptInstTmp = getCptInstances(cptName,descTypes,setType);
		if(onlyFF)
			cptInstTmp.loadInstancesFromFile();
		else 
			cptInstTmp.loadInstances();
		return cptInstTmp;
	}
	
	// ------------------------------------------------------
	
	
	abstract protected CptInstances getCptInstances(String cptName, DescriptorType[] descTypes,SetsFileTools.SetType setType);
	
	
	// -------------------------------------------------------


	@Override
	public void makeClassifiers(String[] cptsNames, DescriptorType[] descTypes,boolean overWrite) {
		WekaTools.println("Not implemented",false);
		for(int i=0;i<cptsNames.length;i++) {
			filterDescFromInst(descTypes);
			loadTraining(cptsNames[i], instOnlyFF);
			CptClassifier cls = CptClassifierGlobalDesc.getCptClsGlobal(cptInstTrain);
			cls.setClassifier();
			cls.setOverWritable(overWrite);
			if(cptInstTrain.isLoaded()) {
				if(isResample) cptInstTrain.resample(resampB,resampPct);
				cls.trainClassifier(cptInstTrain.getInstances());
				cls.saveClsToFile();
			}
		}
	}
	
	
	// -------------------------------------------------------
	
	/* (non-Javadoc)
	 * @see es.ugr.siar.ml.weka.trainerxp.CptsTrainerXp#getStatsList()
	 */
	@Override
	public StatsStructure[] getStatsList() {
		StatsStructure[] statStruc = new StatsStructure[cptsNames.length];
		
		
		//Iterate over classNames.
		for(int i=0;i<cptsNames.length;i++) {
			//Load testing set and classifier
			loadTraining(cptsNames[i],isInstOnlyFF());
			loadTesting(cptsNames[i],isInstOnlyFF());
			CptClassifier cls = CptClassifierGlobalDesc.getCptClsGlobal(cptInstTrain);
			cls.loadCls();
			
			//If classifier is not trained or need to be overwritten, load training set for learning
			if(!cls.isTrained() || isClsOverwrite()) {
				cls.setClassifier();
				cls.setOverWritable(isClsOverwrite());
				if(cptInstTrain.isLoaded()) {
					if(isResample) cptInstTrain.resample(resampB,resampPct);
					cls.trainClassifier(cptInstTrain.getInstances());
					cls.saveClsToFile();
				}
			}
			if(cptInstTest.isLoaded() && cls.isTrained()) {
				statStruc[i] = new StatsStructure(cls.getEvalution(cptInstTest.getInstances()));
				statStruc[i].setClassName(cptsNames[i]);
			}
		}
		return statStruc;
	}
	

	
	
	// -------------------------------------------------------


	@Override
	public void printResultInLatex(DescriptorType[] descTypes) {
		WekaTools.println("Not implemented",false);

	}

	@Override
	public void showResult(String[] cptsNames, DescriptorType[] descTypes) {
		WekaTools.println("Not implemenented",false);

	}
	
	

}
