/**
 * 
 */
package es.ugr.siar.ml.weka.trainerxp;

import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ip.desc.ExifDescriptor.ExifParam;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.weka.StatsStructure;
import es.ugr.siar.ml.weka.WekaTools;
import es.ugr.siar.ml.weka.cptinst.CptInstances;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 21 janv. 08
 *
 */
abstract public class CptsTrainerXp {

	CptInstances cptInstTrain;
	CptInstances cptInstTest;

	DescriptorType[] descTypes;

	String[] cptsNames;
	String[] globalNames;

	boolean instOnlyFF=true;
	boolean clsOnlyFF=true;
	boolean clsOverwrite=false;
	boolean instOverwrite=false;

	boolean isResample=false;
	float resampB=1.0f;
	float resampPct=100;

	/**
	 * 
	 */
	public CptsTrainerXp(String[] cptsNames,DescriptorType[] descTypes) {
		this.cptsNames=cptsNames;
		globalNames=SetsFileTools.readGlobalClassNames();
		this.descTypes=descTypes;
	}


	public CptsTrainerXp(String[] cptsNames,String[] globalNames,DescriptorType[] descTypes){
		this.cptsNames=cptsNames;
		this.globalNames=globalNames;
		this.descTypes=descTypes;
	}

//	------------------------------------------------------------
	public void makeInstances() {
		makeInstances(cptsNames, instOverwrite);
	}
	abstract public void makeInstances(String[] cptsNames,  boolean overWrite);

//	------------------------------------------------------------

//	abstract public void loadTraining(String cptName,boolean instOnlyFF);
//	abstract public void loadTesting(String cptName,boolean instOnlyFF);

//	------------------------------------------------------------

	public void makeClassifiers() {
		makeClassifiers(cptsNames,descTypes,clsOverwrite);
	}
	abstract public void makeClassifiers(String[] cptsNames,DescriptorType[] descTypes,boolean overWrite);



	//-------------------------------


	protected String[] getGlobalsNames(String cptName) {
		if(globalNames==null) 
			return new String[]{cptName};
		else
			return globalNames;	
	}


//	------------------------------------------------------------

	public void showResult() {
		showResult(cptsNames,descTypes);
	}
	abstract public void showResult(String[] cptsNames,DescriptorType[] descTypes);

	public void printResultInLatex() {
		printResultInLatex(descTypes);
	}
	abstract public void printResultInLatex(DescriptorType[] descTypes);

	//Should not be abstract and should go on CptInstance method
	void filterDescFromInst(DescriptorType[] descTypes) {

	}
	void filterExif(ExifParam[] exifParams){

	}




	/**
	 * @return the instOnlyFF
	 */
	public boolean isInstOnlyFF() {
		return instOnlyFF;
	}


	/**
	 * @param instOnlyFF the instOnlyFF to set
	 */
	public void setInstOnlyFF(boolean instOnlyFF) {
		this.instOnlyFF = instOnlyFF;
	}


	/**
	 * @return the clsOnlyFF
	 */
	public boolean isClsOnlyFF() {
		return clsOnlyFF;
	}


	/**
	 * @param clsOnlyFF the clsOnlyFF to set
	 */
	public void setClsOnlyFF(boolean clsOnlyFF) {
		this.clsOnlyFF = clsOnlyFF;
	}


	/**
	 * @return the clsOverwrite
	 */
	public boolean isClsOverwrite() {
		return clsOverwrite;
	}


	/**
	 * @param clsOverwrite the clsOverwrite to set
	 */
	public void setClsOverwrite(boolean clsOverwrite) {
		this.clsOverwrite = clsOverwrite;
		this.clsOnlyFF=false;
	}


	/**
	 * @return the instOverwrite
	 */
	public boolean isInstOverwrite() {
		return instOverwrite;
	}


	/**
	 * @param instOverwrite the instOverwrite to set
	 */
	public void setInstOverwrite(boolean instOverwrite) {
		this.instOverwrite = instOverwrite;
		this.clsOnlyFF=false;
	}


	/**
	 * Set resample() with default parameters.
	 * bias=1.0 and pct=100%.
	 * @param isResample
	 */
	public void setResample(boolean isResample) {
		this.isResample=isResample;
	}
	
	public void setResampleParam(float bias, float pct) {
		this.resampB=bias;
		this.resampPct=pct;
		setResample(true);
	}

	//----------------------------------------------------

	abstract public StatsStructure[] getStatsList();

	
	
	public static void printResultTexRow(StatsStructure[][] statsMx, DescriptorType[] descVec) {
		DescriptorType[][] descMx = new DescriptorType[descVec.length][1];
		for(int i=0;i<descVec.length;i++) {
			descMx[i][0] = descVec[i];
		}
		printResultTexRow(statsMx, descMx);
	}

	public static void printResultTexRow(StatsStructure[][] statsMx, DescriptorType[][] descMx) {

		if(descMx.length != statsMx.length) {
			WekaTools.println("Error because statsMx et descMx are not the same", false);
			return;
		}

		StatsStructure[] ssMeans = new StatsStructure[statsMx.length];
		for(int i=0;i<ssMeans.length;i++)  
			ssMeans[i] = new StatsStructure();

		String header="\\begin{table}[ht]\n\\centering\n\\small % centering table\n";
		String tabularFormat="\\begin{tabular}{| l ";
		String tabularNames="$D_k$ ";
		String subColumnNames="$Score$ ";
		for(int i=0;i<descMx.length;i++) {
			String[] valHeaders = StatsStructure.getValuesHeader();
			tabularFormat+="| ";
			tabularNames +="& \\multicolumn{"+valHeaders.length+"}{|c|}{";
			for(int j=0;j<descMx[i].length;j++) {
				tabularNames+="\\acro{"+descMx[i][j].getAcronyms(3)+"},";
			}
			tabularNames=tabularNames.substring(0, tabularNames.length()-1)+"} ";
			for(int j=0;j<valHeaders.length;j++) {
				tabularFormat+="c ";
				subColumnNames+="&"+valHeaders[j]+" ";
			}
		}

		header+=tabularFormat+"| } \\hline\n";
		header+=tabularNames+" \\\\ \\hline\n";
		header+=subColumnNames+"\\\\ \\hline\\hline\n\n";

		String data="";	
		//For each concept names (line)
		for(int j=0; j< statsMx[0].length;j++) {
			String line="";
			if(statsMx.length > 2) {
				line+=statsMx[0][j].getClassName().substring(0,3)+"\t";
			}
			else {
				line+=statsMx[0][j].getClassName()+"\t\t";
			}

			//For each descriptor (column)
			for(int i=0;i<statsMx.length;i++) {
				ssMeans[i].plusEqual(statsMx[i][j]);
				String[] vals = statsMx[i][j].getValues();
				for(int k=0;k<vals.length;k++) {
					line+="&"+vals[k]+" ";
				}
			}
			line+="\\\\ \\hline \n";
			data+=line;
		}
		//Total
		String line="\\hline\n";
		if(statsMx.length > 2) line+="Tot \t\t";
		else	line+="Total \t";
		//For each descriptor (column)
		for(int i=0;i<statsMx.length;i++) {
			ssMeans[i].divideBy(statsMx[i].length);
			String[] vals = ssMeans[i].getValues();
			for(int k=0;k<vals.length;k++) {
				line+="&"+vals[k]+" ";
			}
		}
		line+="\\\\ \\hline \n";
		data+=line;



		String bottom;
		bottom ="\n"; 
		bottom+="\\end{tabular} \n";
		bottom+="\\caption{??? performances with ??? Descriptors} \n"; 
		bottom+="\\label{tab:result:???}\n";
		bottom+="\\end{table}\n";

		System.out.println(header+data+bottom);

	}


	/**
	 * 
	 * @param statsMx 	a matrix [xPeriments x cptNames]
	 * @param descStrMx	a matrix [xPeriments]
	 */
	public static void printResultTexCol(StatsStructure[][] statsMx, String[] descStr) {

		if(descStr.length != statsMx.length) {
			WekaTools.println("Error because statsMx et descMx are not the same", false);
			return;
		}
		String[] resultNames = StatsStructure.getValuesHeader();	

		//Print latex
		String header="\\begin{tabular}{| l ";
		String colNames=" ";
		String lines ="";

		for(int j=0,i=0;j<statsMx[i].length;j++) {
			header+="| c ";
			colNames+="&"+statsMx[i][j].getClassName().substring(0,3)+" ";
		}
		header+="|| c";
		colNames+="& $\\mu$ ";
		header+="| } \\hline \n";
		colNames+="\\\\ \\hline \n";


		//Iterate over Xp
		for(int i=0;i<statsMx.length;i++) {
			lines+="\\hline \n {\\large \\strut } ";
			lines+="Desc &\\multicolumn{"+(statsMx[i].length+1)+"}{|c|}{"+descStr[i]+"} \\\\[0.5ex] \\hline \n";
			StatsStructure meanSS = new StatsStructure();
			for(int j=0;j<statsMx[i].length;j++) {
				meanSS.plusEqual(statsMx[i][j]);
			}
			meanSS.divideBy(statsMx[i].length);

			//iterate over TP, FP, F_1,...
			for(int k=0;k<resultNames.length;k++) {
				lines+=resultNames[k]+" ";
				//Iterate over conceptName
				for(int j=0;j<statsMx[i].length;j++) {
					lines+="&"+statsMx[i][j].getValues()[k]+" ";
				}
				lines+="&"+meanSS.getValues()[k]+" ";
				lines+="\\\\ \n";
			}
		}
		String bottom="\\hline \\end{tabular} \n";
		System.out.println(header+colNames+lines+bottom);

	}
	
	public static String getXpString(DescriptorType[] descTypes, String func, boolean multi) {
		String rv="";
		String cpt;
		if(multi) cpt="C_p";
		else	cpt="C";
		
		for(DescriptorType descType:descTypes) {
			rv+=" $";
			if(multi) rv+="\\{";
			rv+="\\acro{"+func+"}_{\\acro{"+descType.getAcronyms(3)+"}|"+cpt+"}";
			if(multi) rv+="\\}";
			rv+="$ ,";
		}
		return rv.substring(0,rv.length()-1);
	}
	

}
