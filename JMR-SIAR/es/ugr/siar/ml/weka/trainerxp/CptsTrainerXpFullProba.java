/**
 * 
 */
package es.ugr.siar.ml.weka.trainerxp;

import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.weka.WekaTools;
import es.ugr.siar.ml.weka.cptinst.CptInstFullProba;
import es.ugr.siar.ml.weka.cptinst.CptInstances;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 22 janv. 08
 *
 */
public class CptsTrainerXpFullProba extends CptsTrainerXpGlobal {

	/**
	 * 
	 */
	public CptsTrainerXpFullProba() {
		super(SetsFileTools.readGlobalClassNames(),WekaTools.getFullDescTypes());
	}

	public CptsTrainerXpFullProba(DescriptorType[] descTypes) {
		super(SetsFileTools.readGlobalClassNames(),SetsFileTools.readGlobalClassNames(),descTypes);
	}

	public CptsTrainerXpFullProba(String[] cptsNames,String[] globalNames,DescriptorType[] descTypes){
		super(cptsNames,globalNames,descTypes);
	}

	//-------------------------------
	

	
	
	/** 
	 * This is the loading for getting a FullProba cptInstance.
	 *  
	 * @see es.ugr.siar.ml.weka.trainerxp.CptsTrainerXpGlobal#getCptInstances(java.lang.String, es.ugr.siar.ip.desc.DescriptorValues.DescriptorType[], es.ugr.siar.ml.SetsFileTools.SetType)
	 */
	protected CptInstances getCptInstances(String cptName, DescriptorType[] descTypes,SetsFileTools.SetType setType) {
		String[] globalNamesTmp = getGlobalsNames(cptName);
		return new CptInstFullProba(cptName,globalNamesTmp,descTypes,setType);
	}
	




}
