/**
 * 
 */
package es.ugr.siar.ml.weka.trainerxp;

import weka.classifiers.Evaluation;
import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.weka.StatsStructure;
import es.ugr.siar.ml.weka.cptcls.CptClassifierIndDesc;
import es.ugr.siar.ml.weka.cptinst.CptInstDesc;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 23 janv. 08
 *
 */
public class CptsTrainerXpIndDesc extends CptsTrainerXp {


	public CptsTrainerXpIndDesc() {
		super(SetsFileTools.readGlobalClassNames(),new DescriptorType[]{
			DescriptorType.I_M7CSD, DescriptorType.I_M7SCD,
			DescriptorType.I_M7EHD});
	}

	/**
	 * @param cptsNames
	 */
	public CptsTrainerXpIndDesc(String[] cptsNames) {
		super(cptsNames,cptsNames,new DescriptorType[]{
				DescriptorType.I_M7CSD, DescriptorType.I_M7SCD,
				DescriptorType.I_M7EHD});
	}

	/**
	 * @param cptsNames
	 * @param globalNames
	 * @param descTypes
	 */
	public CptsTrainerXpIndDesc(String[] cptsNames, String[] globalNames,
			DescriptorType[] descTypes) {
		super(cptsNames, globalNames, descTypes);
		// TODO Auto-generated constructor stub
	}

	public void loadTraining(String cptsName,DescriptorType descType, boolean onlyFF) {
		cptInstTrain = new CptInstDesc(cptsName,descType,SetsFileTools.SetType.TRAIN);
		if(onlyFF)
			cptInstTrain.loadInstancesFromFile();
		else 
			cptInstTrain.loadInstances();
	}

	public void loadTesting(String cptsName,DescriptorType descType, boolean onlyFF) {
		cptInstTest = new CptInstDesc(cptsName,descType,SetsFileTools.SetType.TEST);
		if(onlyFF)
			cptInstTest.loadInstancesFromFile();
		else 
			cptInstTest.loadInstances();
	}


	@Override
	public void makeClassifiers(String[] cptsNames, DescriptorType[] descTypes,
			boolean overWrite) {

		//For each classNames
		for(String cptName : cptsNames) {
			for(DescriptorType descType : descTypes) {
				loadTraining(cptName, descType, instOnlyFF);
				CptClassifierIndDesc cptCls= CptClassifierIndDesc.getInstance(cptName,descType);
				cptCls.loadClsFromFile();
				if(cptCls.isTrained() && clsOnlyFF) continue;
				cptCls.setClassifier();
				cptCls.setOverWritable(overWrite);
				if(cptCls.isLoaded() && cptInstTrain.isLoaded()) {
					cptInstTrain.resample();
					cptCls.trainClassifier(cptInstTrain.getInstances());
					cptCls.saveClsToFile();
				}
			}
		}

	}


	@Override
	public void makeInstances(String[] cptsNames, boolean overWrite) {

		//For each classNames
		for(String cptName : cptsNames) {
			for(DescriptorType descType : descTypes) {

				//Training
				CptInstDesc instDescTrain = new CptInstDesc(cptName,descType,SetsFileTools.SetType.TRAIN);
				instDescTrain.setOverWritable(overWrite);
				instDescTrain.loadInstances();

				//Testing
				CptInstDesc instDescTest = new CptInstDesc(cptName,descType,SetsFileTools.SetType.TEST);
				instDescTest.setOverWritable(overWrite);
				instDescTest.loadInstances();
			}
		}
	}

	/* (non-Javadoc)
	 * @see es.ugr.siar.ml.weka.CptsTrainerXp#printResultInLatex(es.ugr.siar.ip.desc.DescriptorValues.DescriptorType[])
	 */
	@Override
	public void printResultInLatex(DescriptorType[] descTypes) {
		StatsStructure[][] statsMx = getStatsMx(); 

		String[] cptNames = cptsNames;

		String header="\\begin{table}[h]\n\\centering % centering table\n";
		String tabularFormat="\\begin{tabular}{| l c ";
		String tabularNames="Class & Label ";
		String subColumnNames=" & ";
		for(int i=0; i< statsMx[0].length;i++) {
			String[] valHeaders = StatsStructure.getValuesHeader();
			tabularFormat+="| ";
			tabularNames +="& \\multicolumn{"+valHeaders.length+"}{|c|}{"+descTypes[i].getAcronyms(3)+"} ";
			for(int j=0;j<valHeaders.length;j++) {
				tabularFormat+="c ";
				subColumnNames+="&"+valHeaders[j]+" ";
			}
		}

		header+=tabularFormat+"| } \\hline\\hline\n";
		header+=tabularNames+" \\\\ \\hline\n";
		header+=subColumnNames+"\\\\ [0.5ex]  \\hline\\hline\n\n";

		String data="";
		//For each concept names (line)
		for(int i=0; i< statsMx.length;i++) {
			String line=cptNames[i]+" ";

			//For each descriptor (column)
			for(int j=0;j<statsMx[i].length;j++) {
				String[] vals = statsMx[i][j].getValues();
				for(int k=0;k<vals.length;k++) {
					line+="&"+vals[k]+" ";
				}
			}
			line+="\\\\ \\hline \n";
			data+=line;
		}

		String bottom;
		bottom ="\\hline \n"; 
		bottom+="\\end{tabular} \n";
		bottom+="\\caption{SVM performances with MPEG7 Descriptors} \n"; 
		bottom+="\\label{tab:result:svmdesc}\n";
		bottom+="\\end{table}\n";

		System.out.println(header+data+bottom);

	}

	/* (non-Javadoc)
	 * @see es.ugr.siar.ml.weka.CptsTrainerXp#showResult(java.lang.String[], es.ugr.siar.ip.desc.DescriptorValues.DescriptorType[])
	 */
	@Override
	public void showResult(String[] cptsNames, DescriptorType[] descTypes) {
		// TODO Auto-generated method stub
	}

	
	/**
	 * Return a {@link StatsStructure} matrix where i represent the descriptors and j the classes.
	 * @return
	 */
	public StatsStructure[][] getStatsMx()  {
		StatsStructure[][] statsMx = new StatsStructure[descTypes.length][cptsNames.length];

		for(int i=0; i< descTypes.length;i++) {
			DescriptorType descType = descTypes[i];
			for(int j=0;j<cptsNames.length;j++) {
				String cptName = cptsNames[j];
				loadTesting(cptName,descType,instOnlyFF);
				CptClassifierIndDesc cptCls = CptClassifierIndDesc.getInstance(cptName,descType);
				cptCls.loadCls();
				Evaluation eval = cptCls.getEvalution(cptInstTest.getInstances());
				statsMx[i][j] = new StatsStructure(eval);
				statsMx[i][j].setClassName(cptName);
			}
		}
		return statsMx;
	}
	
	public StatsStructure[] getStatsList() {
		return null;
	}
}