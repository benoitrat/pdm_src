/**
 * 
 */
package es.ugr.siar.ml.weka.trainerxp;

import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.weka.cptinst.CptInstDist;
import es.ugr.siar.ml.weka.cptinst.CptInstances;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 22 janv. 08
 *
 */
public class CptsTrainerXpMixedDist extends CptsTrainerXpGlobal {

	/**
	 * 
	 */
	public CptsTrainerXpMixedDist() {
		super(SetsFileTools.readGlobalClassNames(),new DescriptorType[]{
			DescriptorType.I_M7CSD, DescriptorType.I_M7SCD,
			DescriptorType.I_M7EHD,DescriptorType.C_EXF});
	}

	public CptsTrainerXpMixedDist(DescriptorType[] descTypes) {
		super(SetsFileTools.readGlobalClassNames(),SetsFileTools.readGlobalClassNames(),descTypes);
	}

	public CptsTrainerXpMixedDist(String[] cptsNames,String[] globalNames,DescriptorType[] descTypes){
		super(cptsNames,globalNames,descTypes);
	}

	//-------------------------------
	
	/** 
	 * This is the loading for getting a FullProba cptInstance.
	 *  
	 * @see es.ugr.siar.ml.weka.trainerxp.CptsTrainerXpGlobal#getCptInstances(java.lang.String, es.ugr.siar.ip.desc.DescriptorValues.DescriptorType[], es.ugr.siar.ml.SetsFileTools.SetType)
	 */
	protected CptInstances getCptInstances(String cptName, DescriptorType[] descTypes,SetsFileTools.SetType setType) {
		String[] globalNamesTmp = getGlobalsNames(cptName);
		return new CptInstDist(cptName,globalNamesTmp,descTypes,setType);
	}
	
}
