/**
 * 
 */
package es.ugr.siar.ml.weka.trainerxp;

import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.weka.cptinst.CptInstMultiDesc;
import es.ugr.siar.ml.weka.cptinst.CptInstances;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 22 janv. 08
 *
 */
public class CptsTrainerXPMultiDesc extends CptsTrainerXpGlobal {

	/**
	 * 
	 */
	public CptsTrainerXPMultiDesc() {
		super(SetsFileTools.readGlobalClassNames(),new DescriptorType[]{
			DescriptorType.I_M7CSD, DescriptorType.I_M7SCD,
			DescriptorType.I_M7EHD,DescriptorType.C_EXF});
	}

	public CptsTrainerXPMultiDesc(DescriptorType[] descTypes) {
		super(SetsFileTools.readGlobalClassNames(),SetsFileTools.readGlobalClassNames(),descTypes);
	}

	public CptsTrainerXPMultiDesc(String[] cptsNames,String[] globalNames,DescriptorType[] descTypes){
		super(cptsNames,globalNames,descTypes);
	}

	//-------------------------------
	
	
	/** 
	 * This is the loading for getting a FullProba cptInstance.
	 *  
	 * @see es.ugr.siar.ml.weka.trainerxp.CptsTrainerXpGlobal#getCptInstances(java.lang.String, es.ugr.siar.ip.desc.DescriptorValues.DescriptorType[], es.ugr.siar.ml.SetsFileTools.SetType)
	 */
	protected CptInstances getCptInstances(String cptName, DescriptorType[] descTypes,SetsFileTools.SetType setType) {
		return new CptInstMultiDesc(cptName,descTypes,setType);
	}
	
	
//
//	@Override
//	public void makeInstances(String[] cptsNames,boolean overWrite) {
//		for(String cptName : cptsNames) {
//			CptInstMultiDesc cptIMDTrain = new CptInstMultiDesc(cptName,descTypes,SetsFileTools.SetType.TRAIN);
//			cptIMDTrain.setOverWritable(overWrite);
//			cptIMDTrain.loadInstances();
//			CptInstMultiDesc cptIMDTest = new CptInstMultiDesc(cptName,descTypes,SetsFileTools.SetType.TEST);
//			cptIMDTest.setOverWritable(overWrite);
//			cptIMDTest.loadInstances();
//		}
//	}
//
//
//	/**
//	 * @param cptsName
//	 * @param gCptNames
//	 * @param descTypes
//	 * @param instOnlyFF only From File Tag
//	 */
//	public void loadTesting(String cptsName,boolean onlyFF) {
//		cptInstTest = new CptInstMultiDesc(cptsName,descTypes,SetsFileTools.SetType.TEST);
//		if(onlyFF)
//			cptInstTest.loadInstancesFromFile();
//		else 
//			cptInstTest.loadInstances();
//	}
//
//
//	/**
//	 * @param cptsName
//	 * @param gCptNames
//	 * @param descTypes
//	 * @param instOnlyFF only From File tag
//	 */
//	public void loadTraining(String cptsName,boolean onlyFF) {
//		cptInstTrain = new CptInstMultiDesc(cptsName,descTypes,SetsFileTools.SetType.TRAIN);
//		if(onlyFF)
//			cptInstTrain.loadInstancesFromFile();
//		else 
//			cptInstTrain.loadInstances();
//	}
//
//
//
//	/* (non-Javadoc)
//	 * @see es.ugr.siar.ml.weka.CptsTrainerXp#makeClassifiers(java.lang.String[], es.ugr.siar.ip.desc.DescriptorValues.DescriptorType[])
//	 */
//	@Override
//	public void makeClassifiers(String[] cptsNames, DescriptorType[] descTypes,boolean overWrite) {
//		for(int i=0;i<cptsNames.length;i++) {
//			filterDescFromInst(descTypes);
//			loadTraining(cptsNames[i], instOnlyFF);
//			CptClassifierGlobalDesc cls = new CptClassifierGlobalDesc(cptsNames[i],descTypes);
//			cls.setClsPath(WekaTools.pathClsMultiDescFile+cptInstTrain.getFName()+WekaTools.extFClassifier);
//			cls.setClassifier();
//			cls.setOverWritable(overWrite);
//			if(cptInstTrain.isLoaded()) {
//				cptInstTrain.resample();
//				cls.trainClassifier(cptInstTrain.getInstances());
//				cls.saveClsToFile();
//			}
//		}
//	}
//	
//	
//	public StatsStructure[] getStatsList() {
//		StatsStructure[] statStruc = new StatsStructure[cptsNames.length];
//		
//		
//		//Iterate over classNames.
//		for(int i=0;i<cptsNames.length;i++) {
//			//Load testing set and classifier
//			loadTesting(cptsNames[i],isInstOnlyFF());
//			CptClassifierGlobalDesc cls = new CptClassifierGlobalDesc(cptsNames[i],descTypes);
//			cls.setClsPath(WekaTools.pathClsMultiDescFile+cptInstTest.getFName()+WekaTools.extFClassifier);
//			cls.loadCls();
//			
//			//If classifier is not trained, load training set for learning
//			if(!cls.isTrained()) {
//				loadTraining(cptsNames[i], isInstOnlyFF());
//				cls.setClassifier();
//				cls.setOverWritable(isClsOverwrite());
//				if(cptInstTrain.isLoaded()) {
//					cptInstTrain.resample();
//					cls.trainClassifier(cptInstTrain.getInstances());
//					cls.saveClsToFile();
//				}
//			}
//			if(cptInstTest.isLoaded() && cls.isTrained()) {
//				statStruc[i] = new StatsStructure(cls.getEvalution(cptInstTest.getInstances()));
//				statStruc[i].setClassName(cptsNames[i]);
//			}
//		}
//		return statStruc;
//	}
//	
//	
//	
//
//	
//
//	/* (non-Javadoc)
//	 * @see es.ugr.siar.ml.weka.CptsTrainerXp#printResultInLatex(es.ugr.siar.ip.desc.DescriptorValues.DescriptorType[])
//	 */
//	@Override
//	public void printResultInLatex(DescriptorType[] descTypes) {
//		//makeClassifiers(cptsNames, descTypes, false);
//
//		String paramsCol[][] = new String[cptsNames.length+1][];
//		paramsCol[0] = StatsStructure.getValuesHeader();	
//
//		//Fill paramsCols stats.
//		for(int i=0;i<cptsNames.length;i++) {
//			loadTesting(cptsNames[i],isInstOnlyFF());
//			
//			CptClassifierGlobalDesc cls = new CptClassifierGlobalDesc(cptsNames[i],descTypes);
//			cls.setClsPath(WekaTools.pathClsMultiDescFile+cptInstTest.getFName()+WekaTools.extFClassifier);
//			cls.loadCls();
//			if(!cls.isTrained()) {
//				loadTraining(cptsNames[i], isInstOnlyFF());
//				cls.setClassifier();
//				cls.setOverWritable(isClsOverwrite());
//				if(cptInstTrain.isLoaded()) {
//					cptInstTrain.resample();
//					cls.trainClassifier(cptInstTrain.getInstances());
//					cls.saveClsToFile();
//				}
//			}
//			
//			if(cptInstTest.isLoaded() && cls.isTrained()) {
//				StatsStructure sStruc = new StatsStructure(cls.getEvalution(cptInstTest.getInstances()));
//				paramsCol[i+1] = sStruc.getValues();
//			}
//		}
//
//		//Print latex
//		String header="\\begin{tabular}{| l ";
//		String colNames=" ";
//		String line ="";
//		for(int i=0;i<cptsNames.length;i++) {
//			header+="| c ";
//			colNames+="&"+cptsNames[i].substring(0,3)+" ";
//		}
//		header+="| } \\hline \n";
//		colNames+="\\\\ \\hline\\hline \n";
//		String descUsed="";
//		for(DescriptorType descType : descTypes) descUsed+=descType.getAcronyms(3)+", ";
//		line+="Desc &\\multicolumn{"+cptsNames.length+"}{|c|}{"+descUsed+"}\\\\ \\hline \n";
//		for(int i=0;i<paramsCol[0].length;i++){
//			line+=paramsCol[0][i]+" ";
//			for(int j=1;j<=cptsNames.length;j++) {
//				line+="&"+paramsCol[j][i]+" ";
//			}
//			line+="\\\\ \n";
//		}
//		String bottom="\\hline \\end{tabular} \n";
//		System.out.println(header+colNames+line+bottom);
//
//
//
//	}
//
//	/* (non-Javadoc)
//	 * @see es.ugr.siar.ml.weka.CptsTrainerXp#showResult(java.lang.String[], es.ugr.siar.ip.desc.DescriptorValues.DescriptorType[])
//	 */
//	@Override
//	public void showResult(String[] cptsNames, DescriptorType[] descTypes) {
//		WekaTools.println("Not implemented", false);
//		
//	}

}
