/**
 * 
 */
package es.ugr.siar.ml.weka;

import java.util.HashSet;
import java.util.Set;

import es.ugr.siar.gui.thumbs.WinThumbsMain;
import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.weka.cptcls.CptClassifier;
import es.ugr.siar.ml.weka.cptcls.CptClassifierGlobalDesc;
import es.ugr.siar.ml.weka.cptcls.CptClassifierIndDesc;
import es.ugr.siar.ml.weka.cptinst.CptInstDesc;
import es.ugr.siar.ml.weka.cptinst.CptInstFullProba;
import es.ugr.siar.ml.weka.cptinst.CptInstProba;
import es.ugr.siar.ml.weka.cptinst.CptInstances;
import es.ugr.siar.tools.ArraysTools;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 19 févr. 08
 *
 */
public class CptExifCorrected {
	
	private boolean conjugateSet = false;
	
	private static SetsFileTools.SetType testSet = SetsFileTools.SetType.TEST;
	
	
	public String formatPercentage(double x) {
		return new my.PrintfFormat("%2.1f").sprintf(100*x);
	}
	
	public void getStats() {
		String[] cptNames = SetsFileTools.readGlobalClassNames();
		String format="";
		String titles="";
		String values="";
		for(String cptName : cptNames) {
			Set<Integer> onlyExif = getClassifiedOnlyByExif(cptName);
			int [] photoID = SetsFileTools.readPhotosID(cptName, testSet);
			
			format+="|c ";
			titles+="\t&"+cptName.substring(0,3);
			values+="\t&"+formatPercentage((double)onlyExif.size()/(double)photoID.length);
			
			System.out.println("onlyExif= "+onlyExif.size() +"/"+photoID.length+"\n--------\n");
		}
		
		System.out.println(format+"|}\\hline\n"+titles+"\\\\\\hline\n"+values+"\\\\\\hline\n");
	}
	
	public void visualizeImproved(String cptName) {
		Set<Integer> set;
		
		conjugateSet=false;
		set = getImprovedByExif(cptName);
		if(set.size()!=0) 
			new WinThumbsMain("Recover with Exif",ArraysTools.toPrimitiveVec(set));
		
		conjugateSet=true;
		set = getImprovedByExif(cptName);
		if(set.size()!=0) 
			new WinThumbsMain("Recover with Exif",ArraysTools.toPrimitiveVec(set));
		
	}
	
	
	
	
	public Set<Integer> getImprovedByExif(String cptName) {
		Set<Integer> visual = getClassifiedIdByMPEG7(cptName);
		System.out.println("visual:"+visual.size() );
		Set<Integer> both = getClassifiedIdByBoth(cptName);
		System.out.println("both:"+both.size() );
		both.removeAll(visual);
		return both;
	}
	
	
	public Set<Integer> getClassifiedOnlyByExif(String cptName) {
		Set<Integer> exif = getClassifiedIdByExif(cptName);
		System.out.println("exif:"+exif.size() );
		Set<Integer> visual = getClassifiedIdByMPEG7(cptName);
		System.out.println("visual:"+visual.size() );
		exif.removeAll(visual);
		return exif;
	}
	
	
	public Set<Integer> getClassifiedIdByExif(String cptName) {
		CptInstDesc cptInst = new CptInstDesc(cptName,DescriptorType.C_EXF,testSet);
		CptClassifierIndDesc cptCls = CptClassifierIndDesc.getInstance(cptName,DescriptorType.C_EXF);
		return getCCPhotosId(cptInst, cptCls);
	}
	
	public Set<Integer> getClassifiedIdByMPEG7(String cptName) {
		String[] cptNames = SetsFileTools.readGlobalClassNames();
		DescriptorType[] descTypes = WekaTools.getVisualDescTypes();
		
		CptInstances cptInst = new CptInstProba(cptName,cptNames,descTypes,testSet);
		CptClassifier cptCls = CptClassifierGlobalDesc.getCptClsGlobal(cptInst);
		return getCCPhotosId(cptInst, cptCls);
	}
	
	public Set<Integer> getClassifiedIdByBoth(String cptName) {
		String[] cptNames = SetsFileTools.readGlobalClassNames();
		DescriptorType[] descTypes = WekaTools.getFullDescTypes();
		
		CptInstances cptInst = new CptInstFullProba(cptName,cptNames,descTypes,testSet);
		CptClassifier cptCls = CptClassifierGlobalDesc.getCptClsGlobal(cptInst);
		return getCCPhotosId(cptInst, cptCls);
	}
	
	
	public Set<Integer> getCCPhotosId(CptInstances cptInst, CptClassifier cptCls) {
		cptInst.loadInstances();
		cptCls.loadCls();
		
		double[] predicClass = cptCls.computePrediction(cptInst.getInstances());
		double[] actualClass = cptInst.getClassIndexes();
		double[] pIdVec = cptInst.getPhotosID();
		
		return getCCPhotosId(pIdVec,actualClass,predicClass);
	}
	
	

	
	public Set<Integer> getCCPhotosId(double[] id, double[] actual, double[] predic) {
		Set<Integer> set = new HashSet<Integer>();
		int length=0;
		if(id.length == actual.length && id.length == predic.length) 
			length=id.length;
		else 
			System.err.println("Invalid length");
		
		for(int i=0;i<length;i++) {
			if(actual[i] == predic[i]) //Corrected
				if(!conjugateSet && actual[i]==1)
					set.add((int)id[i]);
				if(conjugateSet && actual[i]==0)
					set.add((int)id[i]);
		}
		return set;
	}
	
}
