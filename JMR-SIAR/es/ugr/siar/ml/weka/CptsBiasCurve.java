/**
 * 
 */
package es.ugr.siar.ml.weka;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import es.ugr.siar.ml.weka.cptcls.CptClassifier;
import es.ugr.siar.ml.weka.cptcls.CptClassifierGlobalDesc;
import es.ugr.siar.ml.weka.cptinst.CptInstances;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 9 févr. 08
 *
 */
public class CptsBiasCurve {

	//Create the printStream
	PrintStream pStream;
	
	//Training instance
	CptInstances cptInstTrain;

	//Testing instance
	CptInstances cptInstTest;
	
		
	/**
	 * @param cptInstTrain
	 * @param cptInstTest
	 */
	public CptsBiasCurve(CptInstances cptInstTrain, CptInstances cptInstTest) {
		super();
		this.cptInstTrain = cptInstTrain;
		this.cptInstTest = cptInstTest;
		cptInstTest.loadInstancesFromFile();
	}


	/**
	 * Start ROC with default bias paramameters.
	 * b_start=0;b_end=1.5;b_step=0.25.
	 */
	public void start() {
		start(0,2,0.1);
	}
	
	
	public void start(double b_start, double b_end, double b_step) {
		createPrintStream();
		for(double b=b_start;b<b_end;b+=b_step) {
			trainTest(b);
		}
	
	}
	
	public void trainTest(double bias) {
		cptInstTrain.loadInstancesFromFile();
		cptInstTrain.resample(bias, 100);
		CptClassifier cptCls = CptClassifierGlobalDesc.getCptClsGlobal(cptInstTrain);
		cptCls.setClassifier();
		cptCls.trainClassifier(cptInstTrain.getInstances());
		StatsStructure sStruc = new StatsStructure(cptCls.getEvalution(cptInstTest.getInstances()));
		pStream.println(sStruc.fp1()+"\t"+sStruc.tp1());
	}
	
	
	protected void createPrintStream() {
		//Create the printStream
		try {
			pStream = new PrintStream(getFPath());

		} catch (FileNotFoundException e) {
			System.err.println("Files "+getFName()+" can not be created");
			e.printStackTrace();
			return;
		}
	}
	
	
	public String getFName() {
		return "ROC_"+cptInstTest.getFName();
	}

	public String getFPath() {
		return my.Config.dbClassfierFilePath+"curvesROC"+my.Config.OS_Slash+getFName();
	}
	
	
}
