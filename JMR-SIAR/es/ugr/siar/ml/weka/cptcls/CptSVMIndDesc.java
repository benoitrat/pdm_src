/**
 * 
 */
package es.ugr.siar.ml.weka.cptcls;

import weka.classifiers.functions.SMO;
import weka.core.Utils;
import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.weka.WekaTools;

/**
 * The object that model a CptClassifier on Independant Descriptor.
 * 
 * <p>The classifiers by default used are two SVM with LinearKernel or RBF Kernel</p> 
 * 
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 19 janv. 08
 *
 */

public class CptSVMIndDesc extends CptClassifierIndDesc {


	/** The default SVM Kernel */
	boolean kernelRBF;

	/** The default C params for SVM */
	float C;

	/** The default gamma params if kernelRBF=true */
	float gamma;



//	-----------------------------------------------------------

	/**
	 * Constructor of the CptClassifier on Independant Descriptor.
	 * 
	 * <p>By default the classifier is a linear SVM kernel</p>
	 * 
	 * @param conceptName
	 * @param descType
	 */
	public CptSVMIndDesc(String conceptName, DescriptorType descType) {
		super(conceptName,descType);
		kernelRBF=false;
		C=2.0f;
		gamma=0.5f;
	}
	
	public void setParamKernRBF(float C, float gamma) {
		this.C=C;
		this.gamma=gamma;
		this.kernelRBF=true;
	}
	
	public void setParamKernLin(float C) {
		this.C=C;
		this.kernelRBF=false;
	}


//	------------------------------------------------------------

	@Override
	public void setClassifier() {
		try {
			cls = new SMO();
			String options;
			options  = " -C "+C;		// C param 	2 is a good value
			options += " -L 0.0010";	// Learning Rate ...??? (Should not be changed)
			options += " -P 1.0E-12"; 	// Epsilon				(Should not be changed)
			options += " -N 0";			// {0=Normalize,1=Standardize,2=None}
			options += " -V -1";		// Number of Folds
			options += " -W 1";			// Tolerance Param ...???
			options += " -M"; 			// Probabilistic output	
			if(kernelRBF) //Gamma with 0.5 works well
				options += " -K \"weka.classifiers.functions.supportVector.RBFKernel -C 250007 -G "+gamma+"\"";
			else
				options += " -K \"weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0\"";

			String[] optVec = Utils.splitOptions(options);
			String debugTxt ="SMO -C "+C;
			if(kernelRBF) debugTxt+=" RBFKernel -G "+gamma;
			cls.setOptions(optVec);
			WekaTools.println("Creation of Classifier done ("+debugTxt+")",true);
		} catch (Exception e) {
			WekaTools.println("Setting the paramaters of classifier not done",false);
			return;
		}
		isLoaded=true;
	}


//	------------------------------------------------------------


	@Override
	protected String getClsFPath() {
		return WekaTools.pathClsIndDescFile+getClsName()+WekaTools.extFClassifier;
	}

	@Override
	public String getClsName() {
		boolean kernelRBF=false;
		String fName = ""; 
		fName += WekaTools.getPatternFName(conceptName, descType);
		fName += "_SMO-";
		if(kernelRBF) fName+="RBF";
		else fName+="Poly";
		return fName;	
	}
}
