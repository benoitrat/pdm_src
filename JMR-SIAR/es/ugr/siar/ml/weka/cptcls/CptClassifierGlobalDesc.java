/**
 * 
 */
package es.ugr.siar.ml.weka.cptcls;

import weka.classifiers.bayes.BayesNet;
import weka.classifiers.bayes.NaiveBayes;
import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.weka.WekaTools;
import es.ugr.siar.ml.weka.cptinst.CptInstGlobal;
import es.ugr.siar.ml.weka.cptinst.CptInstances;

/**
 * The object that model the CptClassifier on Global Descriptor.
 * 
 * <p>The classifier used are NaiveBayes and BayesNet</p>
 * 
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 19 janv. 08
 *
 */

public class CptClassifierGlobalDesc extends CptClassifier {

	public static enum GlobalInstType {
		FullProba, MixedProba, MixedDist; 
	}
	
	/** The descriptor used to get the values */
	private DescriptorType[] descTypes;
	
	/** If we use bayes net or naivebayes */
	private boolean isBayesNet;



//	-----------------------------------------------------------

	/**
	 * Constructor of the Train Independant Descriptor eXperiment.
	 * 
	 * @param conceptName
	 * @param descTypes
	 */
	public CptClassifierGlobalDesc(String conceptName, DescriptorType descTypes[]) {
		super(conceptName);
		this.descTypes = descTypes;
		isBayesNet=false;
	}
	
	
	public static CptClassifierGlobalDesc getCptClsGlobal(CptInstances cptInst) {
		if(cptInst instanceof CptInstGlobal) {
			CptInstGlobal cptInstGlob = (CptInstGlobal)cptInst; 
			CptClassifierGlobalDesc cptClsGlo = new CptClassifierGlobalDesc(cptInst.getConceptName(),cptInstGlob.getDescTypes());
			String path="";
			switch(cptInstGlob.getCptInstGlobalType()) {
				case MultiDesc: 
					path=WekaTools.pathClsMultiDescFile; break;
				case MixedDist:
					path=WekaTools.pathClsDistDescFile;	break;
				case MixedProba:
					path=WekaTools.pathProbaClsFile; break;
				case FullProba:
					path=WekaTools.pathClsFullProbaFile; break;
				default:
					return null;
			}
			cptClsGlo.setClsPath(path+cptInstGlob.getFName(null)+"_"+cptClsGlo.getClsType()+WekaTools.extFClassifier);
			return cptClsGlo;
		}
		WekaTools.println("Instance is not compatible with CptGlobalClassifier", false);
		return null;
	}


//	------------------------------------------------------------

	@Override
	public void setClassifier() {
		if(!isBayesNet) { 
			cls = new NaiveBayes();
		}
		else {
			cls = new BayesNet();
			String opts="weka.classifiers.bayes.BayesNet";
			opts+=" -D ";
			opts+=" -Q weka.classifiers.bayes.net.search.local.K2 -- -P 3 -S BAYES ";
			opts+=" -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5";
			setClsOptions(opts);
		}
		isLoaded=true;
	}

	
//	------------------------------------------------------------
	
	protected String getClsFPath() {
		if(clsPath != null) return clsPath;
		return WekaTools.pathProbaClsFile+getClsName()+WekaTools.extFClassifier;
	}

	public String getClsName() {
		String fName = ""; 
		fName += WekaTools.getPatternFName(conceptName, descTypes);
		fName +="_"+getClsType();
		return fName;	
	}
	
	protected String getClsType() {
		if(isBayesNet)
			return "BayesNet";
		else
			return "NaiveBayes";
	}


}
