/**
 * 
 */
package es.ugr.siar.ml.weka.cptcls;

import java.io.File;
import java.util.Random;

import es.ugr.siar.ml.weka.WekaTools;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.evaluation.ThresholdCurve;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.*;

/**
 * The object that model the classifier on a concept.
 * 
 * This class contains function for : 
 * <ul> 
 * <li> Creating a Classifier using Weka library </li>
 * <li> Obtain data using a list of photoID and a concept</li>
 * <li> Learned a concept defined by className </li>
 * <li> Return statistics, classification and distribution </li>
 * <li> Save this classifier </li>
 * <li> Load this classifier </li>
 * </ul>
 * 
 * 
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 19 janv. 08
 *
 */

abstract public class CptClassifier {

	/** The concept's name which is learned */
	protected String conceptName;

	/** The classifier used */
	protected Classifier cls;

	/** Name put to the classifier */
	String clsPath = null;

	/** tag : classifier correctly loaded */
	protected boolean isLoaded;

	/** tag : classifier correctly trained */
	protected boolean isTrained;

	/** tag : to know if we need to overwrite the classifier if it exist a serialized classifier */
	protected boolean isOverWritable;



//	-----------------------------------------------------------

	/**
	 * Constructor of the Train Independant Descriptor eXperiment.
	 * 
	 * @param conceptName
	 * @param descType
	 */
	public CptClassifier(String conceptName) {
		super();
		this.conceptName = conceptName;
		isLoaded=false;
		isOverWritable=false;
	}


//	------------------------------------------------------------


	/**
	 * Return the probabilities that of each instance to
	 * be classified as {@link #conceptName}.
	 * 
	 * <p><b>The file need to be in as the last field in this nominal format: 
	 * <code>@attribute class {Others,Sunset}</code> (Others=0,Sunset=1)
	 * </b></p>
	 * 
	 * <p>
	 * For more details look at :<ul>
	 * <li>{@link Instance#classValue()} give the concept ID. </li>
	 * <li> {@link Classifier#classifyInstance(Instance)} give the concept ID 
	 * that the classifer as selected.</li>
	 * <li> {@link Classifier#distributionForInstance(Instance)} return an array where 
	 * we have the probabilitie to belong to the concept i at index i.</li>
	 * </ul>
	 *  
	 * @param dataSet
	 * @return the array of probabilities.
	 */
	public double[] computeDistribution(Instances dataSet) {
		if(clsNotAvailable() || dataSet == null) return null;
		double[] rv = null;
		try {
			WekaTools.println("Start evaluating distribution",true);
			Evaluation eval = new Evaluation(dataSet);
			eval.evaluateModel(cls, dataSet);

			rv = new double[dataSet.numInstances()];
			double[] distribIns;

			for(int i=0;i<rv.length;i++) {
				//Compute the class distribution of instance i
				distribIns = cls.distributionForInstance(dataSet.instance(i));
				//Return the probability to belong to concepts[1] (which is equal to this.conceptName)
				rv[i] = distribIns[1];
			}
		}
		catch (Exception e) {
			WekaTools.println("Evaluation of distribution on "+dataSet.relationName()+" is impossible",false);
		}
		return rv;

	}

	public double[] computePrediction(Instances dataSet) {
		if(clsNotAvailable() || dataSet == null) return null;
		double[] rv = null;

		try {
			Evaluation eval = new Evaluation(dataSet);
			eval.evaluateModel(cls, dataSet);
			rv = new double[dataSet.numInstances()];

			for(int i=0;i<rv.length;i++) {
				//Obtain the class prediction of instance i
				rv[i] = cls.classifyInstance(dataSet.instance(i));
			}
		}
		catch (Exception e) {
			WekaTools.println("Evaluation of classification on "+dataSet.relationName()+" is impossible",false);
		}
		return rv;

	}

//	-------------------------------------------------------------

	abstract public void setClassifier();

	public void setFilteredClassifier(String opts) {
		cls = new FilteredClassifier();
		setClsOptions(opts);
	}

	protected void setClsOptions(String opts) {
		if(isLoaded) {
			try {
				String[] optVec = Utils.splitOptions(opts);
				cls.setOptions(optVec);
				WekaTools.println("Creation of Classifier done",true);
			} catch (Exception e) {
				WekaTools.println("Setting the paramaters of classifier not done",false);
				return;
			}	
		}
	}

	public void trainClassifier(Instances dataTrain) {
		if(!isLoaded || dataTrain == null) return;

		//Building classifier
		try {
			WekaTools.println("Start training the classifier",true);
			cls.buildClassifier(dataTrain);
			WekaTools.println("Classifier trained",true);
		} catch (Exception e) {
			WekaTools.println("Error during training (check the dataTrain instances)",false);
		}
		isTrained=true;
	}

	public Evaluation trainCValidClassifier(Instances dataTrain,int nofFold) {
		if(!isLoaded || dataTrain == null) return null;
		Evaluation eval=null;
		//Building classifier
		try {
			WekaTools.println("Start CrossValidation Evaluation (NofInstances="+dataTrain.numInstances()+") ",true);
			cls.setDebug(true);
			eval = new Evaluation(dataTrain);
			eval.crossValidateModel(cls, dataTrain, nofFold, new Random(1));
		}
		catch(Exception e) {
			WekaTools.println("Error in CV evaluation", false);
			return null;
		}
		WekaTools.println("Classifier trained",true);
		isTrained=true;
		return eval;
	}

	public String getResults(Instances dataSet) {
		if(clsNotAvailable() || dataSet == null) return null;
		String rv =null;

		try {
			Evaluation eval = new Evaluation(dataSet);
			eval.evaluateModel(cls, dataSet);
			rv = eval.toClassDetailsString();
		}
		catch (Exception e) {
			WekaTools.println("Evaluation of classification on "+dataSet.relationName()+" is impossible",false);
		}
		return rv;
	}

	public Evaluation getEvalution(Instances dataSet) {
		if(clsNotAvailable() || dataSet == null) return null;
		Evaluation eval=null;
		try {
			eval = new Evaluation(dataSet);
			eval.evaluateModel(cls, dataSet);
		}
		catch (Exception e) {
			WekaTools.println("Evaluation of classification on "+dataSet.relationName()+" is impossible",false);
		}
		return eval;
	}

	
	/**
	 * Default function to get ROC Evaluation.
	 * 
	 * The class that we examine is always set to 1. 
	 * The instance return contains:
	 *<ul>
	 * <li>#0 'True_Positives'		: The number of TP (Class=1,Predic=1)</li>
	 * <li>#1 'False Negatives'	: The number of FN (Class=1,Predic=0) </li>
	 * <li>#2 'False Positives' 	: The number of FP (Class=0,Predic=1) </li>
	 * <li>#3 'True Negatives'		: The number of TN (Class=0,Predic=0) </li>
	 * <li>#4 'False Positive Rate': FP_rate ([0,1]) used in ROC curves</li>
	 * <li>#5 'True Positive Rate'	: TP_rate ([0,1]) used in ROC curves </li>
	 * <li>#6 Precision</li><li>#7 Recall</li><li>#8 Fallout</li><li>#9 FMeasure</li><li>#10 Threshold</li>
	 * </ul>
	 * 
	 * @param dataSet 	the training instances.
	 * @return Instance with the attributes describe upper.
	 */
	public Instances getROCEvaluation(Instances dataSet) {
		return getROCEvaluation(dataSet,1);
	}

	public Instances getROCEvaluation(Instances dataSet,int classIndex) {
		Evaluation eval = getEvalution(dataSet);
		ThresholdCurve tc = new ThresholdCurve();
		
		return tc.getCurve(eval.predictions(), classIndex);
	}

//	-------------------------------------------------------------

//	-------------------------------------------------------------


	public void loadCls() {
		if(!isOverWritable)
			loadClsFromFile();
		else {
			WekaTools.println("Not implemented",false);
			//saveClsToFile();
		}
		//We should do another method that can load only if the file doesn't exist
	}


	public void loadClsFromFile() {
		try { 
			cls = (Classifier)weka.core.SerializationHelper.read(getClsFPath());
			isLoaded=true;
			isTrained=true;
			WekaTools.println(getClsName()+" correctly loaded", true);
		}
		catch (Exception e) {
			WekaTools.println(getClsFPath()+" can't be loaded", false);
		}
	}


	public void saveClsToFile() {
		if(clsNotAvailable()) return;
		try {
			if(!isOverWritable) {
				File tmp = new File(getClsFPath());
				if(tmp.exists()) {
					WekaTools.println("Can't overwrite existing file "+getClsName(), false);
					return;
				}
			}
			weka.core.SerializationHelper.write(getClsFPath(), cls);
			WekaTools.println(getClsName()+" written", true);
		}
		catch (Exception e) {
			WekaTools.println(getClsFPath()+" can't be written", false);
		}
	}


//	-------------------------------------------------------------


	abstract protected String getClsFPath();

	abstract public String getClsName();


//	-------------------------------------------------------------


	public Classifier getClassifier() {
		if(cls == null) System.err.println("The Classifier could not be load");
		return cls;
	}

	public boolean isLoaded() {
		return isLoaded;
	}

	public boolean isTrained() {
		return isTrained;
	}

	protected boolean clsNotAvailable() {
		if(!isTrained) WekaTools.println("Classifier is not trained", false);
		return !isTrained;
	}


//	-------------------------------------------------------------

	public void setOverWritable(boolean overWrite) {
		isOverWritable=overWrite;
	}

	public void setClsPath(String clsPath) {
		this.clsPath=clsPath;
	}

}
