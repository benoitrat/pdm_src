/**
 * 
 */
package es.ugr.siar.ml.weka.cptcls;

import weka.classifiers.bayes.BayesNet;
import weka.classifiers.bayes.net.estimate.BayesNetEstimator;
import weka.classifiers.bayes.net.estimate.SimpleEstimator;
import weka.classifiers.bayes.net.search.SearchAlgorithm;
import weka.classifiers.bayes.net.search.local.K2;
import weka.core.Utils;
import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.weka.WekaTools;

/**
 * The object that model a CptClassifier on Independant Descriptor.
 * 
 * <p>The classifiers by default used are two SVM with LinearKernel or RBF Kernel</p> 
 * 
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 19 janv. 08
 *
 */

public class CptBayesNetIndDesc extends CptClassifierIndDesc {


	int nofLevels;

	boolean ADTree;


//	-----------------------------------------------------------

	/**
	 * Constructor of the CptClassifier on Independant Descriptor.
	 * 
	 * <p>By default the classifier is a linear SVM kernel</p>
	 * 
	 * @param conceptName
	 * @param descType
	 */
	public CptBayesNetIndDesc(String conceptName, DescriptorType descType) {
		super(conceptName,descType);
		nofLevels=2;
		ADTree=true;
	}


//	------------------------------------------------------------

	@Override
	public void setClassifier() {
		try {
			BayesNet tmp = new BayesNet();
			tmp.setUseADTree(ADTree);
			
			BayesNetEstimator bnEstim= new SimpleEstimator();
			bnEstim.setAlpha(0.5);
			
			SearchAlgorithm bnSearch = new K2();
			String options="-P "+nofLevels+" -mbc -S BAYES ";
			String[] optVec = Utils.splitOptions(options);
			bnSearch.setOptions(optVec);
			
			tmp.setEstimator(bnEstim);
			tmp.setSearchAlgorithm(bnSearch);
			
			this.cls = tmp;
			
			WekaTools.println("Creation of Classifier done",true);
		} catch (Exception e) {
			WekaTools.println("Setting the paramaters of classifier not done",false);
			return;
		}
		isLoaded=true;
	}


//	------------------------------------------------------------


	@Override
	protected String getClsFPath() {
		return WekaTools.pathClsIndDescFile+getClsName()+WekaTools.extFClassifier;
	}

	@Override
	public String getClsName() {
		String fName = ""; 
		fName += WekaTools.getPatternFName(conceptName, descType);
		fName += "_BayesNet-"+nofLevels;
		return fName;	
	}
}
