/**
 * 
 */
package es.ugr.siar.ml.weka.cptcls;

import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.weka.WekaTools;

/**
 * The object that model a CptClassifier on Independant Descriptor.
 * 
 * <p>The classifiers by default used are two SVM with LinearKernel or RBF Kernel</p> 
 * 
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 19 janv. 08
 *
 */

abstract public class CptClassifierIndDesc extends CptClassifier {


	/** The descriptor used to get the values */
	protected DescriptorType descType;

//	-----------------------------------------------------------

	/**
	 * Constructor of the CptClassifier on Independant Descriptor.
	 * 
	 * <p>By default the classifier is a linear SVM kernel</p>
	 * 
	 * @param conceptName
	 * @param descType
	 */
	public CptClassifierIndDesc(String conceptName, DescriptorType descType) {
		super(conceptName);
		this.descType=descType;
	}

	public static CptClassifierIndDesc getInstance(String conceptName,DescriptorType descType) {
		CptSVMIndDesc cls;
		switch(descType) {
		case I_M7CSD:
			 cls = new CptSVMIndDesc(conceptName,descType);
			 cls.setParamKernRBF(8.0f, 0.5f); //Find using gridSearch
			 return cls;
		case I_M7SCD:
			cls = new CptSVMIndDesc(conceptName,descType);
			cls.setParamKernRBF(8.0f, 0.5f); //Find using gridSearch
			return cls;
		case I_M7EHD:
			cls = new CptSVMIndDesc(conceptName,descType);
			cls.setParamKernRBF(8.0f, 0.5f); //Find using gridSearch
			return cls;
		case C_EXF:
			return new CptBayesNetIndDesc(conceptName,descType);
		default:
			return null;
		}


	}

//	------------------------------------------------------------


	@Override
	protected String getClsFPath() {
		return WekaTools.pathClsIndDescFile+getClsName()+WekaTools.extFClassifier;
	}

	@Override
	abstract public String getClsName();
}
