/**
 * 
 */
package es.ugr.siar.ml.weka;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;
import weka.classifiers.Evaluation;

import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.weka.cptcls.CptSVMIndDesc;
import es.ugr.siar.ml.weka.cptinst.CptInstDesc;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 6 févr. 08
 *
 */
public class ParamSearchSVM {

	//Create the printStream
	PrintStream pStream;
	DescriptorType[] descTypes;
	String cptName;
	CptInstDesc cptInstDesc;

	float c_start;
	float c_end;
	float c_step;
	float gamma_start;
	float gamma_end;
	float gamma_step;

	float g_start_tmp;
	float c_start_tmp;

	int nofFold;
	int seed;


	/**
	 * @param cptName
	 * @param descType
	 * @param c_start
	 * @param c_end
	 * @param gamma_start
	 * @param gamma_end
	 */
	public ParamSearchSVM(String cptName, DescriptorType[] descTypes) {
		this.cptName = cptName;
		this.descTypes = descTypes;
		setGridParamC(-3,7,2);
		setGridParamGamma(-7,3,2);
		nofFold=10;
	}



	/**
	 * Set paramaters for C search.
	 * 
	 * Coarse search recommendation (-3,7,2).
	 *  
	 * 
	 * @param start 
	 * @param end
	 * @param step
	 */
	public void setGridParamC(float start, float end, float step) {
		this.c_start=start;
		this.c_end=end;
		this.c_step=step;
	}

	/**
	 * Set paramaters for Gamma search.
	 * 
	 * Coarse search recommendation (-7,3,2).
	 *  
	 * 
	 * @param start 
	 * @param end
	 * @param step
	 */
	public void setGridParamGamma(float start, float end, float step) {
		this.gamma_start=start;
		this.gamma_end=end;
		this.gamma_step=step;
	}

	public void setNofFold(int nofFold) {
		this.nofFold=nofFold;
	}

	public void start() {
		for(DescriptorType descType:descTypes) {
			descSearchRound(descType);
		}
	}


	protected void descSearchRound(DescriptorType descType) {
		g_start_tmp=gamma_start;
		c_start_tmp=c_start;

		cptInstDesc = new CptInstDesc(cptName,descType,SetsFileTools.SetType.TRAIN);
		cptInstDesc.loadInstances();
		cptInstDesc.resample(1.0,33);
		PrintStream pStream = getPrintStream(descType);
		if(pStream == null) {
			WekaTools.println("File can't be load. (file previously exist)",false);
			return;
		}
		long time = System.currentTimeMillis();
		Evaluation eva;

		for(float c=c_start_tmp; c<=c_end;c+=c_step) {
			float cPow = (float)Math.pow(2, c);
			for(float g=g_start_tmp; g<=gamma_end;g+=gamma_step) {
				long timeDesc = System.currentTimeMillis();
				float gammaPow=(float)Math.pow(2,g);
				WekaTools.println("C:2^"+c+"="+cPow+", gamma:2^"+g+"="+gammaPow, true);
				CptSVMIndDesc cptCls = new CptSVMIndDesc(cptName,descType);
				cptCls.setParamKernRBF(cPow,gammaPow);
				cptCls.setClassifier();
				eva = cptCls.trainCValidClassifier(cptInstDesc.getInstances(), nofFold);
				printLine(pStream,c,g,eva);
				System.out.println("Time spend (m) "+(System.currentTimeMillis()-timeDesc)/(60.0*1000));

			}
			g_start_tmp=gamma_start;
		}
		pStream.close();
		time=System.currentTimeMillis()-time;
		WekaTools.println("Total Time (m)"+time/(60.0*1000.0), true);
	}

	protected void printLine(PrintStream pStream,float c, float g, Evaluation eva) {
		if(eva != null) 
			pStream.print(c+"\t"+g+"\t"+eva.truePositiveRate(1)+"\t"+eva.falsePositiveRate(1)+"\t"+eva.kappa()+"\t"+eva.fMeasure(1)+"\n");
	}


	protected PrintStream getPrintStream(DescriptorType descType) {
		//Create the printStream
		String fileAppend="";
		PrintStream pStream;
		try {
			if(fileExist(descType)) {
				Scanner scanner=new Scanner(new File(getFPath(descType)));

				String line="";
				//Iterate over each line
				while (scanner.hasNextLine()) {
					line = scanner.nextLine();
					fileAppend+=line+"\n";
				}
				if(line != "") {
					String[] tmp = line.split("[\t]");

					c_start_tmp=new Float(tmp[0]);
					g_start_tmp=new Float(tmp[1])+gamma_step;

					System.out.println("Append files from : c="+c_start_tmp+", g="+g_start_tmp);
				}
				scanner.close();

			}
			pStream = new PrintStream(getFPath(descType));
			pStream.print(fileAppend);

		} catch (FileNotFoundException e) {
			System.err.println("Files "+getFName(descType)+" can not be created");
			e.printStackTrace();
			return null;
		}
		return pStream;
	}

	public boolean fileExist(DescriptorType descType) {
		File f = new File(getFPath(descType));
		return f.exists();
	}


	public String getFName(DescriptorType descType) {
		return cptName+"_"+descType.getAcronyms(3)+"_SVMRBF_"+c_start+"-"+c_end+"_"+gamma_start+"-"+gamma_end+".grid";
	}

	public String getFPath(DescriptorType descType) {
		return my.Config.dbClassfierFilePath+"gridSearch"+my.Config.OS_Slash+getFName(descType);
	}



}
