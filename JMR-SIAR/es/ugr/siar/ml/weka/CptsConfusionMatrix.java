/**
 * 
 */
package es.ugr.siar.ml.weka;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.weka.cptcls.CptClassifier;
import es.ugr.siar.ml.weka.cptcls.CptClassifierGlobalDesc;
import es.ugr.siar.ml.weka.cptinst.CptInstFullProba;
import es.ugr.siar.ml.weka.cptinst.CptInstances;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 8 févr. 08
 *
 */
public class CptsConfusionMatrix {

	
	protected String[] cptNames;
	protected int size;
	protected List<List<Set<Integer>>> list;
	protected DescriptorType[] descTypes;

	protected double[][][] prediction;
	protected double[][][] distribution;
	protected double[][] cptPhotoIdMx;
	
	protected boolean isComputed=false;
	
	/**
	 * @param cptNames
	 */
	public CptsConfusionMatrix(String[] cptNames) {
		super();
		this.cptNames = cptNames;
		size=cptNames.length;
		descTypes = WekaTools.getFullDescTypes();
	}
	
	
	public double[][] getConfusionMx() {
		compute();
		//Set<Double> setCptFN;
		Set<Double> setCptTP; //To remove
		Set<Double> setOtherTP;
		double[][] confMx = new double[size][size];
		double tmp;
		
		for(int i=0;i<size;i++) {
			double nofInst = (double)prediction[i][i].length;
			setCptTP = getTP(i,i);
			for(int j=0;j<size;j++) {
				setOtherTP = getTP(i,j);
				setOtherTP.removeAll(setCptTP);
				tmp=(double)setOtherTP.size();
				if(i==j) {
					tmp=(double)setCptTP.size();
				}
				confMx[i][j] = tmp/nofInst;
			}
		}
		
		return confMx;
	}
	

	public void printConfusionMx() {
		double[][] scoreMx = getConfusionMx();
		scoreMxToLatex(cptNames, scoreMx);	
	}
	


	/**
	 * @param ds
	 * @param ds2
	 */
	private Set<Double> getTP(int cptId, int clsId) {
		return getSet(cptId,clsId,1);
	}
	
//	private Set<Double> getFN(int cptId, int clsId) {
//		return getSet(cptId,clsId,0);
//	}
	
	private Set<Double> getSet(int cptId, int clsId, double nominalVal) {
		Set<Double> set = new HashSet<Double>();
		
		double[] photoIdVec = cptPhotoIdMx[cptId];
		double[] predicVec = prediction[cptId][clsId];
		
		//Look at the prediction for the concept i (cptId) trained by the classifier j  (clsId) 
		for(int k=0;k<predicVec.length;k++) {
			if(predicVec[k] == nominalVal) {
				set.add(photoIdVec[k]);
			}
		}
		return set;
	}

	
	public void recompute() {
		isComputed=false;
		compute();
	}

	public void compute() {
		if(isComputed) return;
		
		
		prediction = new double[size][size][];
		distribution = new double[size][size][];
		cptPhotoIdMx = new double[size][];
		CptClassifier[] cptClsVec = new CptClassifier[size];
		
		
		//Preload the classifier with the instance name
		for(int j=0;j<size;j++) {
			CptInstances cptInst = new CptInstFullProba(cptNames[j],cptNames,descTypes,SetsFileTools.SetType.TEST);
			cptClsVec[j] = CptClassifierGlobalDesc.getCptClsGlobal(cptInst);
			cptClsVec[j].loadCls();
		}
		
		
		//Iterate over concept C_i
		for(int i=0;i<size;i++) {
			
			//Load the corresponding test instances.
			CptInstances cptInst = new CptInstFullProba(cptNames[i],cptNames,descTypes,SetsFileTools.SetType.TEST);
			cptInst.loadInstancesFromFile();
			
			//Filter the concept other to have only C_i 
			if(!cptInst.isLoaded()) continue;
			cptInst.removeOneClass(true);
			cptPhotoIdMx[i] = Arrays.copyOfRange(cptInst.getPhotosID(),0,cptInst.getInstances().numInstances());
			
			//Iterate over concept C_j
			for(int j=0;j<size;j++) {	
				prediction[i][j] = cptClsVec[j].computePrediction(cptInst.getInstances());
				distribution[i][j] = cptClsVec[j].computeDistribution(cptInst.getInstances());
			}
			System.out.println();
		}
		isComputed=true;
	}
	

	
	public static void scoreMxToLatex(String[] cNames, double[][] scoreMx) {
		String header ="";
		header +="\\footnotesize\n";
		header += "\\newcommand{\\sep}{\\hspace{0.6ex};\\hspace{0.6ex}}\n";
		header +="\\begin{tabular}{| l ";
		String colName ="{\\large \\strut}$_i\\diagdown^j$ ";
		String lines="";
		//Build Latex Matrix

		for(int i=0;i<cNames.length;i++) {
			colName+="&"+cNames[i].substring(0,3)+"   ";
			header+= "@{\\sep} c ";	
			
			double[] tmp= Arrays.copyOf(scoreMx[i],scoreMx[i].length);
			Arrays.sort(tmp);
			double max=tmp[tmp.length-1];
			double thrs=tmp[tmp.length-2];
			thrs*=0.90;
			
			lines+="{\\large \\strut}";
			lines+=cNames[i].substring(0,3)+"\t";	
			for(int j=0;j<cNames.length;j++) {
				String format="%1.1f";
				if(scoreMx[i][j] > 0.099) format="%3.0f";
				String scoreStr=new my.PrintfFormat(format).sprintf(100*scoreMx[i][j]);
				if(scoreMx[i][j] >= thrs && scoreMx[i][j] < max)
					lines+="   &\\textbf{"+scoreStr+"} ";
				else
					lines+="   &"+scoreStr+" ";
			}
			lines+="\\\\ \n";
		}
		header+=" |}\\hline \n";
		colName+="\\\\ \\hline\n";
		lines+="\\hline \n \\end{tabular}";
		
		System.out.println(header+colName+lines);
		
	}
		
}
