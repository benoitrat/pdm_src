/**
 * 
 */
package es.ugr.siar.ml.weka;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.Resample;

import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.SetsFileTools.SetType;
import es.ugr.siar.ml.weka.cptcls.CptClassifier;
import es.ugr.siar.ml.weka.cptcls.CptClassifierGlobalDesc;
import es.ugr.siar.ml.weka.cptinst.CptInstGlobal;
import es.ugr.siar.ml.weka.cptinst.CptInstances;
import es.ugr.siar.ml.weka.cptinst.CptInstGlobal.CptInstGlobalType;
import es.ugr.siar.tools.FilePrintTools;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 9 févr. 08
 *
 */
public class CptsROCCurve {

	//Create the printStream
	PrintStream pStream;
	
	String[] cptNames;
	String[] cptNamesTrained;
	CptInstGlobalType globInstType;
		
	/**
	 * @param cptInstTrain
	 * @param cptInstTest
	 */
	public CptsROCCurve(String[] cptNames,String cptNamesTrained[], CptInstGlobalType globInstType) {
		super();
		this.cptNames=cptNames;
		this.cptNamesTrained=cptNamesTrained;
		this.globInstType=globInstType;
	}
	
	public CptsROCCurve(String cptName, CptInstGlobalType globInstType) {
		this(new String[]{cptName},SetsFileTools.readGlobalClassNames(),globInstType);
	}
	public CptsROCCurve(String[] cptNames, CptInstGlobalType globInstType) {
		this(cptNames,SetsFileTools.readGlobalClassNames(),globInstType);
	}

	
	public void startROC() {
		for(String cptName : cptNames) {
			CptInstances test = CptInstGlobal.getCptInstGlobal(cptName, cptNamesTrained,
					WekaTools.getFullDescTypes(), 
					SetType.TEST, globInstType);
			CptInstances train = CptInstGlobal.getCptInstGlobal(cptName, cptNamesTrained,
					WekaTools.getFullDescTypes(), 
					SetType.TRAIN, globInstType);
			
			double[][] mXROC = getROC(train,test);
			createPrintStream(test);
			FilePrintTools.printMx(pStream, mXROC);
			WekaTools.println("ROC curves printed in "+getFName(test), true);
		}
	}
	
	public double[][] getROC(CptInstances train, CptInstances test) {
		
		
		test.loadInstancesFromFile();
		CptClassifier cptCls = CptClassifierGlobalDesc.getCptClsGlobal(test);
		cptCls.loadCls();
		//Train if the classifier is not found.
		if(!cptCls.isLoaded()) {
			train.loadInstancesFromFile();
			cptCls.setClassifier();
			cptCls.trainClassifier(train.getInstances());
		}
		Instances instROC =  cptCls.getROCEvaluation(test.getInstances());
		Resample resampFilter = new Resample();
		double pct = 100.0*(1000.0/(double)instROC.numInstances());
		System.out.println(pct);
		try {
			resampFilter.setSampleSizePercent(pct);
			//resampFilter.setRandomSeed(1);
			resampFilter.setInputFormat(instROC);
			instROC = Filter.useFilter(instROC, resampFilter);
		} catch (Exception e) {
			WekaTools.println("Error while resampling",false);
		}
		
		
		int indRateFP=4,indRateTP=5;
		double[][] rv = new double[instROC.numInstances()][2];
		
		Instance rowInst;
		for(int i=0;i<instROC.numInstances();i++) {
			rowInst = instROC.instance(i);
			rv[i][0]=rowInst.value(indRateFP);
			rv[i][1]=rowInst.value(indRateTP);
		}
		return rv;
	}
	

	
	
	protected void createPrintStream(CptInstances cptInst) {
		//Create the printStream
		try {
			pStream = new PrintStream(getFPath(cptInst));

		} catch (FileNotFoundException e) {
			System.err.println("Files "+getFName(cptInst)+" can not be created");
			e.printStackTrace();
			return;
		}
	}
	
	
	public String getFName(CptInstances cptInst) {
		return "ROC_"+cptInst.getFName();
	}

	public String getFPath(CptInstances cptInst) {
		return my.Config.dbClassfierFilePath+"curvesROC"+my.Config.OS_Slash+getFName(cptInst);
	}
	
	
}
