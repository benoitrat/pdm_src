/**
 * 
 */
package es.ugr.siar.ml.weka.cptinst;

import java.io.File;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import es.ugr.siar.db.APNDBManager;
import es.ugr.siar.ip.desc.DescriptorValues;
import es.ugr.siar.ip.desc.ExifDescriptor;
import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.DataManager;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.SetsFileTools.SetType;
import es.ugr.siar.ml.weka.WekaTools;
import es.ugr.siar.tools.ArraysTools;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 19 janv. 08
 *
 */
public class CptInstDesc extends CptInstances {


	/** The descriptor used to get the values */
	private DescriptorType descType;
	
	
//	-------------------------------------------------------------
	
	/**
	 * @param conceptName
	 * @param descType
	 * @param setsType
	 */
	public CptInstDesc(String conceptName, DescriptorType descType, SetType setsType) {
		super(conceptName,setsType);
		this.descType=descType;
	}
	
//	-------------------------------------------------------------
	
	
	public boolean loadInstancesFromSQL() {

		boolean photoID = false;
		
		//Load the data manager
		DataManager datMan = new DataManager(
				new APNDBManager(),
				new String[]{"Others",conceptName},
				new DescriptorType[]{descType});
		datMan.setClassesPhotosIdFromFile(setsType);

		//make Instance header 
		Instances instances = makeInstancesHeader(datMan,photoID);
		Double[][] datMx= datMan.getDataInDoubleMx();
		double[] cptIdVec= datMan.getClassIdVec();
		double[] photoIdVec =datMan.getPhotoIdVec();
		
		for(int i=0;i<datMx.length;i++) {
			double[] vals = new double[instances.numAttributes()];
			
			int j=0;
			if(photoID) vals[j++]=photoIdVec[i];
			
			for(;j<datMx[i].length;j++) {
				vals[j] = datMx[i][j];
			}
			vals[j]=cptIdVec[i];
			instances.add(new Instance(1.0,vals));
		}
		this.dataSet= instances;
		setConceptAttribute();
		isLoaded=true;
		
		File fpicId = getFilePhotoID();
		SetsFileTools.writePhotoID(fpicId, ArraysTools.toPrimitiveIVec(photoIdVec));
		
		WekaTools.println(getFName()+" dataSet is created",true);
		return true;
	}
	
	
	public double[] getPhotosID() {
		
		File f = getFilePhotoID();
		if(f.exists()) {
			int[] photosID = SetsFileTools.readPhotosId(f);
			return ArraysTools.toPrimiviteDVec(photosID);
		}
		else {
			//Load the data manager
			DataManager datMan = new DataManager(
					new APNDBManager(),
					new String[]{"Others",conceptName},
					new DescriptorType[]{descType});
			datMan.setClassesPhotosIdFromFile(setsType);
			
			double[] photosID = datMan.getPhotoIdVec();
			SetsFileTools.writePhotoID(f, ArraysTools.toPrimitiveIVec(photosID));
			return photosID;
		}
	}
	
	private Instances makeInstancesHeader(DataManager datMan,boolean photoID) {

		//Obtain the standard descriptor value for this descriptor
		FastVector atts = new FastVector();
		DescriptorValues dV = datMan.getFirstAvailableDescValues(descType);
		String[] attrHeader = dV.getValuesHeader();

		if(photoID) {
			atts.addElement(new Attribute("photoID"));
		}
		
		//Setup special nominal attribute for EXIF
		if(descType == DescriptorType.C_EXF) {
			ExifDescriptor eDesc = new ExifDescriptor();
			String[] nominals=null;
			// Setup attribute using the descriptor values attribute headers.
			for(int j=0;j<dV.getNofAttributes();j++) {
				nominals=eDesc.getNominals(j);
				if(nominals==null)
					atts.addElement(new Attribute(attrHeader[j]));
				else {
					//Building nominals
					FastVector nominalAttr = new FastVector();
					for(int k=0;k<nominals.length;k++) {
						nominalAttr.addElement(nominals[k]);
					}
					atts.addElement(new Attribute(attrHeader[j],nominalAttr));
				}
			}
		}
		else {
			//Setup attribute using the descriptor values attribute headers. 
			for(int i=0;i<dV.getNofAttributes();i++)
				atts.addElement(new Attribute(attrHeader[i]));
		}

		// Build the two nominal concepts
		FastVector nominalConcepts = new FastVector();
		nominalConcepts.addElement("Others");
		nominalConcepts.addElement(conceptName);
		atts.addElement(new Attribute("Class",nominalConcepts));

		return new Instances(getFName(),atts,0);
	}
	
//	-------------------------------------------------------------
	
	public String getFName() {
		String fName=WekaTools.getPatternFName(conceptName, descType);
		fName+="_"+setsType.toString();
		return fName;
	}
	
	public DescriptorType getDescType() {
		return descType;
	}
	
	public void keepOnlyDesc(DescriptorType[] descTypes) {
		WekaTools.println("This method is not usefull for independant desc", false);
	}

	@Override
	public String getFPath() {
		return WekaTools.pathInstIndDesc+descType.getAcronyms(3)+my.Config.OS_Slash+getFName()+getExtension();
	}

	
}
