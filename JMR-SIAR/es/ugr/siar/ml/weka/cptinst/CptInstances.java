/**
 * 
 */
package es.ugr.siar.ml.weka.cptinst;

import java.io.File;
import java.io.IOException;

import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.supervised.instance.Resample;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.instance.RemoveWithValues;
import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.SetsFileTools.SetType;
import es.ugr.siar.ml.weka.WekaTools;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 19 janv. 08
 *
 */
abstract public class CptInstances {

	/** The values coming from this concepts and the others Concepts */
	protected String conceptName;
	
	/** The type of value the files are coming from */
	protected SetsFileTools.SetType setsType;
	
	/** The files with the dataSet */
	protected Instances dataSet;
	
	/** Tag: Instances are loaded correctly */
	protected boolean isLoaded;
	
	/** Tag: Instances are resampled */
	protected boolean isResampled;
	
	/** Tag: Instances contains ID */
	protected boolean isWithID;
	
	/** Tag: Instances is overwritable */
	protected boolean isOverWritable;
	
	
//	-------------------------------------------------------------
	
	/**
	 * @param conceptName
	 * @param descType
	 * @param setsType
	 */
	public CptInstances(String conceptName, SetType setsType) {
		this.conceptName = conceptName;
		this.setsType = setsType;
		isLoaded=false;
		isResampled=false;
		isOverWritable=false;
		isWithID=false;
	}
	
//	-------------------------------------------------------------
	
	/**
	 * load the Instances from a file if this one exist or from SQL.
	 * 
	 * @see #loadInstancesFromFile()
	 * @see #loadInstancesFromSQL()
	 */
	public void loadInstances() {
		
		//Check if we want to overwrite a possible existing file
		if(!isOverWritable) { 
			//First check if it exist a .arff with this instance
			loadInstancesFromFile();
		}
		//If this file is not found or can't be read
		if(isNotAvailable()) {
			//Load this instance from the SQL database.
			loadInstancesFromSQL();
			//Save the instance in a file to not redo all this operation
			saveInstancesInArff();
		}
	}
	
//	-------------------------------------------------------------

	public boolean loadInstancesFromFile() {

			DataSource sourceTrain;
			try {
				sourceTrain = new DataSource(getFPath());
				dataSet = sourceTrain.getDataSet();
			} catch (Exception e) {
				WekaTools.println(getFPath()+" not found",false);
				return false;
			}
			setConceptAttribute();
			isLoaded=true;
			WekaTools.println(getFName()+" Loaded",true);
			return true;
			
	}
	
	abstract public boolean loadInstancesFromSQL();
	
	abstract public double[] getPhotosID();
	
//	-------------------------------------------------------------
	
	public void saveInstancesInArff() {
		if(isResampled) {
			WekaTools.println(getFName()+" is Resampled and therefore not saved", false);
			return;
		}
		saveInstancesInArff(getFPath());
		
	}

	public void saveInstancesInArff(String fPath) {
		if(isNotAvailable()) return;
		
		try {
			ArffSaver saver = new ArffSaver();
			saver.setInstances(dataSet);
			saver.setFile(new File(fPath));
			saver.writeBatch();
			WekaTools.println(getFName()+" is written",true);
		} catch (IOException e) {
			WekaTools.println(fPath+" Error during writing",false);
			e.printStackTrace();
		}
	}
	
//	-------------------------------------------------------------
	
	public void resample() {
		if(isNotAvailable()) return;
		resample(1.0, 100.0);
	}
	
	public void resample(double bias, double pct) {
		if(isNotAvailable()) return;
		
		Resample resampFilter = new Resample();
		String[] options;
		try {
			options = Utils.splitOptions("-B "+bias+" -S 1 -Z "+pct);
			resampFilter.setOptions(options);
			resampFilter.setInputFormat(dataSet);
			dataSet = Filter.useFilter(dataSet, resampFilter);
			WekaTools.println(getFName()+" Resampled",true);
		} catch (Exception e) {
			WekaTools.println("Error while resampling",false);
			return;
		}
		isResampled=true;
	}
	
	
	public void removeOneClass(boolean isOthers) {
		if(isNotAvailable()) return;
		RemoveWithValues filter = new RemoveWithValues();
		filter.setAttributeIndex("last");
		if(isOthers)
			filter.setNominalIndices("first");
		else 
			filter.setNominalIndices("last");
		try {
			filter.setInputFormat(dataSet);
			dataSet = Filter.useFilter(dataSet, filter);
			WekaTools.println(getFName()+" Attribute Filtered",true);
		} catch (Exception e) {
			WekaTools.println("Error while filtering attribute",false);
			return;
		}
		
	}
	
	
	public void keepOnlyAttr(String rangeList) {
		if(isNotAvailable()) return;
		
		//weka.filters.unsupervised.attribute.
		Remove filter = new Remove();
		filter.setInvertSelection(true);
		filter.setAttributeIndices(rangeList);
		try {
			filter.setInputFormat(dataSet);
			dataSet = Filter.useFilter(dataSet, filter);
			WekaTools.println(getFName()+" Attribute Filtered",true);
		} catch (Exception e) {
			WekaTools.println("Error while filtering attribute",false);
			return;
		}
	}

	abstract public void keepOnlyDesc(DescriptorType[] descTypes);
	
	
	
	/**
	 * Set the last attribute as the concept or class.
	 * 
	 * This function only call at {@link Instances#setClassIndex(int)}
	 */
	protected void setConceptAttribute() {
		//If there is no concept defined it is always the last one.
		if (dataSet.classIndex() == -1)
			dataSet.setClassIndex(dataSet.numAttributes() - 1);
	}
	
	
//	-------------------------------------------------------------
	
	public String getFPath() {
		return WekaTools.pathInstArffFile+getFName()+getExtension();
	}
	
	abstract public String getFName();
	
	protected String getExtension() {
		return WekaTools.extFIntances;
	}

	public File  getFilePhotoID() {
		
		File fInst = new File(getFPath());
		return new File(fInst.getParent()+my.Config.OS_Slash+"."+getFName()+WekaTools.extFPhotoId);
	}
	
	
//	-------------------------------------------------------------
	
	public String getConceptName() {
		return conceptName;
	}
	

	public Instances getInstances() {
		if(isNotAvailable()) return null;
		return dataSet;
	}


	public SetsFileTools.SetType getSetsType() {
		return setsType;
	}
	
//	-------------------------------------------------------------
	
	protected boolean isNotAvailable() {
		if(!isLoaded)
			WekaTools.println(getFName()+" is not avaiable", false);
		return !isLoaded;
	}

	public boolean isLoaded() {
		return isLoaded;
	}
	

	public boolean isResample() {
		return isResampled;
	}
	
//	-------------------------------------------------------------
	
	public void setOverWritable(boolean overWrite) {
		isOverWritable=overWrite;
	}
	
	
	public double[] getClassIndexes() {
		return dataSet.attributeToDoubleArray(dataSet.numAttributes()-1);
	}
	
}
