/**
 * 
 */
package es.ugr.siar.ml.weka.cptinst;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import es.ugr.siar.db.APNDBManager;
import es.ugr.siar.ip.desc.DescriptorValues;
import es.ugr.siar.ip.desc.DescriptorValuesTools;
import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.DataManager;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.SetsFileTools.SetType;
import es.ugr.siar.ml.weka.WekaTools;
import es.ugr.siar.ml.weka.cptcls.CptClassifier;
import es.ugr.siar.ml.weka.cptcls.CptClassifierGlobalDesc;
import es.ugr.siar.ml.weka.cptinst.CptInstDesc;
import es.ugr.siar.tools.ArraysTools;

/**
 * The concept instances with all the descriptor joined.
 * 
 * 
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 18 janv. 08
 *
 */
public class CptInstMultiDesc extends CptInstGlobal {


	List<CptInstDesc> ciDescVec = new ArrayList<CptInstDesc>();
	int [] descStartIndex;

//	-------------------------------------------------------------

	/**
	 * Constructor of the <b>C</b>on<b>c</b>e<b>p</b>t <b>Inst</b>ances with multi <b>Desc</b>riptors. 
	 * 
	 * @param conceptName 			The concept that we will load with its others set to obtain the instances.
	 * @param descTypes				A vector with the type of descriptors used.
	 * @param setsType				The type of set (train or test) on which the instances are loaded.
	 */
	public CptInstMultiDesc(String conceptName,DescriptorType[] descTypes, SetType setsType) {
		super(conceptName,setsType,new String[]{conceptName},descTypes,CptInstGlobalType.MultiDesc);
		descStartIndex=new int[descTypes.length];
	}


	protected void loadCptInstDescList() {
		CptInstDesc ciDesc;
		for(DescriptorType descType : descTypes) {
			ciDesc= new CptInstDesc(conceptName,descType,setsType);
			ciDesc.loadInstances();
			ciDescVec.add(ciDesc);
		}
	}


	private void initDataManager() {
		dbMan = new APNDBManager();
		datMan = new DataManager(dbMan,new String[]{"Others",conceptName},descTypes);
		WekaTools.println("Init data manager", true);
		datMan.setClassesPhotosIdFromFile(setsType);
	}

//	-------------------------------------------------------------

	private void makeInstancesHeader() {
		if(isDatManNotAvailable()) return;

		int nofDesc= descTypes.length;

//		Obtain the standard descriptor value for this descriptor
		FastVector atts = new FastVector();
		FastVector attsTmp = new FastVector(); 

		if(isWithID) {
			atts.addElement(new Attribute("photoID"));
		}

		//Recover all the attributes for each descriptors
		for(int i=0;i<nofDesc;i++) {
			DescriptorValues dV = datMan.getFirstAvailableDescValues(descTypes[i]);
			attsTmp = DescriptorValuesTools.getFVInstance(dV);
			//Linearize the temporary attribute vector into a unique attributes vector
			for(int j=0;j<attsTmp.size();j++) {
				atts.addElement(attsTmp.elementAt(j));
			}
		}

		// Build the two nominal concepts
		FastVector nominalConcepts = new FastVector();
		nominalConcepts.addElement("Others");
		nominalConcepts.addElement(conceptName);
		atts.addElement(new Attribute("Class",nominalConcepts));

		dataSet = new Instances(getFName(),atts,0);
	}

	private void makeInstancesValue() {
		if(isDatManNotAvailable() || dataSet == null) return;

		Double[][] datMx= datMan.getDataInDoubleMx();
		double[] cptIdVec= datMan.getClassIdVec();
		double[] photoIdVec =datMan.getPhotoIdVec();

		for(int i=0;i<datMx.length;i++) {
			double[] vals = new double[dataSet.numAttributes()];

			int j=0;
			if(isWithID) vals[j++]=photoIdVec[i];

			for(;j<datMx[i].length;j++) {
				if(datMx[i][j] != Double.NaN)
					vals[j] = datMx[i][j];
			}
			vals[j]=cptIdVec[i];
			dataSet.add(new Instance(1.0,vals));
		}
		setConceptAttribute();
		isLoaded=true;
	}

//	-------------------------------------------------------------


	public boolean loadInstancesFromSQL() {

		//Initialize the size and the data Manager
		initDataManager();

		//Write the fileID used
		int[] photosID = ArraysTools.toPrimitiveIVec(datMan.getPhotoIdVec());
		SetsFileTools.writePhotoID(getFilePhotoID(), photosID);

		//Create the header of the instances set
		makeInstancesHeader();

		//Create the instance from the others classifier
		makeInstancesValue();

		return true;
	}


//	-------------------------------------------------------------


	public File getFilePhotoID() {
		return new File(WekaTools.pathInstMultiDesc+"."+getFName()+WekaTools.extFPhotoId);
	}

	public CptClassifier getCptClassifier() {
		return new CptClassifierGlobalDesc(conceptName,descTypes);
	}

	public CptClassifier getCptClassifier(String conceptName) {
		return new CptClassifierGlobalDesc(conceptName,descTypes);
	}


	/* (non-Javadoc)
	 * @see es.ugr.siar.ml.weka.cptinst.CptInstGlobal#getStartIndex(es.ugr.siar.ip.desc.DescriptorValues.DescriptorType)
	 */
	@Override
	protected int getStartIndex(DescriptorType descType) {
		// TODO Auto-generated method stub
		return 0;
	}


	/* (non-Javadoc)
	 * @see es.ugr.siar.ml.weka.cptinst.CptInstGlobal#keepOnlyDesc(es.ugr.siar.ip.desc.DescriptorValues.DescriptorType[])
	 */
	@Override
	public void keepOnlyDesc(DescriptorType[] filteredDescTypes) {
		if(isLoaded()) {
			String list="";
			descStartIndex=getDescStartIndex();
			
			for(DescriptorType fDescType : filteredDescTypes) {
				int ind=DescriptorType.getIndex(fDescType, descTypes);

				if(ind >= 0) {			
					list+=(descStartIndex[ind]+1)+"-";
					if(ind == descTypes.length-1) {
						list+=dataSet.numAttributes()-1;
					}
					else {
						list+=descStartIndex[ind+1]+1;
					}
					list+=",";
				}
			} //End for loop
			list +=  dataSet.numAttributes();
			keepOnlyAttr(list);
			this.descTypes=filteredDescTypes;
		}
	}

	@Override
	protected boolean specialDescriptor(DescriptorType descType) {
		// TODO Auto-generated method stub
		return false;
	}
	
	protected int[] getDescStartIndex() {
		int[] descStartIndex = new int[descTypes.length];
		initDataManager();
		DescriptorValues dV;
		
		int count=0;
		for(int i=0;i<descTypes.length;i++) {
			dV = datMan.getFirstAvailableDescValues(descTypes[i]);
			descStartIndex[i]=count;
			count+=dV.getNofAttributes();
		}
		
		
		return descStartIndex;
	}
}
