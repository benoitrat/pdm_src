/**
 * 
 */
package es.ugr.siar.ml.weka.cptinst;

import java.io.File;

import es.ugr.siar.db.APNDBManager;
import es.ugr.siar.db.DatabaseManager;
import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.DataManager;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.SetsFileTools.SetType;
import es.ugr.siar.ml.weka.WekaTools;
import es.ugr.siar.ml.weka.cptcls.CptClassifier;
import es.ugr.siar.tools.ArraysTools;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 21 janv. 08
 *
 */
public abstract class CptInstGlobal extends CptInstances {
	
	public static enum CptInstGlobalType {
		FullProba, MixedProba, MixedDist,MultiDesc;
		public String getSkeltonName() {
			switch(this) {
			case FullProba:
				return "FullProbaMx";
			case MixedProba:
				return "MixedProba";
			case MixedDist:
				return "MixedDist";
			case MultiDesc:
				return "MultiDesc";
			default:
				return null;
			}
		}
		public String getDirectory() {
			switch(this) {
			case FullProba:
				return WekaTools.pathInstFullProba;
			case MixedProba:
				return WekaTools.pathInstMixedProba;
			case MixedDist:
				return WekaTools.pathInstMixedDist;
			case MultiDesc:
				return WekaTools.pathInstMultiDesc;
			default:
				return null;
			}
		}
		
	}
	
	/** List of the descriptor used during training */
	protected DescriptorType descTypes[];
	protected int[] descStartIndex;
	
	
	/** List of the classNamesTrained */
	protected String classNamesTrained[];
	protected int nofClassTrained;

	/** Use the DataBase manager */
	protected DatabaseManager dbMan;
	
	/** Use the DataManager 	*/
	protected DataManager 	datMan;
	
	
	/** The type of global instance */
	protected CptInstGlobalType type;
	
//	-------------------------------------------------------------
	
	/**
	 * Constructor of the Concept Instances for global sets and descriptors.
	 * 
	 * @param classNameStat 		The concept that we will load with its others set to obtain the instances.
	 * @param classNamesTrained		The classifiers previously trained for these classes.
	 * @param descTypes				The type of descriptor used to fill this matrix.
	 * @param setType				The type of set (train or test) on which the instances are loaded.
	 */
	public CptInstGlobal(String conceptName, SetType setsType, 
			String[] classNamesTrained,DescriptorType[] descTypes,CptInstGlobalType type) {
		super(conceptName, setsType);
		this.classNamesTrained=classNamesTrained;
		this.descTypes=descTypes;
		this.nofClassTrained=classNamesTrained.length;
		this.descStartIndex=new int[descTypes.length+1];
		this.type=type;
	}
	
	public static CptInstGlobal getCptInstGlobal(String cptName,
			DescriptorType[] descTypes,SetType setsType,CptInstGlobalType type) {
		return getCptInstGlobal(cptName,new String[]{cptName}, descTypes, setsType, type);
	}
	
	
	public static CptInstGlobal getCptInstGlobal(String cptName,String[] cptNamesTrained,
			DescriptorType[] descTypes,SetType setsType,CptInstGlobalType type) {
		switch(type) {
		case FullProba:
			return new CptInstFullProba(cptName,cptNamesTrained,descTypes,setsType);
		case MixedProba:
			return new CptInstProba(cptName,cptNamesTrained,descTypes,setsType);
		case MixedDist:
			return new CptInstDist(cptName,cptNamesTrained,descTypes,setsType);
		case MultiDesc:
			return new CptInstDist(cptName,cptNamesTrained,descTypes,setsType);
		default:
				WekaTools.println("Problem while creating CptInstGlobal", false);
				return null;
		}
	}
	
	
	//	-------------------------------------------------------------
	
	
	protected boolean isDatManNotAvailable() {
		if(datMan == null || !datMan.isPhotoIDAvailable()) {
			WekaTools.println("DataManager is not available", false);
			return true;
		}
		return false;
	}
	
	protected boolean isInstNotAvailable() {
		return isNotAvailable();
	}
	
	public double[] getPhotosID() {
		
		File f = getFilePhotoID();
		if(f.exists()) {
			int[] photosID = SetsFileTools.readPhotosId(f);
			return ArraysTools.toPrimiviteDVec(photosID);
		}
		else {
			//Load the data manager
			DataManager datMan = new DataManager(
				new APNDBManager(),
				new String[]{"Others",conceptName},
				descTypes);
			datMan.setClassesPhotosIdFromFile(setsType);
			double[] photosID = datMan.getPhotoIdVec();
			SetsFileTools.writePhotoID(f, ArraysTools.toPrimitiveIVec(photosID));
			return photosID;
		}
	}
	
	abstract public File getFilePhotoID();
	
	
	protected String getNofGlobalClassNames() {
		if(nofClassTrained==1) return "1";
		else return "M";
	}
		
	public String getFName(SetType setType) {
		String fName=conceptName;
		if(type != CptInstGlobalType.MultiDesc)
			fName+="-"+getNofGlobalClassNames();
		fName+="-"+this.type.getSkeltonName()+"_";
		fName+=getDescList();
		if(setType != null) 
			fName+="-"+setType.toString();
		return fName;
	}
	
	protected String getDescList() {
		String rv="";
		for(DescriptorType descType: descTypes) {
			rv+=descType.getAcronyms(3);
		}
		return rv;
	}
	
	@Override
	public String getFName() {
		return getFName(this.setsType);
	}
	
	@Override
	public String getFPath() {
		return this.type.getDirectory()+getFName()+WekaTools.extFIntances;
	}
	
	public CptInstGlobalType getCptInstGlobalType() {
		return type;
	}
	
	public DescriptorType[] getDescTypes() {
		return descTypes;
	}
	
	//	-------------------------------------------------------------
	
	
	abstract protected int getStartIndex(DescriptorType descType);
	
	abstract protected boolean specialDescriptor(DescriptorType descType);
	
	abstract public void keepOnlyDesc(DescriptorType[] descTypes);
	
	abstract public CptClassifier getCptClassifier();
	
	abstract public CptClassifier getCptClassifier(String cptName);
	
	
	

}
