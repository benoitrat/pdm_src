/**
 * 
 */
package es.ugr.siar.ml.weka.cptinst;


import java.io.File;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import es.ugr.siar.db.APNDBManager;
import es.ugr.siar.ip.desc.DescriptorValues;
import es.ugr.siar.ip.desc.ExifDescriptor;
import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.DataManager;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.SetsFileTools.SetType;
import es.ugr.siar.ml.weka.WekaTools;
import es.ugr.siar.ml.weka.cptcls.CptClassifier;
import es.ugr.siar.ml.weka.cptcls.CptClassifierGlobalDesc;
import es.ugr.siar.ml.weka.cptcls.CptClassifierIndDesc;
import es.ugr.siar.ml.weka.cptinst.CptInstDesc;
import es.ugr.siar.tools.ArraysTools;

/**
 * The concept instances with the probabilities of all 
 * the concepts for each descriptors. 
 * 
 * 
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 18 janv. 08
 *
 */
public class CptInstProba extends CptInstGlobal {


	/** The probability matrix that we are filling */
	private Double[][] probaMx;

//	-------------------------------------------------------------

	/**
	 * Constructor of the <b>C</b>on<b>c</b>e<b>p</b>t <b>Inst</b>ances with <b>Proba</b>bilities. 
	 * 
	 * @param conceptName 		The concept that we will load with its others set to obtain the instances.
	 * @param classNamesTrained		The classifiers previously trained for these classes.
	 * @param descTypes				The type of descriptor used to fill this matrix.
	 * @param setsType				The type of set (train or test) on which the instances are loaded.
	 */
	public CptInstProba(String conceptName,
			String[] classNamesTrained,DescriptorType[] descTypes, SetType setsType) {
		super(conceptName,setsType,classNamesTrained,descTypes,CptInstGlobalType.MixedProba);

		nofClassTrained=classNamesTrained.length;

		isWithID=false;
		isResampled=false;

	}

//	-------------------------------------------------------------

	/**
	 * Initialisation of the probaMx size; 
	 */
	private void initProbaMx() {
		dbMan = new APNDBManager();
		datMan = new DataManager(dbMan,new String[]{"Others",conceptName},descTypes);
		datMan.setClassesPhotosIdFromFile(setsType);

		//Obtain the number of row
		int col=0;
		descStartIndex[0]=0;
		for(int i=0;i<descTypes.length;i++) {

			if(specialDescriptor(descTypes[i])) {
				col+=nofClassTrained;
			}
			else {
				col+=datMan.getFirstAvailableDescValues(descTypes[i]).getNofAttributes();
			}
			descStartIndex[i+1]=col;
		}


		//Obtain the number of row (number of instances)
		int row=datMan.getNofInstances();

		//Then set the matrix size.
		probaMx = new Double[row][col];

		my.Debug.print("row,col="+row+","+col+"\n");

	}


	
//	-------------------------------------------------------------

	private void fillProbaMx() {
		if(isDatManNotAvailable()) return;

		for(int i=0;i<descTypes.length;i++) {
			if(specialDescriptor(descTypes[i]))  
				fillOneProbaDesc(descTypes[i]);
			else 
				fillOneFullDesc(descTypes[i]);
		}

	}

	private void fillOneProbaDesc(DescriptorType descType) {

		//Obtain in which range of the all the attribute we should look to find our descriptor information
		int start=getStartIndex(descType);

		//Loading all the instances for one descriptor.
		CptInstDesc cptDescInst = new CptInstDesc(conceptName,descType,setsType);
		cptDescInst.loadInstances();
		Instances dataSet = cptDescInst.getInstances();


		for(int i=0;i<classNamesTrained.length;i++) {
			//Obtain the classifier for this specific concepts.
			CptClassifier tidxp = CptClassifierIndDesc.getInstance(classNamesTrained[i],descType);
			tidxp.loadCls();
			double[] probaVec = tidxp.computeDistribution(dataSet);
			if(probaVec != null) {
				for(int j=0;j<probaMx.length;j++) probaMx[j][i+start] = probaVec[j];
			}
			else {
				for(int j=0;j<probaMx.length;j++) probaMx[j][i+start] = Double.NaN;
			}
		}
		WekaTools.println("ProbaMx for descriptor "+descType.getAcronyms(3)+" is computed",true);
	}

	private void fillOneFullDesc(DescriptorType descType) {

		//Obtain in which range of the all the attribute we should look to find our descriptor information
		int start=getStartIndex(descType);

		//Obtain the whole list in double
		Double[][] dataDescMx = datMan.getDataInDoubleMx(descType); 

		for(int i=0;i<dataDescMx.length;i++) {
			for(int j=0;j<dataDescMx[i].length;j++) {
				probaMx[i][j+start] = dataDescMx[i][j];
			}
		}
	}

//	-------------------------------------------------------------

	private void makeInstancesHeader() {
		if(isDatManNotAvailable()) return;

//		Obtain the standard descriptor value for this descriptor
		FastVector atts = new FastVector();

		if(isWithID) {
			atts.addElement(new Attribute("photoID"));
		}

		for(int i=0;i<descTypes.length;i++) {
			DescriptorValues dV = datMan.getFirstAvailableDescValues(descTypes[i]);
			String[] attrHeader = dV.getValuesHeader();
			String descAcro=descTypes[i].getAcronyms(3);

			//Set attribute using descAcro and concept name with proba
			if(specialDescriptor(descTypes[i])) {
				for(int j=0;j<classNamesTrained.length;j++) {
					String tmp=conceptName.substring(0, 3)+"|";
					tmp+=classNamesTrained[j].substring(0, 3);
					atts.addElement(new Attribute(descAcro+"_P("+tmp+")"));
				}
			}
			else {
				if(descTypes[i] == DescriptorType.C_EXF) {
					ExifDescriptor eDesc = new ExifDescriptor();
					String[] nominals=null;
					// Setup attribute using the descriptor values attribute headers.
					for(int j=0;j<dV.getNofAttributes();j++) {
						nominals=eDesc.getNominals(j);
						if(nominals==null)
							atts.addElement(new Attribute(attrHeader[j]));
						else {
							//Building nominals
							FastVector nominalAttr = new FastVector();
							for(int k=0;k<nominals.length;k++) {
								nominalAttr.addElement(nominals[k]);
							}
							atts.addElement(new Attribute(attrHeader[j],nominalAttr));
						}
					}
				}
			}
		}

		// Build the two nominal concepts
		FastVector nominalConcepts = new FastVector();
		nominalConcepts.addElement("Others");
		nominalConcepts.addElement(conceptName);
		atts.addElement(new Attribute("Class",nominalConcepts));

		dataSet = new Instances(getFName(),atts,0);
	}

	private void makeInstancesFromProbaMx() {
		if(isDatManNotAvailable()) return;

		double[] photoIdVec = datMan.getPhotoIdVec();
		double[] cptIdVec = datMan.getClassIdVec();

		for(int i=0;i<probaMx.length;i++) {
			double[] vals = new double[dataSet.numAttributes()];

			int j=0;
			if(isWithID) vals[j++]=photoIdVec[i];

			for(;j<probaMx[i].length;j++) {
				if(probaMx[i][j] != Double.NaN)
					vals[j] = probaMx[i][j];
			}
			vals[j]=cptIdVec[i];
			dataSet.add(new Instance(1.0,vals));
		}
		isLoaded=true;
	}

//	-------------------------------------------------------------


	public boolean loadInstancesFromSQL() {

		//Initialize the size and the data Manager
		initProbaMx();
		
		//Write the fileID used
		int[] photosID = ArraysTools.toPrimitiveIVec(datMan.getPhotoIdVec());
		SetsFileTools.writePhotoID(getFilePhotoID(), photosID);

		//Fill the probaMx using others descriptors
		fillProbaMx();

		//Create the header of the instances set
		makeInstancesHeader();

		//Create the instance from the others classifier
		makeInstancesFromProbaMx();

		return true;
	}

//	-------------------------------------------------------------

	/**
	 * Return true if this descriptor has been used previously in a
	 * learning machine to output only the probabilities vector 
	 * and not all the features vector. 
	 * 
	 * <b>This is Maybe to do in a specific way looking 
	 * at the directory containing all the classes .model.</b>
	 * 
	 * @param descType
	 * @return true if has been trained previously.
	 */
	@Override
	protected boolean specialDescriptor(DescriptorType descType) {
		switch(descType) {
		case I_M7CSD:
		case I_M7EHD:
		case I_M7SCD:
			return true;
		case C_EXF:
		default:
			return false;
		}
	}

	@Override
	protected int getStartIndex(DescriptorType descType) {
		int descInd = DescriptorType.getIndex(descType, descTypes);
		return descStartIndex[descInd];
	}

	public void keepOnlyDesc(DescriptorType[] filteredDescTypes) {
		if(isLoaded()) {
			int nbC = classNamesTrained.length;
			String list="";
			for(DescriptorType fDescType : filteredDescTypes) {
				int ind=DescriptorType.getIndex(fDescType, descTypes);
				if(ind >= 0) {
					list+=(ind*nbC+1)+"-";
					list+=(int)Math.min((double)(ind+1)*nbC,dataSet.numAttributes()-1);
					list+=",";
				}
			}
			list +=  dataSet.numAttributes();
			keepOnlyAttr(list);
			this.descTypes=filteredDescTypes;
		}
	}
		
//	-------------------------------------------------------------

	@Override
	protected String getDescList() {
		String rv="";
		for(DescriptorType descType: descTypes) {
			if(specialDescriptor(descType))
				rv+=descType.getAcronyms(3);
		}
		boolean neverDone=true;
		for(DescriptorType descType: descTypes) {
			if(!specialDescriptor(descType)) {
				if(neverDone) {
					rv+="-";
					neverDone=false;
				}
				rv+=descType.getAcronyms(3);
			}
		}
		return rv;
	}
	

	public File getFilePhotoID() {
		return new File(WekaTools.pathInstMixedProba+"."+getFName()+WekaTools.extFPhotoId);
	}
	
	public CptClassifier getCptClassifier() {
		return new CptClassifierGlobalDesc(conceptName,descTypes);
	}
	
	public CptClassifier getCptClassifier(String conceptName) {
		return new CptClassifierGlobalDesc(conceptName,descTypes);
	}
}
