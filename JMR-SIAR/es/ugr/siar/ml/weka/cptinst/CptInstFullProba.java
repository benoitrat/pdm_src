/**
 * 
 */
package es.ugr.siar.ml.weka.cptinst;

import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.SetsFileTools.SetType;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 30 janv. 08
 *
 */
public class CptInstFullProba extends CptInstProba {

	/**
	 * @param conceptName
	 * @param classNamesTrained
	 * @param descTypes
	 * @param setsType
	 */
	public CptInstFullProba(String conceptName, String[] classNamesTrained, DescriptorType[] descTypes, SetType setsType) {
		super(conceptName, classNamesTrained, descTypes, setsType);
		this.type=CptInstGlobalType.FullProba;
	}



	/**
	 * Return true if this descriptor has been used previously in a
	 * learning machine to output only the probabilities vector 
	 * and not all the features vector. 
	 * 
	 * <b>This is Maybe to do in a specific way looking 
	 * at the directory containing all the classes .model.</b>
	 * 
	 * @param descType
	 * @return true if has been trained previously.
	 */
	@Override
	protected boolean specialDescriptor(DescriptorType descType) {
		switch(descType) {
		case I_M7CSD:
		case I_M7EHD:
		case I_M7SCD:
		case C_EXF:
			return true;
		default:
			return false;
		}
	}


	@Override
	protected String getDescList() {
		String rv="";
		for(DescriptorType descType: descTypes) {
			rv+=descType.getAcronyms(3);
		}
		return rv;
	}

}
