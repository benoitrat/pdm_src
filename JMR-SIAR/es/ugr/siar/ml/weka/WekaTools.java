/**
 * 
 */
package es.ugr.siar.ml.weka;

import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 19 janv. 08
 *
 */
public class WekaTools {

	public static final String pathSetsFile=my.Config.dbDumpedFilePath;

	public static final String pathClsMultiDescFile	=my.Config.dbClassfierFilePath+"multiDesc"+my.Config.OS_Slash;
	public static final String pathClsDistDescFile	=my.Config.dbClassfierFilePath+"distDesc"+my.Config.OS_Slash;
	public static final String pathClsIndDescFile		=my.Config.dbClassfierFilePath+"indDesc"+my.Config.OS_Slash;
	public static final String pathProbaClsFile		=my.Config.dbClassfierFilePath+"mixedProba"+my.Config.OS_Slash;
	public static final String pathClsFullProbaFile	=my.Config.dbClassfierFilePath+"fullProba"+my.Config.OS_Slash;
	
	
	public static final String pathInstArffFile=my.Config.dbDumpedFilePath+"arff"+my.Config.OS_Slash;
	public static final String pathInstIndDesc=my.Config.dbDumpedFilePath+"indDesc"+my.Config.OS_Slash;
	public static final String pathInstMultiDesc=my.Config.dbDumpedFilePath+"multiDesc"+my.Config.OS_Slash;
	public static final String pathInstMixedDist=my.Config.dbDumpedFilePath+"distMx"+my.Config.OS_Slash;
	public static final String pathInstMixedProba=my.Config.dbDumpedFilePath+"probaMx"+my.Config.OS_Slash;
	public static final String pathInstFullProba=my.Config.dbDumpedFilePath+"fullProbaMx"+my.Config.OS_Slash;
	
	public static final String extFPhotoId=".id";
	public static final String extFIntances=".arff";
	public static final String extFClassifier=".model";
	
	
	public static String getPatternFName(String conceptName, DescriptorType descType) {
		return conceptName+"-Others_"+descType.getAcronyms(3);
	}
	
	public static String getPatternFName(String conceptName,DescriptorType[] descTypes) {
		String fName=conceptName+"-Others_";
		for(DescriptorType descType: descTypes) {
			fName+=descType.getAcronyms(3);
		}
		return fName;
		
	}
	
	
	
	public static void println(String txt, boolean succes) {
		if(!succes) {
			System.err.println("Weka > ERROR :\t"+txt);
			System.err.flush();
		}
		else 
			System.out.println("Weka > OK :\t"+txt);
	}
	
	
	/**
	 * Return the typical descriptor matrix for experiments.
	 * index 0/1 <=> with/without EXIF descriptors;
	 * 
	 * 
	 * @return the descriptorMx 
	 */
	public static DescriptorType[][] getDescMx() {
		return new DescriptorType[][]{
				{DescriptorType.I_M7CSD, DescriptorType.I_M7SCD,
			DescriptorType.I_M7EHD,DescriptorType.C_EXF},
			
				{DescriptorType.I_M7CSD, DescriptorType.I_M7SCD,
				DescriptorType.I_M7EHD}
			};
	}
	
	public static DescriptorType[] getFullDescTypes() {
		return new DescriptorType[]{DescriptorType.I_M7CSD, DescriptorType.I_M7SCD,
			DescriptorType.I_M7EHD,DescriptorType.C_EXF};
	}
	
	public static DescriptorType[] getVisualDescTypes() {
		return new DescriptorType[]{DescriptorType.I_M7CSD, DescriptorType.I_M7SCD,
			DescriptorType.I_M7EHD};
	}
	
}
