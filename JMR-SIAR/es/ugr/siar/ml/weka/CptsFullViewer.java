/**
 * 
 */
package es.ugr.siar.ml.weka;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import es.ugr.siar.gui.thumbs.ResultProperties;
import es.ugr.siar.gui.thumbs.WinThumbsProba;
import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.weka.cptcls.CptClassifier;
import es.ugr.siar.ml.weka.cptinst.CptInstGlobal;
import es.ugr.siar.ml.weka.cptinst.CptInstProba;
import es.ugr.siar.ml.weka.cptinst.CptInstGlobal.CptInstGlobalType;
import es.ugr.siar.tools.IndexedGeneric;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 30 janv. 08
 *
 */
public class CptsFullViewer {

	/** The Type of view to show */
	public static enum ViewType {
		ALL,FPONLY,TPONLY;
	}


	/** Which concepts are shown in the statistics */
	protected String[] cptsInStats;

	/** Which concepts are read */
	protected String[] cptsGlobal;

	/** Which descriptor are used */
	protected DescriptorType[] descTypes;

	/** Which kind of set is used */
	protected SetsFileTools.SetType setType;


	/** If we want to see the others set */
	protected boolean othersView=false;

	/**
	 * 
	 */
	public CptsFullViewer() {	
		cptsInStats=SetsFileTools.readGlobalClassNames();
		cptsGlobal=SetsFileTools.readGlobalClassNames();
		setType= SetsFileTools.SetType.TEST;
		descTypes = new DescriptorType[] {
				DescriptorType.I_M7CSD,DescriptorType.I_M7SCD,
				DescriptorType.I_M7EHD,DescriptorType.C_EXF};
	}


	public void show(String cptToShow,ViewType viewType, CptInstGlobalType gInstType, 
			boolean othersView) {


		double[][] statsMx = getFinalStatsMatrix(cptToShow,gInstType);
		List<IndexedGeneric<ResultProperties>> listId = new ArrayList<IndexedGeneric<ResultProperties>>();
		int nofCol = statsMx.length;
		int indCptToShow=-1;

		//The index of the cptToShow.
		for(int i=0;i<cptsInStats.length;i++) {
			if(cptsInStats[i].equals(cptToShow)) {
				indCptToShow=i;
				break;
			}
		}


		//The concept they should have.
		int cpt1_Ok=1;
		int cpt0_Ok=0;

		switch(viewType) {
		case ALL: //Nothing
			break;
		case FPONLY:
			if(othersView) cpt0_Ok=-1; //No image in class0 if I need to know which one are not part of this class
			else cpt1_Ok=-1;
			break;
		case TPONLY:
			if(othersView) cpt1_Ok=-1; //No image in class1, because there is only image of othersView.
			else cpt0_Ok=-1;
			break;

		}

		for(int j=0;j<statsMx[0].length;j++) {
			IndexedGeneric<ResultProperties> e;
			int classId = (int)statsMx[nofCol-2][j];

			//Check if we except to see this class in the plot
			if(classId == cpt1_Ok || classId == cpt0_Ok) {
				int photoId = (int)statsMx[nofCol-1][j];
				float score = (float)statsMx[indCptToShow][j];
				String txt=""+photoId+" ("+(score)+")";

				//Image should be less than 0.5
				if(othersView) {
					if(score >= 0.5f) continue;
				}
				else {
					if(score < 0.5f) continue;
				}
				ResultProperties rProp = new ResultProperties(classId,(score >=0.5f),score);
				
				for(int i=0;i<(nofCol-2);i++) {
					rProp.addAnnotation(i, cptsInStats[i].substring(0,3), ((float)statsMx[i][j]));
				}
				e = new IndexedGeneric<ResultProperties>(photoId,txt,rProp);
				listId.add(e);
			}
		}
		Collections.sort(listId);
		Collections.reverse(listId);
		new WinThumbsProba("Training of "+cptToShow+" class",listId);
		
	}


	public void showExifAdd(String cptToShow,ViewType viewType, CptInstGlobalType gInstType, 
			boolean othersView) {


		double[][] statsMx = getFinalStatsMatrix(cptToShow,gInstType);
		List<IndexedGeneric<ResultProperties>> listId = new ArrayList<IndexedGeneric<ResultProperties>>();
		int nofCol = statsMx.length;
		int indCptToShow=-1;

		//The index of the cptToShow.
		for(int i=0;i<cptsInStats.length;i++) {
			if(cptsInStats[i].equals(cptToShow)) {
				indCptToShow=i;
				break;
			}
		}


		//The concept they should have.
		int cpt1_Ok=1;
		int cpt0_Ok=0;

		switch(viewType) {
		case ALL: //Nothing
			break;
		case FPONLY:
			if(othersView) cpt0_Ok=-1; //No image in class0 if I need to know which one are not part of this class
			else cpt1_Ok=-1;
			break;
		case TPONLY:
			if(othersView) cpt1_Ok=-1; //No image in class1, because there is only image of othersView.
			else cpt0_Ok=-1;
			break;

		}

		for(int j=0;j<statsMx[0].length;j++) {
			IndexedGeneric<ResultProperties> e;
			int classId = (int)statsMx[nofCol-2][j];

			//Check if we except to see this class in the plot
			if(classId == cpt1_Ok || classId == cpt0_Ok) {
				int photoId = (int)statsMx[nofCol-1][j];
				float score = (float)statsMx[indCptToShow][j];
				String txt=""+photoId+" ("+(score)+")";

				//Image should be less than 0.5
				if(othersView) {
					if(score >= 0.5f) continue;
				}
				else {
					if(score < 0.5f) continue;
				}
				ResultProperties rProp = new ResultProperties(classId,(score >=0.5f),score);
				
				for(int i=0;i<(nofCol-2);i++) {
					rProp.addAnnotation(i, cptsInStats[i].substring(0,3), ((float)statsMx[i][j]));
				}
				e = new IndexedGeneric<ResultProperties>(photoId,txt,rProp);
				listId.add(e);
			}
		}
		Collections.sort(listId);
		Collections.reverse(listId);
		new WinThumbsProba("Training of "+cptToShow+" class",listId);
		
	}
	
	

	/**
	 * Return a matrix with the statistic to be displayed.
	 *  Matrix structure is : {cptStats1,...,cptStatsN,classID,photoID} x numInstance, 
	 *  where cptStats1 represents the result of the cptToShow using the classifier,
	 *  cptInStats.
	 *   
	 * @param cptToShow
	 * @param gInstType
	 * @return the super statistic matrix.
	 */
	public double[][] getFinalStatsMatrix(String cptToShow,CptInstGlobalType gInstType) {
	
		
		int nCol = cptsInStats.length +2;
		double[][] statsMx =  new double[nCol][];
		

		//Loading instances
		CptInstGlobal cptGlobalInst = null;
		CptClassifier cptClassifier = null;

		switch(gInstType) {
		case MixedProba:
			cptGlobalInst = new CptInstProba(cptToShow,cptsGlobal,descTypes,setType);
		}
				
		cptGlobalInst.loadInstances();

		if(cptGlobalInst.isLoaded()) {
			int i;
			for(i=0;i<cptsInStats.length;i++) {
				cptClassifier = cptGlobalInst.getCptClassifier(cptsInStats[i]);
				cptClassifier.loadCls();
				if(!cptClassifier.isTrained()) continue;
				statsMx[i] = cptClassifier.computeDistribution(cptGlobalInst.getInstances());
			}
			statsMx[i++] = cptGlobalInst.getClassIndexes();
			statsMx[i] = cptGlobalInst.getPhotosID();
		}
		return statsMx;
	}


}
