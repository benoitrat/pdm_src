/**
 * 
 */
package es.ugr.siar.ml;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.List;

import es.ugr.siar.ml.SetsFileTools.SetType;
import es.ugr.siar.tools.IndexedGeneric;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 12 janv. 08
 *
 */
public class DistanceStats {
	public static final String ext=".dist";
	public static final String fPath=my.Config.dbStatResultsPath;
	
	static public void writeDistancePhotosID(String classname, SetType setType,int descType,
			List<IndexedGeneric<Float>> list,boolean distribOk) {
		
		//Get the name of the two files
		String fName="";
		if(distribOk)
			fName= classname+"_"+descType+"-true-"+setType.toString()+ext;
		else
			fName= classname+"_"+descType+"-false-"+setType.toString()+ext;

		//Create the printStream
		PrintStream pStream;
		try {
			pStream = new PrintStream(fPath+fName);
		} catch (FileNotFoundException e) {
			System.err.println("Files "+fPath+fName+" can not be created");
			e.printStackTrace();
			return;
		}
		
		//Printing training ID
		for(IndexedGeneric<Float> igFloat : list) {
			pStream.println(igFloat.getGenericObject());
		}
		System.out.println(fName+" > OK");		
	}
}
