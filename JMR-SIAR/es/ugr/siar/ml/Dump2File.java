/**
 * 
 */
package es.ugr.siar.ml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import es.ugr.siar.db.DatabaseManager;
import es.ugr.siar.db.MySQL;
import es.ugr.siar.ip.desc.ExifDescriptor;
import es.ugr.siar.ip.desc.VisualDescriptor;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.tools.ArraysTools;
import es.ugr.siar.tools.IndexedGeneric;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 15 janv. 08
 *
 */
abstract public class Dump2File {
	
	protected static enum Format { 
		ARFF,MATLAB,CVS,LSVM;
		public String getExt(){
			switch(this) {
			case MATLAB:	return ".m";
			default: return "."+toString();
			}
		}
		public String toString() {
			switch(this) {
			case ARFF:		return "arff";
			case MATLAB: 	return "matlab";
			case CVS:	 	return "cvs";
			case LSVM:		return "lsvm";
			default:		return "";
			}
			
		}
	};
	
	public static String othersName="Others";
	
	protected DatabaseManager dbMan;
	private MySQL db;
	protected Format format;
	protected String[] classNames;
	protected int[] descTypes;
	protected boolean exif;
	protected static final String fPath=my.Config.dbDumpedFilePath;
	
	
	Dump2File(DatabaseManager dbMan, String[] classNames,int[] descTypes, boolean exif,Format format) {
		this.dbMan=dbMan;
		this.classNames=classNames;
		this.descTypes=descTypes;
		this.exif=exif;
		this.format=format;
		this.db=dbMan.getDataBase();
	}
	
	
	public List<IndexedGeneric<Integer[]>> getClassPhotoIDs(SetsFileTools.SetType setType) {
		List<IndexedGeneric<Integer[]>> cNamesIDsUsed = new ArrayList<IndexedGeneric<Integer[]>>();  
		if(isOthersClass()) {
			Integer[] classIDs = ArraysTools.toArray(SetsFileTools.readPhotosID(classNames[1], setType));
			Integer[] otherIDs = ArraysTools.toArray(SetsFileTools.readOthersPhotoID(classNames[1], setType));
			cNamesIDsUsed.add(new IndexedGeneric<Integer[]>(-1,classNames[1],classIDs));
			cNamesIDsUsed.add(new IndexedGeneric<Integer[]>(-1,classNames[0],otherIDs));
		}
		else {
			for(String className : classNames) {
				Integer[] classIDs = ArraysTools.toArray(SetsFileTools.readPhotosID(className, setType));
				cNamesIDsUsed.add(new IndexedGeneric<Integer[]>(-1,className,classIDs));
			}
		}
		return cNamesIDsUsed;
	}
	
	protected PrintStream getPrintStream(SetsFileTools.SetType setType) {
		
		File file = getFile(setType);
		PrintStream pStream;
		try {
			pStream = new PrintStream(file);
			return pStream;
		}
		catch (FileNotFoundException e) {
			System.err.println("File "+file.getName()+" can not be created");
			e.printStackTrace();
			return null;
		}
	}
	
	protected String getPathName(SetsFileTools.SetType setType) {
		String fName="";
		if(isOthersClass()) 
			fName +=classNames[1]+"-"+othersName;
		
		else for(String cN : classNames) fName+=smallClassName(cN);
		fName+="_";
		
		for(int d : descTypes) {
			fName += VisualDescriptor.getAcronyms(d);
		}
		if(exif) fName+="EXF";
		
		fName +="_"+setType.toString();
		return fName;
	}
	
	public File getFile(SetsFileTools.SetType setType) {
		String fName = getPathName(setType);
		System.out.println(fName+format.getExt());
		return new File(fPath+format.toString()+"/"+fName+format.getExt());
	}
	
	protected boolean isOthersClass() {
		return (classNames.length == 2 && classNames[0].equals(othersName));
	}
	
	private String smallClassName(String className) {
		return className.substring(0, 2);
	}
	
	protected List<String[]> getDataHeaders(int photoRef) {	
		List<String[]> list = new ArrayList<String[]>();
		for(int i=0;i<descTypes.length;i++) {
			VisualDescriptor vDesc = VisualDescriptor.getInstance(descTypes[i]);
			//TODO: CHANGE THIS CHEATS
			while(!vDesc.exist(photoRef, db, true)) {
				photoRef++;
			}
				vDesc.fromMySQL(photoRef, db);
				photoRef++;
			
			list.add(vDesc.toStrVec(true));
		}
		if(exif) {
			ExifDescriptor cDesc = new ExifDescriptor();
			cDesc.fromMySQL(photoRef, db);
			list.add(cDesc.toStrVec(true));
		}
		return list;
	}
	
	protected List<String[]> getDataLine(int photoID) {	
		boolean writeExif=exif;
		List<String[]> list = new ArrayList<String[]>();
		for(int i=0;i<descTypes.length;i++) {
			VisualDescriptor vDesc = VisualDescriptor.getInstance(descTypes[i]);
			if(vDesc.exist(photoID, db, true)) {
				vDesc.fromMySQL(photoID, db);
				list.add(vDesc.toStrVec());
			}
			else {
				//TODO:Something better for checking ERROR
				writeExif=false;
				break;
			}
		}
		if(writeExif) {
			ExifDescriptor cDesc = new ExifDescriptor();
				cDesc.fromMySQL(photoID,db);
				list.add(cDesc.toStrVec(false));
		}
		return list;
	}
	
	abstract public void print(SetsFileTools.SetType setType);
	
}
