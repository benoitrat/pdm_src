/**
 * 
 */
package es.ugr.siar.ml;

import java.util.ArrayList;
import java.util.List;

import es.ugr.siar.db.DatabaseManager;
import es.ugr.siar.db.MySQL;
import es.ugr.siar.ip.desc.DescriptorValues;
import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.tools.IndexedGeneric;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 17 janv. 08
 *
 */
public class DataManager {

	protected String[] classNames;
	protected DescriptorType[] descTypes;
	protected int[] descStartIndex;
	protected DatabaseManager dbMan;
	protected List<IndexedGeneric<Integer[]>> cNamesPicId;
	protected List<Double[]> dataMx;
	protected List<Integer> photoIdVec;
	protected List<Integer> classIdVec;

	int nofInstances=0;
	int nofAttributes=0;

	
	/**
	 * Constructor of the dataManager.
	 * <b>To set all the values it is obtain the list of photoID:</b>
	 * This operation can be done using 
	 * {@link #setClassesPhotosIdFromFile(es.ugr.siar.ml.SetsFileTools.SetType)}
	 * or using {@link #setClassesPhotosIdFromSQL(es.ugr.siar.ml.SetsFileTools.SetType)}. 
	 * 
	 * @param dbMan 		The database manager
	 * @param classNames	The list of classNames with which we look the photoID
	 * @param descTypes		The descriptor type to obtain the information.
	 */
	public DataManager(DatabaseManager dbMan, String[] classNames,DescriptorType[] descTypes) {
		this.dbMan = dbMan;
		this.classNames = classNames;
		this.descTypes = descTypes;
		cNamesPicId=null;

		
	}


	/**
	 * Obtain the list containing the tupple <code>ClassName-PhotoID</code>.
	 * This function also init the {@link #dataMx}, the {@link #photoIdVec}, {@link #classIdVec}.
	 * 
	 * @param setType
	 */
	public void setClassesPhotosIdFromFile(SetsFileTools.SetType setType) {
		cNamesPicId=SetsFileTools.getClassPhotoIDs(classNames,setType);
		dataMx = new ArrayList<Double[]>();
		photoIdVec = new ArrayList<Integer>();
		classIdVec = new ArrayList<Integer>();
		setNofAttributes();
		setDataLists();
	}

	//TODO: When the database is better
	public void setClassesPhotosIdFromSQL(SetsFileTools.SetType setType) {
		cNamesPicId=null;
	}

	public boolean isPhotoIDAvailable() {
		if(cNamesPicId==null) {
			System.err.println("The ClassList with corresponding photos Id is not setted");
		}
		return true;
	}

	/**
	 * Get the number of attribute of one instance (nofColumn).
	 * This value is obtained summing the number of attributes 
	 * of each descriptor.
	 * 
	 */
	protected void setNofAttributes() {
		if(!isPhotoIDAvailable()) return;

		nofAttributes=0;
		//Get the number of attribute of one instance (# col)
		DescriptorValues dV;
		descStartIndex = new int[descTypes.length+1];
		descStartIndex[0]=0;
		for(int i=0;i<descTypes.length;i++) {
			dV = getFirstAvailableDescValues(descTypes[i]);
			nofAttributes += dV.getNofAttributes();
			descStartIndex[i+1] =  nofAttributes;
		}
		
	}

	protected void setDataLists() {
		if(!isPhotoIDAvailable()) return;

		setNofAttributes();
		int row=0,col=0;
		DescriptorValues dV;
		MySQL db = dbMan.getDataBase();

		//For each class present in classNames
		for(IndexedGeneric<Integer[]> classPhotosId : cNamesPicId) {

			int classID = classPhotosId.getId();
			
			//For each photo put a line :
			for(int photoID : classPhotosId.getGenericObject()) {
				//System.out.println(row+" -> "+photoID);
				Double[] vec = new Double[nofAttributes];
				boolean notFound=false;

				
//				if(photoID==8967 || photoID == 27802 || photoID == 4136 || photoID == 12895 || photoID == 13454) {
//					System.out.println("ioiouo");
//				}
				
				//For each descriptors get the double array:
				col=0;
				for(int i=0;i<descTypes.length;i++) {
					dV = DescriptorValues.setDescValuesFromMySQL(db, photoID, descTypes[i]);
					if(dV == null) {
						notFound=true;
					}
					else {
						
						Double[] descTab = dV.getValuesInDouble();
						//For each value fill the matrix
						for(Double val : descTab) {
							vec[col] = val;
							col++;
						}
					}
				}
				if(!notFound) {
					dataMx.add(vec);
					classIdVec.add(classID);
					photoIdVec.add(photoID);
					row++;
				}
			}			
		}
		if(dataMx.size() == classIdVec.size() && photoIdVec.size() == dataMx.size()) {
			nofInstances=dataMx.size();
		}
		else
			System.err.println("Problem in the creation of the data list");
	}


	public Double[][] getDataInDoubleMx(DescriptorType descType) {
		
		int descInd = DescriptorType.getIndex(descType, descTypes);
		
		int start=descStartIndex[descInd];
		int end=descStartIndex[descInd+1];
		int nofAttrDesc=end-start;
		
		Double[][] rv = new Double[nofInstances][nofAttrDesc];
		for(int i=0;i<dataMx.size();i++) {
			for(int j=0;j<nofAttrDesc;j++) {
				rv[i][j] = dataMx.get(i)[start+j];
			}
		}
		
		return rv;
	}
	
	
	/**
	 * Obtain a data matrix nofInstance x  nofAttribute.
	 * This matrix doesn't have information about the 
	 * className or classID, the {@link #getClassIdVec()} or 
	 * {@link #getClassNamesVec()} should be used instead.
	 * 
	 * 
	 * @return
	 */
	public Double[][] getDataInDoubleMx() {
		if(!isPhotoIDAvailable()) return null;
		
		Double[][] rv = new Double[nofInstances][nofAttributes];
		for(int i=0;i<dataMx.size();i++) {
			for(int j=0;j<dataMx.get(i).length;j++) {
				rv[i][j] = dataMx.get(i)[j];
			}
		}
		return rv;
	}
	
		

	/**
	 * Obtain the photoID vector (nofInstance x 1).
	 *  
	 * @return photoID vector (nofInstance x 1)
	 */
	public double[] getPhotoIdVec() {
		if(!isPhotoIDAvailable()) return null;
		double[] rv= new double[nofInstances];

		for(int i=0;i<photoIdVec.size();i++) {
			rv[i] = photoIdVec.get(i).doubleValue();
		}
		return rv;
	}


	/**
	 * Obtain the class name vector (nofInstance x 1).
	 * 
	 * @return class name vector (nofInstance x 1).
	 */
	public String[] getClassNamesVec() {
		if(!isPhotoIDAvailable()) return null;

		String[] rv= new String[nofInstances];
		for(int i=0;i<nofInstances;i++) {
			rv[i] = classNames[classIdVec.get(i).intValue()];
		}
		return rv;
	}


	/**
	 * Obtain the class name vector (nofInstance x 1).
	 * 
	 * @return class name vector (nofInstance x 1).
	 */
	public double[] getClassIdVec() {
		if(!isPhotoIDAvailable()) return null;

		double[] rv= new double[nofInstances];
		for(int i=0;i<nofInstances;i++) {
			rv[i] = classIdVec.get(i).doubleValue();
		}
		return rv;

	}


	public DescriptorValues getFirstAvailableDescValues(DescriptorType descType) {
		if(!isPhotoIDAvailable()) return null;
		
		DescriptorValues dV=null;
		int photosID[] = SetsFileTools.ClassPhotoIDstoPhotosID(cNamesPicId);
		
		for(int photoID : photosID) {
			dV = DescriptorValues.setDescValuesFromMySQL(dbMan.getDataBase(),photoID, descType);
			if(dV != null) break;
		}
		return dV;
	}
	
	
	public List<Double[]> getDataMx() {
		return dataMx;
	}
	
	
	public String[] getClassNames() {
		return classNames;
	}


	public DatabaseManager getDbMan() {
		return dbMan;
	}


	public DescriptorType[] getDescTypes() {
		return descTypes;
	}


	public int getNofAttributes() {
		return nofAttributes;
	}


	public int getNofInstances() {
		return nofInstances;
	}

}
