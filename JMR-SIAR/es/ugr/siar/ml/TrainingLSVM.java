/**
 * 
 */
package es.ugr.siar.ml;

import java.io.IOException;
import libsvm.*;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 17 janv. 08
 *
 */
public class TrainingLSVM {

	svm_problem dataTrain;
	svm_problem dataTest;
	svm_model modelSVM;

	//Train with libSVM
	public void train() {
		svm_parameter params = new svm_parameter();
		params.svm_type = svm_parameter.C_SVC;
		params.kernel_type = svm_parameter.RBF;
		params.C = Math.pow(2,4);
		params.gamma = Math.pow(2,-0.5);
		svm.svm_check_parameter(dataTrain, params);
		System.out.println("libSVM > starting training");
		modelSVM = svm.svm_train(dataTrain, params);
		String fName=Dump2LSVM.fPath+"model";
		try {
			
			svm.svm_save_model(fName, modelSVM);
			System.out.println("libSVM > model file "+fName+" writed");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



	}

	public void setTrainingData(DataManager datMan,SetsFileTools.SetType setType) {

		Double[][] dataMx = datMan.getDataInDoubleMx();

		dataTrain = new libsvm.svm_problem();
		dataTrain.l = datMan.getNofInstances(); //Nof instance
		dataTrain.y = datMan.getClassIdVec(); //Instances classes vector.
		dataTrain.x = new svm_node[dataMx.length][dataMx[0].length];

		//Copy for making training set
		for(int i=0;i<dataMx.length;i++) {
			for(int j=0;j<dataMx[i].length;j++) {
				dataTrain.x[i][j] = new svm_node();
				dataTrain.x[i][j].index=j+1;
				dataTrain.x[i][j].value=dataMx[i][j];
			}
		}
		System.out.println("libSVM > Data loaded");
	}


	public void saveTrainingData() {
		
	}



}
