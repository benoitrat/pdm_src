/**
 * 
 */
package es.ugr.siar.ml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

import es.ugr.siar.tools.ArraysTools;
import es.ugr.siar.tools.IndexedGeneric;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 11 janv. 08
 *
 */
public class SetsFileTools {
	
	
	public static enum SetType { 
		TRAIN,TEST,ALL;
		public String getExt(){
			switch(this) {
			case TRAIN: return ".train";
			case TEST: 	return ".test";
			default: 	return ".all";
			}
		}
		public String toString(){
			switch(this) {
			case TRAIN: return "train";
			case TEST: return "test";
			default: return "";
			}
		}
		
	};
	
	public static final String globalName="Global";
	public static final String othersName="Others";
	

	private static final String globalClassFName="Global.classname";
	private static final String fPath=my.Config.dbTrainTestSetPath;
	
	public static void writeTrainTestPhotosID(String classname, int percent, int [] photosId) {
		
		//Randomly sort the array.
		List<Integer> photosID = new ArrayList<Integer>();
		for(int id : photosId) photosID.add(id);
		
		Collections.shuffle(photosID);
		
		//Get the index of the first test photoID
		int firstTestInd = (int)((double)percent*(double)(photosId.length)/100.0);
		
		//Get the name of the two files
		PrintStream fpTrain;
		PrintStream fpTest;
		try {
			fpTrain = new PrintStream(fPath+classname+SetType.TRAIN.getExt());
			fpTest = new PrintStream(fPath+classname+SetType.TEST.getExt());
		} catch (FileNotFoundException e) {
			System.err.println("Files "+classname+".test and "+classname+".train can not be created");
			e.printStackTrace();
			return;
		}
		
		//Printing training ID
		for(int i=0;i<firstTestInd;i++) fpTrain.println(photosID.get(i));
		System.out.println(classname+".train:\t Training file correctly created");
		
		//Printing testing ID
		for(int i=firstTestInd;i<photosID.size();i++) fpTest.println(photosID.get(i));
		System.out.println(classname+".test:\t Testing file correctly created\n");
		
	}
	
	public static void writePhotoID(File file, int[] photosID) {
		PrintStream pStream;
		try {
			pStream = new PrintStream(file.getAbsolutePath());
		} catch (FileNotFoundException e) {
			System.err.println("Files "+file.getAbsolutePath()+" can not be created");
			e.printStackTrace();
			return;
		}
		
		//Write each photo ID obtains the photosID
		for(int photoID : photosID) {
				pStream.println(photoID);
		}
		
		//Finish message 
		System.out.println(file.getName()+" file correctly created\n");
	}
	
	
	public static void writeGlobalClassNames(String[] classNames) {
		
		//Create the printStream
		PrintStream pStream;
		try {
			pStream = new PrintStream(fPath+globalClassFName);
		} catch (FileNotFoundException e) {
			System.err.println("Files "+globalClassFName+" can not be created");
			e.printStackTrace();
			return;
		}
		//Print in the printStream
		for(String className : classNames) {
			pStream.println(className);
		}
		System.out.println(globalClassFName+" file correctly created\n");
		
		
	}
	
	public static String[] readGlobalClassNames() {
		
		//A dynamic list is created before returning a static array.
		List<String> cNamesList = new ArrayList<String>();
		
		File file = new File(fPath+globalClassFName);
		if(!file.exists()) return new String[] {""};
		
		//Reader and Scanner are created
		FileReader reader;
		try {
			reader = new FileReader(file);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			return null;
		}	
		Scanner scanner=new Scanner(reader);
		
		//Regex for separating fields.
		scanner.useDelimiter(Pattern.compile("[\n]"));

		//Iterating on each new fields
		while (scanner.hasNext()) {
		    //The field need to be an int
			try{
				cNamesList.add(scanner.next());
			}
			catch(InputMismatchException e) {
				//do nothing
			}
		}
		String [] tmp = new String[cNamesList.size()]; 
		return cNamesList.toArray(tmp);
	}
	
	
	public static void writeGlobalSet(SetType setType) {
		
		//Create the printStream
		PrintStream pStream;
		try {
			pStream = new PrintStream(fPath+globalName+setType.getExt());
		} catch (FileNotFoundException e) {
			System.err.println("Files "+globalName+" can not be created");
			e.printStackTrace();
			return;
		}
		
		//Get all className in one array
		String [] classNames = readGlobalClassNames();
		
		//For each className[.test,.train] obtains the photosID
		for(String className : classNames) {
			int [] photosID = readPhotosID(className,setType);
			//Write each photoId in the file
			for(int photoID : photosID) {
				pStream.println(photoID);
			}
		}
		
		//Finish message 
		System.out.println(globalName+" file correctly created\n");
	}

	public static int [] readOthersPhotoID(String classname,SetType setType) {
		int[] classPhotosID_train,classPhotosID_test, globalPhotosID;
		//Get the photo from the specific files
		classPhotosID_train = readPhotosID(classname,SetType.TRAIN);
		classPhotosID_test = readPhotosID(classname,SetType.TEST);
		globalPhotosID = readPhotosID(globalName,setType);
		return readOthersPhotoID(ArraysTools.arrayMerge(classPhotosID_train,classPhotosID_test), globalPhotosID);
	}
	
	
	
	/**
	 * It Construct the opposite/negative set of the class set.
	 * <p>
	 * 0thersSet = GlobalSet - ClassSet.
	 * </p>
	 * 	 
	 * @param classPhotosID 	All the photosId in the ClassSet 
	 * @param globalPhotosID	All the photosId of all the different Sets. 
	 * @return					All the photosId of the opposite set to ClassSet.
	 */
	private static int [] readOthersPhotoID(int[] classPhotosID, int [] globalPhotosID) {
		
	
		Set<Integer> classSet = ArraysTools.toSet(classPhotosID);
		Set<Integer> globalSet = ArraysTools.toSet(globalPhotosID);
		int othersPhotosID[] = null;	
		
		if(globalSet.removeAll(classSet)) {
			othersPhotosID = ArraysTools.toPrimitiveVec(globalSet);
		}
		return othersPhotosID;
	}
	
	public static int[] readPhotosID(String classname,SetType setType) {
		File file;
		//Get the photo from the specific files
		file = new File(fPath+classname+setType.getExt());
		return readPhotosId(file);
	}
	
	public static int[] readPhotosId(File file) {
		
		//A dynamic list is created before returning a static array.
		List<Integer> photosID = new ArrayList<Integer>();
		
		//Reader and Scanner are created
		FileReader reader;
		try {
			reader = new FileReader(file);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			return null;
		}	
		Scanner scanner=new Scanner(reader);

		//Regex for separating fields.
		scanner.useDelimiter(Pattern.compile("[\n]"));

		//Iterating on each new fields
		while (scanner.hasNext()) {
		    //The field need to be an int
			try{
				photosID.add(scanner.nextInt());
			}
			catch(InputMismatchException e) {
				//do nothing
			}
		}
		
		//TODO:Use the method in ArraysTools
		//Convert List<Integer> into int[]
		int[] photosIdTab = new int[photosID.size()];
		for(int i=0;i<photosIdTab.length;i++) {
			photosIdTab[i] = photosID.get(i);
		}
		return photosIdTab;
	}
	
	public static List<IndexedGeneric<Integer[]>> getClassPhotoIDs(String classNames[], SetType setType) {
		List<IndexedGeneric<Integer[]>> cNamesIDsUsed = new ArrayList<IndexedGeneric<Integer[]>>();  
		if(isOthersClass(classNames)) {
			Integer[] classIDs = ArraysTools.toArray(readPhotosID(classNames[1], setType));
			Integer[] otherIDs = ArraysTools.toArray(readOthersPhotoID(classNames[1], setType));
			cNamesIDsUsed.add(new IndexedGeneric<Integer[]>(1,classNames[1],classIDs));
			cNamesIDsUsed.add(new IndexedGeneric<Integer[]>(0,classNames[0],otherIDs));
		}
		else {
			int count=0;
			for(String className : classNames) {
				Integer[] classIDs = ArraysTools.toArray(readPhotosID(className, setType));
				cNamesIDsUsed.add(new IndexedGeneric<Integer[]>(count++,className,classIDs));
			}
		}
		return cNamesIDsUsed;
	}
	
	public static int[] ClassPhotoIDstoPhotosID(List<IndexedGeneric<Integer[]>> cpIdList) {
		int count=0;
		for(IndexedGeneric<Integer[]> igI :cpIdList )
			count +=igI.getGenericObject().length;
		
		int [] rv = new int[count];
		count=0;
		for(IndexedGeneric<Integer[]> igI :cpIdList) {
			Integer tab[] = igI.getGenericObject();
			for(int i=0;i<tab.length;i++) {
				rv[count++]=tab[i];
			}
		}
		return rv;
		
	}

	public static boolean isOthersClass(String classNames[]) {
		return (classNames.length == 2 && classNames[0].equals(othersName));
	}
	
	
	public static void printIntersection(int[] pId1, int[] pId2) {
		Set<Integer> s1 = ArraysTools.toSet(pId1);
		Set<Integer> s2 = ArraysTools.toSet(pId2);
		
		s1.retainAll(s2);
		Integer[] rv= new Integer[s1.size()];
		rv = s1.toArray(rv);
		my.Debug.printTab(rv);
		
	}
	
}
