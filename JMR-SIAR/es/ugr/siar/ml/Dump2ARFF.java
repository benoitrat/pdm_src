/**
 * 
 */
package es.ugr.siar.ml;

import java.io.PrintStream;
import java.util.List;

import es.ugr.siar.db.DatabaseManager;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.tools.IndexedGeneric;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 15 janv. 08
 *
 */
public class Dump2ARFF extends Dump2File {

	/**
	 * @param db
	 * @param className
	 * @param setType
	 * @param descTypes
	 * @param exif
	 * @param format
	 */
	

	public Dump2ARFF(DatabaseManager dbMan, String[] className, int[] descTypes, boolean exif) {
		super(dbMan, className, descTypes, exif, Format.ARFF);
	}
	
	public void print(SetsFileTools.SetType setType) {
		PrintStream ps = this.getPrintStream(setType);
		printHeader(ps,setType);
		System.out.println("Header printed");
		printData(ps,setType);
	}
	
	private void printHeader(PrintStream ps,SetsFileTools.SetType setType) {
		  
		   String tmp="";
		   for(String className : classNames) {
			   tmp+=className+",";
		   }
		   tmp=tmp.substring(0,tmp.length()-1);
		   
		   ps.println("% 1. Title: "+tmp);
		   ps.println("% 2. Author: RAT Benoit");
		   ps.println("\n\n");
		   
		   //RELATION
		   ps.println("@RELATION "+getPathName(setType));
		
		   //ATTRIBUTE
		   ps.println("");
		   //ps.println("@ATTRIBUTE photoID NUMERIC");
		   for(String[] descData : getDataHeaders(12054)) {
			   for(String col : descData) {
				   ps.println("@ATTRIBUTE "+col+" NUMERIC");
			   }
		   }
		   //ps.println("@ATTRIBUTE class NUMERIC");
		   ps.println("@ATTRIBUTE class {"+tmp+"}");
		   ps.println("\n\n");
		   
	}
	
	private void printData(PrintStream ps,SetsFileTools.SetType setType) {
		ps.println("@DATA");
		for(IndexedGeneric<Integer[]> classPhotosID : getClassPhotoIDs(setType)) {
			
			//Obtain the className for this file
			String className = classPhotosID.toString();
			System.out.print(className+" ");
			
			//For each photo put a line :
			for(int photoID : classPhotosID.getGenericObject()) {
				//String line=""+photoID+",";
				String line ="";				
				//For each descriptor
				List<String[]> listDescData = getDataLine(photoID);
				if(listDescData.isEmpty()) continue; //Don't print this line
				
				for(String[] descData : listDescData) {
					for(String column : descData) {
						line+=column+",";
					}
				}
//				if(className.equals(this.othersName)) 
//					line+=""+0.0;
//				else 
//					line+=""+1.0;
				line+=className;
				ps.println(line);
			}
			System.out.println(" dumped");
		}
		System.out.println();
	}
}
