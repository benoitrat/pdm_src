/**
 * 
 */
package es.ugr.siar.tools;

import es.ugr.siar.gui.thumbs.ResultProperties;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 11 janv. 08
 *
 */
public class IndexedGeneric<T> implements Comparable<IndexedGeneric<T>> {

	String str;
	int id;
	T genericObj;

	public IndexedGeneric(int id, String str, T genericObj) {
		this.id=id;
		this.str=str;
		this.genericObj=genericObj;
	}

	public String toString() {
		return str;
	}

	public int getId() {
		return id;
	}

	public T getGenericObject() {
		return genericObj;
	}

	
	public int compareTo(IndexedGeneric<T>other) {
		if(genericObj instanceof Float && other.genericObj instanceof Float) {
			return ((Float)this.genericObj).compareTo((Float)other.genericObj);
		} 
		if(genericObj instanceof ResultProperties && other.genericObj instanceof ResultProperties) {
			return ((ResultProperties)this.genericObj).compareTo((ResultProperties)other.genericObj);
		}
		return 0;
	}

}
