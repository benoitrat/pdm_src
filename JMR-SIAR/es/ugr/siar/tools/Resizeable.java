/**
 * 
 */
package es.ugr.siar.tools;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

 
//For testing
 
///*
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
//*/
 
/** 
 * tjacobs.ui.Resizeable<p>
 * Makes a component resizeable. Does not work if there's a layout manager
 * messing with things<p>
 * <code>
 *  usage: 
 *  			Component c = ...
 *  			new Resizeable(c);
 *  			parent.add(c);
 *  </code>
 */
 
public class Resizeable extends MouseAdapter implements MouseMotionListener {
	int fix_pt_x = -1;
	int fix_pt_y = -1;
    Component mResizeable;
	private Double mAspectRatio;
	private Cursor mDefaultCursor;
	private Dimension mMinSize, mMaxSize;

	public static final int RESIZE_MARGIN_SIZE = 4;
	
	public Resizeable(Component c) {
		mResizeable = c;
		c.addMouseListener(this);
		c.addMouseMotionListener(this);
	}
	
	public boolean isRespectingMinSize() {
		return mMinSize == null;
	}
	
	public boolean isRespectingMaxSize() {
		return mMinSize == null;
	}
	
	public void setRespectingMaxSize(boolean b) {
		mMaxSize = b ? mResizeable.getMaximumSize() : null;
	}
	
	public void setRespectingMinSize(boolean b) {
		mMinSize = b ? mResizeable.getMinimumSize() : null;
	}
	
	public void setMaintainAspect(double x, double y) {
		setMaintainAspect(new Double(Math.atan2(y, x)));
	}
	
	public void setMaintainAspect(Double angle) {
		mAspectRatio = angle;
	}
 
	public void mouseEntered(MouseEvent me) {
		setCursorType(me.getPoint());
	}
	
	private void setCursorType(Point p) {
		//boolean n = p.y <= WindowUtilities.RESIZE_MARGIN_SIZE;
		//boolean w = p.x <= WindowUtilities.RESIZE_MARGIN_SIZE;
		boolean s = p.y + RESIZE_MARGIN_SIZE >= mResizeable.getHeight();
		boolean e = p.x + RESIZE_MARGIN_SIZE >= mResizeable.getWidth();
		if (e) {
			if (s) {
				mResizeable.setCursor(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
				return;
			}
			mResizeable.setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
			return;
		}
		if(s) {
			mResizeable.setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
			return;
		}
		else if (mDefaultCursor != null) {
			mResizeable.setCursor(mDefaultCursor);
		}
	}
	
	public void setDefaultMouseCursor(Cursor c) {
		mDefaultCursor = c;
	}
	
	public void mouseExited(MouseEvent me) {
//		if (mOldcursor != null)
//			((Component)me.getSource()).setCursor(mOldcursor);
//		mOldcursor = null;
	}
	
    public void mousePressed(MouseEvent me) {
		Cursor c = mResizeable.getCursor();
		Point loc = mResizeable.getLocation();
		if (c.equals(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR))) {
			fix_pt_x = loc.x;
			fix_pt_y = loc.y;
			return;
		}			
		if (c.equals(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR))) {
			fix_pt_x = loc.x;
			fix_pt_y = -1;
			return;
		}
		if (c.equals(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR))) {
			fix_pt_x = -1;
			fix_pt_y = loc.y;
			return;
		}
    }
	
	public void mouseReleased(MouseEvent me) {
		fix_pt_x = -1;
		fix_pt_y = -1;
	}
	
	public void mouseMoved(MouseEvent me) {
		setCursorType(me.getPoint());
	}
	
	public void mouseDragged(MouseEvent me) {
		Point p = me.getPoint();
		if (fix_pt_x == -1 && fix_pt_y == -1) return;
		int width = fix_pt_x == -1 ? mResizeable.getWidth() : p.x;
		int height = fix_pt_y == -1 ? mResizeable.getHeight() : p.y;
		if (mMinSize != null) {
			width = width < mMinSize.width ? mMinSize.width : width; 
			height = height < mMinSize.height ? mMinSize.height : height;
		}
		if (mMaxSize != null) {
			width = width > mMaxSize.width ? mMaxSize.width : width; 
			height = height > mMaxSize.height ? mMaxSize.height : height;
		}
		if (mAspectRatio == null) {
			mResizeable.setSize(new Dimension(width > 1 ? width : 1, height > 1 ? height : 1));
		}
		else {
			//do something
			double distance = Math.sqrt(width * width + height * height);
			width = (int) (distance * Math.cos(mAspectRatio));
			height = (int) (distance * Math.sin(mAspectRatio));
			if (mMinSize != null) {
				width = width < mMinSize.width ? mMinSize.width : width; 
				height = height < mMinSize.height ? mMinSize.height : height;
			}
			if (mMaxSize != null) {
				width = width > mMaxSize.width ? mMaxSize.width : width; 
				height = height > mMaxSize.height ? mMaxSize.height : height;
			}
			mResizeable.setSize(new Dimension(width > 1 ? width : 1, height > 1 ? height : 1));
		}
	}
	
	//For Testing
/*	
	public static void main(String args[]) {
		JPanel jp = new JPanel();
		JButton jb = new JButton("hello");
		JButton jb2 = new JButton("hello2");
		jp.setPreferredSize(new Dimension(300,300));
		jp.setSize(300,300);
		jp.setLayout(null);
		jp.add(jb);
		jp.add(jb2);
		jb.setSize(20,20);
		jb.setLocation(2,2);
		jb2.setSize(30,20);
		jb2.setLocation(30,10);
		Resizeable _r = new Resizeable(jb);
		_r.setDefaultMouseCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		Resizeable r = new Resizeable(jb2);
		r.setMaintainAspect(30, 20);
		r.setDefaultMouseCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		jb2.setMinimumSize(new Dimension(15,10));
		jb2.setMaximumSize(new Dimension(210,140));
		r.setRespectingMaxSize(true);
		r.setRespectingMinSize(true);
		JFrame jf = new JFrame();
		jf.add(jp);
		jf.setDefaultCloseOperation(jf.EXIT_ON_CLOSE);
		jf.setLocation(100,100);
		jf.pack();
		jf.setVisible(true);
	}
	//*/
}