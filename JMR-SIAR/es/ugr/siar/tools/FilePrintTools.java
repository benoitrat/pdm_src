/**
 * 
 */
package es.ugr.siar.tools;

import java.io.PrintStream;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 9 févr. 08
 *
 */
public class FilePrintTools {

	
	public static void printMx(PrintStream pStream, double[][] mx) {
		for(int i=0;i<mx.length;i++) {
			for(int j=0;j<mx[i].length;j++) {
				pStream.print(mx[i][j]+"\t");
			}
			pStream.println();
		}
	}
	
	public static void printMx(PrintStream pStream, int[][] mx) {
		for(int i=0;i<mx.length;i++) {
			for(int j=0;j<mx[i].length;j++) {
				pStream.print(mx[i][j]+"\t");
			}
			pStream.println();
		}
	}
	
	public static void printMx(PrintStream pStream, float[][] mx) {
		for(int i=0;i<mx.length;i++) {
			for(int j=0;j<mx[i].length;j++) {
				pStream.print(mx[i][j]+"\t");
			}
			pStream.println();
		}
	}
	
}
