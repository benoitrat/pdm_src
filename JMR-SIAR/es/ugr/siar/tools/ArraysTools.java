/**
 * 
 */
package es.ugr.siar.tools;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 12 janv. 08
 *
 */
public class ArraysTools {

	
	
    /**
     * Merge any count of Array Object.
     * This method doesn't work with primitive type.
     *
     * @param arrays manay arrays
     * @return merged array
     */
    @SuppressWarnings ("unchecked")
    public static <T> T[] arrayMerge(T[]... arrays) {
        int count = 0;
        for (T[] array : arrays) {
            count += array.length;
        }
        // create new array
        T[] rv = (T[]) Array.newInstance(arrays[0][0].getClass(),count);
        int start = 0;
        for (T[] array : arrays) {
            System.arraycopy(array,0,rv,start,array.length);
            start += array.length;
        }
        return (T[]) rv;
    }
    
    public static int[] arrayMerge(int[]... arrays) {
        int count = 0;
        for (int[] array : arrays) {
        	if(array == null) continue;
            count += array.length;
        }
        int [] rv = new int[count];
        int start=0;
        for (int[] array : arrays) {
        	if(array == null ) continue;
            System.arraycopy(array,0,rv,start,array.length);
            start += array.length;
        }
        return rv;
    }
    
    
    public static int[] toPrimitiveVec(Set<Integer> s) {
    	Integer[] ob = new Integer[s.size()];
    	return toPrimitiveVec(s.toArray(ob));
    }
    
    public static int [] toPrimitiveVec(List<Integer> l) {
    	Integer[] ob = new Integer[l.size()];
    	return toPrimitiveVec(l.toArray(ob));
    }
    
    public static int [] toPrimitiveVec(Integer[] ob) {
    	int [] rv = new int[ob.length];
    	for(int i=0;i<ob.length;i++) {
    		rv[i] = ob[i].intValue();
    	}
    	return rv;
    }
    
    //TODO: See if we can do something with this
//    public static List toList2(PrimitiveType[] tab) {
//    	
//    	switch(tab[0].getKind()) {
//    	case INT:
//	    	List<Integer> l = new ArrayList<Integer>();
//	    	for(int i=0;i<tab.length;i++) {
//	    		l.add(tab[i].);
//	    	}
//	    	return l;
//    	}
//    }
    
    public static Integer[] toArray(int [] tab) {
    	Integer[] tabObj = new Integer[tab.length];
    	for(int i=0;i<tab.length;i++) {
    		tabObj[i]=tab[i];
    	}
    	return tabObj;
    }
    
    public static List<Integer> toList(int [] tab) {
    	List<Integer> l = new ArrayList<Integer>();
    	for(int i=0;i<tab.length;i++) {
    		l.add(tab[i]);
    	}
    	return l;
    }
    
    public static Set<Integer> toSet(int [] tab) {
    	Set<Integer> s = new HashSet<Integer>();
    	for(int i=0;i<tab.length;i++) {
    		s.add(tab[i]);
    	}
    	return s;
    }
    
    public static double[] toPrimiviteDVec(int[] tab) {
    	double[] rv= new double[tab.length];
    	for(int i=0;i<tab.length;i++) 
    		rv[i] = (double) tab[i];
    	return rv;
    }
    
    public static double[] toPrimDVec(float[] tab) {
       	double[] rv= new double[tab.length];
    	for(int i=0;i<tab.length;i++) 
    		rv[i] = (double) tab[i];
    	return rv;
    }
    
    public static int[] toPrimitiveIVec(double[] tab) {
    	int[] rv= new int[tab.length];
    	for(int i=0;i<tab.length;i++) 
    		rv[i] = (int) tab[i];
    	return rv;
    }
    
    public static Double[] toDVec(float[] tab) {
      	Double[] rv= new Double[tab.length];
    	for(int i=0;i<tab.length;i++) 
    		rv[i] = new Double(tab[i]);
    	return rv;
    }
    
}
