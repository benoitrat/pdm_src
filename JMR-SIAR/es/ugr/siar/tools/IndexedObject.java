/**
 * 
 */
package es.ugr.siar.tools;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 11 janv. 08
 *
 */
public class IndexedObject implements Comparable<IndexedObject> {
	
	String str;
	int id;
	Object o;
	
	public IndexedObject(int id, String str, Object o) {
		this.id=id;
		this.str=str;
		this.o=o;
	}
	
	public String toString() {
		return str;
	}
	
	public int getId() {
		return id;
	}
	
	public Object getObject() {
		return o;
	}
	
	//TODO: Implements this method
	public int compareTo(IndexedObject sio) {
		if(sio.o instanceof Integer) {
			
		}
		return 0;
	}

}
