/**
 * 
 */
package es.ugr.siar.gui;

import java.io.File;
import java.util.List;

import javax.swing.JComponent;

import es.ugr.siar.db.DatabaseManager;
import es.ugr.siar.db.FileScore;
import es.ugr.siar.db.SerializeDescTools;
import es.ugr.siar.ip.ImageJMR;
import es.ugr.siar.ip.desc.VisualDescriptor;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;

/**
 * @author neub
 *
 */
public class CompVDescNode {

	private static final long serialVersionUID = 1L;
	int descType;
	private JFrame jFrame = null;  //  @jve:decl-index=0:visual-constraint="114,17"
	private JPanel jContentPane = null;
    
	CompVDescNode(int descType) {
		this.descType = descType;
		
	}
	
	public String toString() {
		return "comp. with "+VisualDescriptor.getSmallName(descType);
	}
	
	public void run(ImageJMR imSrc) {
		VisualDescriptor vDesc = VisualDescriptor.getInstance(descType);
		//TODO: Check if exist in database.
		vDesc.extract(imSrc);
		DatabaseManager dbManager = new DatabaseManager("all_photographers_now"); 
		List l = dbManager.compare(vDesc);
		JThumbsWindows thumbsWin = new JThumbsWindows();
		thumbsWin.setTitle("Result for "+VisualDescriptor.getSmallName(descType));
		thumbsWin.setThumbsImage(l);
		
	}
	
	
	public void runOnDirectory(JComponent jCmp,ImageJMR imSrc) {
		List scoreList = SerializeDescTools.compareDirDescriptor(new File("/home/neub/pdm/db/myphotos/"),imSrc.getSrcfile(),this.descType);
		JFrame jframe = getJFrame();
		jframe.setTitle("Result for "+VisualDescriptor.getSmallName(descType));
		Container c =  jframe.getContentPane();
		for(int i=0;i<Math.min(scoreList.size(),20);i++) {
			if(scoreList.get(i) instanceof FileScore) {
				FileScore tmp = (FileScore)scoreList.get(i);
				ImageJMR im = ImageJMR.loadFromFile(tmp.getAbsoluteFile());
				//PrintStream
				ThumbImage tIm = new ThumbImage(im,new my.PrintfFormat("%2d").sprintf(i)+"# | "+
						new my.PrintfFormat("%3.3f").sprintf((float)(100*tmp.getScore()))+" %");
				//ImageIcon imIco = new ImageIcon(tmp.getAbsolutePath(),i+". "+tmp.getScore());
				//JLabel jlab = new JLabel(imIco);
				c.add(tIm);
			}
		}
		jframe.setVisible(true);
	}

	/**
	 * This method initializes jFrame	
	 * 	
	 * @return javax.swing.JFrame	
	 */
	private JFrame getJFrame() {
		if (jFrame == null) {
			jFrame = new JFrame();
			jFrame.setTitle("Result");
			jFrame.setSize(new Dimension(600, 600));  // Generated
			jFrame.setContentPane(getJContentPane());  // Generated
			//jFrame.setDefaultCloseOperation(JFrame.);
		}
		else jFrame.removeAll(); 
		return jFrame;
	}

	/**
	 * This method initializes jContentPane	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			GridLayout gridLayout = new GridLayout();
			gridLayout.setRows(5);  // Generated
			gridLayout.setColumns(4);  // Generated
			jContentPane = new JPanel();
			jContentPane.setName("Result");  // Generated
			jContentPane.setLayout(gridLayout);  // Generated
		}
		return jContentPane;
	}

}
