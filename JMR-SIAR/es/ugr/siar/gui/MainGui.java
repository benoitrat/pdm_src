/**
 * 
 */
package es.ugr.siar.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.JTabbedPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;

import es.ugr.siar.gui.menu.MenuConstruct;
import es.ugr.siar.gui.menu.MenuMySQL;
import es.ugr.siar.gui.menu.MenuResult;
import es.ugr.siar.gui.menu.MenuShow;
import es.ugr.siar.gui.menu.MenuTest;
import es.ugr.siar.gui.menu.MenuTrain;
import es.ugr.siar.ip.*;
import es.ugr.siar.ip.colspace.ColorSpaceJMR;
import es.ugr.siar.ip.desc.VisualDescriptor;

import javax.swing.JCheckBoxMenuItem;


/**
 * @author neub
 *
 */
public class MainGui extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel mainPanel = null;

	private JLabel jSouthLabel = null;

	private JMenuBar mainMenuBar = null;

	private JMenu menuFile = null;

	private JMenu menuEdit = null;

	private JMenuItem menuFileOpen = null;

	private JMenuItem MenuFileSave = null;

	private JPanel jWestPanel = null;

	private JTree imProcTree = null;

	private JFileChooser fc = null;  //  @jve:decl-index=0:visual-constraint="177,215"

	private ImageJMR imageInput = null;

	private JTabbedPane jCenterTabbedPan = null;

	private JImagePanel jCenterPanOriginal = null;

	private JTextArea jExifTextArea = null;
	
	public static ImageJMR mainImageFile = null;
	public static String sqlToken = null;

	
	private JMenu menuShow = null;
	private JMenu menuConstruct = null;
	private JMenu menuTrain = null;
	private JMenu menuResult = null;
	private JMenu menuTest = null;
	private JMenu menuMySQL = null;

	private JCheckBoxMenuItem menuShowExif = null;

	/**
	 * This method initializes jFileChooser	
	 * 	
	 * @return javax.swing.JFileChooser	
	 */
	private JFileChooser getJFileChooser() {
		if (fc == null) {
			fc = new JFileChooser(); 
			//FileNameExtensionFilter filter = new FileNameExtensionFilter("Only Images", "jpg","png");
			ImageFilter filter = new ImageFilter();
			fc.setFileFilter(filter);
			fc.setCurrentDirectory(new File(my.Config.openDir));
			fc.setAccessory(new ImagePreview(fc));

		}
		return fc;
	}


	/**
	 * This method initializes mainMenuBar	
	 * 	
	 * @return javax.swing.JMenuBar	
	 */
	private JMenuBar getMainMenuBar() {
		if (mainMenuBar == null) {
			mainMenuBar = new JMenuBar();
			mainMenuBar.add(getMenuFile());
			mainMenuBar.add(getMenuEdit());
			
			menuShow = new JMenu("Show");  
			MenuShow.fillJMenu(menuShow);
			mainMenuBar.add(menuShow);
			
			menuConstruct = new JMenu("Construct");  
			MenuConstruct.fillJMenu(menuConstruct);
			mainMenuBar.add(menuConstruct);
			
			menuTrain = new JMenu("Train XP");  
			MenuTrain.fillJMenu(menuTrain);
			mainMenuBar.add(menuTrain);
			
			menuResult = new JMenu("Result");
			MenuResult.fillJMenu(menuResult);
			mainMenuBar.add(menuResult);
			
			mainMenuBar.add(getMenuMySQL());
			mainMenuBar.add(getMenuTest());  // Generated
		}
		return mainMenuBar;
	}

	/**
	 * This method initializes menuFile	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getMenuFile() {
		if (menuFile == null) {
			menuFile = new JMenu();
			menuFile.setText("File");
			menuFile.add(getMenuFileOpen());
			menuFile.add(getMenuFileSave());
		}
		return menuFile;
	}

	/**
	 * This method initializes menuEdit	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getMenuEdit() {
		if (menuEdit == null) {
			menuEdit = new JMenu();
			menuEdit.setText("Edit");
			JMenuItem setToken = new JMenuItem("set MySQL Token");
			setToken.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(ActionEvent e) {
					sqlToken = (String)JOptionPane.showInputDialog(null,
							"Give the MySQL token value:\n","Question ?",JOptionPane.PLAIN_MESSAGE,
							null,null,"9e8aa0af46e3056de047cb54b3186ea6");					
				}
				
				
			});
			menuEdit.add(setToken);
			
		}
		return menuEdit;
	}

	/**
	 * This method initializes menuFileOpen	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getMenuFileOpen() {
		if (menuFileOpen == null) {
			menuFileOpen = new JMenuItem();
			menuFileOpen.setText("Open");
			menuFileOpen.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					System.out.println("open actionPerformed()");
					int returnVal = getJFileChooser().showOpenDialog(MainGui.this);
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						File file = fc.getSelectedFile();
						//This is where a real application would open the file.
						System.out.println("Opening: " + file.getName());
						jSouthLabel.setText("Input Image : "+ file.getName() +" loaded");
						//Load image files
						imageInput = ImageJMR.loadFromFile(file);
//						jExifTextArea.setVisible(false);
//						String exifStr = ExifFactory.toString(imageInput.getSrcfile());
//						if(exifStr != "") {
//							jExifTextArea.setText(exifStr);
//							menuShowExif.setEnabled(true);
//						}
//						else menuShowExif.setEnabled(false);
						jCenterPanOriginal.setImage(imageInput);
						jCenterPanOriginal.repaint();



					} else {
						System.out.println("Open command cancelled by user.");
					}

				}
			});
		}
		return menuFileOpen;
	}


	/**
	 * This method initializes MenuFileSave	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getMenuFileSave() {
		if (MenuFileSave == null) {
			MenuFileSave = new JMenuItem();
			MenuFileSave.setText("Save");
		}
		return MenuFileSave;
	}

	/**
	 * This method initializes jWestPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJWestPanel() {
		if (jWestPanel == null) {
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.fill = GridBagConstraints.BOTH;
			gridBagConstraints.weighty = 1.0;
			gridBagConstraints.weightx = 1.0;
			jWestPanel = new JPanel();
			jWestPanel.setLayout(new GridBagLayout());
			jWestPanel.add(getImProcTree(), gridBagConstraints);
		}
		return jWestPanel;
	}

	/**
	 * This method initializes imProcTree	
	 * 	
	 * @return javax.swing.JTree	
	 */
	private JTree getImProcTree() {
		if (imProcTree == null) {
			DefaultMutableTreeNode top = new DefaultMutableTreeNode("Image Proc");
			DefaultMutableTreeNode colorSpaceNode = new DefaultMutableTreeNode("Color Space");
			DefaultMutableTreeNode descriptorsNode = new DefaultMutableTreeNode("Descriptors");
			DefaultMutableTreeNode compDescNode = new DefaultMutableTreeNode("Compare Desc");

			DefaultMutableTreeNode GrayColorNode = new DefaultMutableTreeNode(new ColorSpaceNode(ColorSpaceJMR.CS_GRAY));
			DefaultMutableTreeNode YCbCrColorNode = new DefaultMutableTreeNode(new ColorSpaceNode(ColorSpaceJMR.CS_YCbCr));
			DefaultMutableTreeNode HSIColorNode = new DefaultMutableTreeNode(new ColorSpaceNode(ColorSpaceJMR.CS_HSI));
			DefaultMutableTreeNode HSVColorNode = new DefaultMutableTreeNode(new ColorSpaceNode(ColorSpaceJMR.CS_HSV));
			DefaultMutableTreeNode LabColorNode = new DefaultMutableTreeNode(new ColorSpaceNode(ColorSpaceJMR.CS_Lab));
			DefaultMutableTreeNode HMMDColorNode = new DefaultMutableTreeNode(new ColorSpaceNode(ColorSpaceJMR.CS_HMMD));
			DefaultMutableTreeNode LuvColorNode = new DefaultMutableTreeNode(new ColorSpaceNode(ColorSpaceJMR.CS_Luv));		
			colorSpaceNode.add(GrayColorNode);
			colorSpaceNode.add(YCbCrColorNode);
			colorSpaceNode.add(HSIColorNode);
			colorSpaceNode.add(HSVColorNode);
			colorSpaceNode.add(HMMDColorNode);
			colorSpaceNode.add(LabColorNode);
			colorSpaceNode.add(LuvColorNode);

			DefaultMutableTreeNode DescSCDNode = new DefaultMutableTreeNode(new VDescriptorNode(VisualDescriptor.DESC_MPEG7_SCD));
			DefaultMutableTreeNode DescCSDNode = new DefaultMutableTreeNode(new VDescriptorNode(VisualDescriptor.DESC_MPEG7_CSD));
			DefaultMutableTreeNode DescEHDNode = new DefaultMutableTreeNode(new VDescriptorNode(VisualDescriptor.DESC_MPEG7_EHD));
			descriptorsNode.add(DescSCDNode);
			descriptorsNode.add(DescCSDNode);
			descriptorsNode.add(DescEHDNode);
			

			top.add(colorSpaceNode);
			top.add(descriptorsNode);
			top.add(compDescNode);
			imProcTree = new JTree(top);
			imProcTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
			imProcTree.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
				public void valueChanged(javax.swing.event.TreeSelectionEvent e) {
					DefaultMutableTreeNode node = (DefaultMutableTreeNode)imProcTree.getLastSelectedPathComponent();
					if (node == null) return; //Nothing is selected.	
					if (node.isLeaf()) {
						Object nodeObj = node.getUserObject();
						

						if(nodeObj instanceof ColorSpaceNode) {

							ColorSpaceNode csNode = (ColorSpaceNode)nodeObj;
							System.out.println("Color Space Selected: "+csNode.toString());

							ImageJMR imOut = ColorConvertTools.convertColor(jCenterPanOriginal.getImage(),csNode.colorSpaceType);
							if(imOut !=  null) {
								JImageJMRTabbedPane jCenterConvertedPan = new JImageJMRTabbedPane(imOut); 
								jCenterTabbedPan.addTab(csNode.toString(), null,jCenterConvertedPan, null);
							}
						}
						else if(nodeObj instanceof VDescriptorNode) {
							VDescriptorNode nodeVD = (VDescriptorNode)nodeObj;
							nodeVD.run(jCenterTabbedPan,jCenterPanOriginal.getImage());
						}
					}
				}
			});
		}
		return imProcTree;
	}

	/**
	 * This method initializes jCenterTabbedPan	
	 * 	
	 * @return javax.swing.JTabbedPane	
	 */
	private JTabbedPane getJCenterTabbedPan() {
		if (jCenterTabbedPan == null) {
			jCenterTabbedPan = new JTabbedPane();
			jCenterTabbedPan.setName("TabbedPane");
			jCenterTabbedPan.addTab("Original", null, getJCenterPanOriginal(), null);
//			jCenterTabbedPan.addMouseListener(new java.awt.event.MouseAdapter() {
//			public void mouseClicked(java.awt.event.MouseEvent e) {
//			System.out.println("mouseClicked()"); // TODO Auto-generated Event stub mouseClicked()
//			}
//			});
		}
		return jCenterTabbedPan;
	}


	/**
	 * This method initializes jCenterPanOriginal	
	 * 	
	 * @return javax.swing.JPanel	
	 */

	private JImagePanel getJCenterPanOriginal() {
		if (jCenterPanOriginal == null) {
			jCenterPanOriginal = new JImagePanel();
			jCenterPanOriginal.setLayout(new GridBagLayout());
			jCenterPanOriginal.setName("");
			imageInput = ImageJMR.loadFromFile(new File(my.Config.openImage));
			jCenterPanOriginal.setImage(imageInput);
			jCenterPanOriginal.repaint();
		}
		return jCenterPanOriginal;
	}

	/**
	 * This method initializes jExifTextArea	
	 * 	
	 * @return javax.swing.JTextArea	
	 */
	private JTextArea getJExifTextArea() {
		if (jExifTextArea == null) {
			jExifTextArea = new JTextArea();
			jExifTextArea.setEditable(false);  // Generated
			//jExifTextArea.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);  // Generated
			//jExifTextArea.setLineWrap(true);  // Generated
			jExifTextArea.setVisible(false);  // Generated
			jExifTextArea.setMaximumSize(new Dimension(150,700));
		}
		return jExifTextArea;
	}



	/**
	 * This method initializes menuShow	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getMenuTest() {
		if (menuTest == null) {
			menuTest = new JMenu();
			menuTest.setText("Test");  // Generated
			MenuTest.fillJMenu(menuTest);
		}
		return menuTest;
	}
	
	/**
	 * This method initializes menuShow	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getMenuMySQL() {
		if (menuMySQL == null) {
			menuMySQL = new JMenu();
			menuMySQL.setText("MySQL Query");  // Generated
			MenuMySQL.fillJMenu(menuMySQL);
		}
		return menuMySQL;
	}




	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				my.Config.loadConfiguration();
				MainGui thisClass = new MainGui();
				thisClass.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				thisClass.setVisible(true);
			}
		});
	}

	/**
	 * This is the default constructor
	 */
	public MainGui() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 */
	private void initialize() {
		this.setSize(800, 600);
		this.setLocation(new Point(400, 100));
		this.setJMenuBar(getMainMenuBar());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setContentPane(getMainPanel());
		this.setTitle("SIAR GUI");
		this.setVisible(true);
	}

	/**
	 * This method initializes mainPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getMainPanel() {
		if (mainPanel == null) {
			jSouthLabel = new JLabel();
			jSouthLabel.setText("Open an Image");
			mainPanel = new JPanel();
			mainPanel.setLayout(new BorderLayout());
			mainPanel.add(jSouthLabel, BorderLayout.SOUTH);
			mainPanel.add(getJWestPanel(), BorderLayout.WEST);
			mainPanel.add(getJCenterTabbedPan(), BorderLayout.CENTER);
			mainPanel.add(getJExifTextArea(), BorderLayout.EAST);  // Generated
		}
		return mainPanel;
	}

}
