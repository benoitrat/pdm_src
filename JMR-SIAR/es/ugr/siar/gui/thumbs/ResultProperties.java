/**
 * 
 */
package es.ugr.siar.gui.thumbs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import es.ugr.siar.tools.IndexedGeneric;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 30 janv. 08
 *
 */
public class ResultProperties implements Comparable<ResultProperties>{
	
	List<IndexedGeneric<Float>> annotations;
	int classID;
	int classifyAs;
	float score;
	
	
	public ResultProperties(int classID, boolean classifyAs,float score) {
		annotations = new ArrayList<IndexedGeneric<Float>>();
		this.classID=classID;
		if(classifyAs) 
			this.classifyAs=1;
		else
			this.classifyAs=0;
		this.score=score;
	}
	
	
	public void addAnnotation(int id,String txt,Float score) {
		annotations.add(new IndexedGeneric<Float>(id,txt,score));
	}
	
		
	public String getStrAnnotations(boolean breakline) {
		String rv="";
		
		Collections.sort(annotations);
		Collections.reverse(annotations);
		for(IndexedGeneric<Float> a:annotations) {
			if(a.getGenericObject() > 0.3) {
				rv+=a.toString()+" ("+new my.PrintfFormat("%3.1f").sprintf((float)(100*a.getGenericObject()))+" %)";
				if(breakline) rv+="\n ";
				else rv+=", ";
			}
		}
		return rv.substring(0,rv.length()-2);
	}
	
	
	public String getCompactStrAnnotations() {
		String rv="";
		
		Collections.sort(annotations);
		Collections.reverse(annotations);
		for(IndexedGeneric<Float> a:annotations) {
			if(a.getGenericObject() > 0.3) {
				rv+=a.toString()+" ("+new my.PrintfFormat("%.2f").sprintf((float)(a.getGenericObject()))+")";
				rv+=", ";
			}
		}
		return rv.substring(0,rv.length()-2);
	}
	
	boolean isCCI() {
		return (this.classID == this.classifyAs);
	}
	boolean isICI() {
		return !isCCI();
	}
	
	boolean isFP(int classID) {
		if(classID == classifyAs)
			return isICI();
		else return false;
	}

	boolean isTP(int classID) {
		if(classID == classifyAs)
			return isCCI();
		else return false;
	}
	
	public String toString() {
		String rv="";
		rv="classID:"+classID+", classifyAs:"+classifyAs+", score:"+score;
		return rv;
	}
	
	
	
	public int compareTo(ResultProperties other) {
			if (other.score > score)  return -1; 
			else if(other.score == score) return 0; 
			else return 1; 
	}
	
	
	
}
