/**
 * 
 */
package es.ugr.siar.gui.thumbs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import es.ugr.siar.db.APNDBManager;
import es.ugr.siar.db.DatabaseManager;
import es.ugr.siar.tools.IndexedGeneric;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 10 janv. 08
 *
 */
public class WinThumbsMain extends JFrame implements MouseListener, ActionListener, KeyListener {
	
	/**     */
	protected static final long serialVersionUID = 1L;
	JPanel mainPanel;
	JPanel jPanCenter;
	JPanel jPanBottom;
	JButton previousB;
	JButton nextB;
	JLabel[] imgsIco;
	JTextField actualTf;
	int nofRow=5;
	int nofCol=4;
	int nofPhotos;
	int[] photosID;
	String[] photosTxt;
	boolean border=false;
	
	protected List<IndexedGeneric<ResultProperties>> rPropList = null;
	
	//------------- Constructors ------------
	
	public WinThumbsMain(String title, int[] photosID, String[] photosTxt) {
		super(title);
		this.photosTxt = photosTxt;
		this.photosID = photosID;
		this.nofPhotos=photosID.length;
		init();
	}
	
	public WinThumbsMain(String title,int [] photosID) {
		super(title);
		this.photosID = photosID;
		this.nofPhotos=photosID.length;
		init();
	}
	
	protected WinThumbsMain(String title) {
		super(title);
	}
	protected void setPhotosID(int[] photosID) {
		this.photosID=photosID;
		this.nofPhotos=photosID.length;
	}
	
	protected void setPhotosTxt(String[] photosTxt) {
		this.photosTxt=photosTxt;
	}
	protected void setResultPropList(List<IndexedGeneric<ResultProperties>> rProp) {
		this.rPropList= rProp;
	}
	

	protected void init() {
		setSize(550,550);
		mainPanel = new JPanel();
		setContentPane(mainPanel);
		this.jPanCenter = new JPanel();
		this.jPanBottom = new JPanel();
	

		mainPanel.setLayout(new BorderLayout());
		mainPanel.add(jPanBottom, BorderLayout.SOUTH);
		mainPanel.add(jPanCenter, BorderLayout.CENTER);
		this.setVisible(true);

		
		jPanCenter.setLayout(new GridLayout(nofRow,nofCol));
		jPanCenter.setVisible(true);
		imgsIco = new JLabel[nofCol*nofRow];
		for(int i=0;i<nofCol*nofRow;i++) {
			imgsIco[i] = new JLabel();
			imgsIco[i].addMouseListener(this);
			jPanCenter.add(imgsIco[i]);
			
		}
		
		previousB = new JButton(" <- ");
		previousB.addActionListener(this);
		previousB.setEnabled(false);
		nextB = new JButton(" -> ");
		nextB.addActionListener(this);
		actualTf = new JTextField("0",5);
		actualTf.addKeyListener(this);
		
		jPanBottom.add(previousB);
		jPanBottom.add(actualTf);
		jPanBottom.add(new JLabel("/"+nofPhotos));
		jPanBottom.add(nextB);
		
		showImage(0);
	}
	
	
	
	//------------- Methods to move between images ------------
	
	public void showImage(int start) {
		int end=Math.min(nofPhotos, start+nofCol*nofRow);
		String txt="";
		ImageIcon imIco;
		File fImg;
		int count=0;
		DatabaseManager dbManager = new APNDBManager();
		for(int i=start; i<end;i++) {
			//Reset variables at each image loaded
			txt="";
			imIco=null;
			
			//Load the thumbs image if it exist
			fImg = dbManager.ImgDbThumbsFile(photosID[i]);
			if(fImg != null) {
				imIco = new ImageIcon(fImg.getAbsolutePath());
				imgsIco[count].setIcon(imIco);
			}
			
			//Load the text if it exist
			if(photosTxt != null) txt=photosTxt[i];
			else txt = new my.PrintfFormat("%05d").sprintf(photosID[i]);
			imgsIco[count].setToolTipText(txt);
			imgsIco[count].setName(""+i);
			specialSubClassAction(imgsIco[count], i);
			count++;
		}

		
		//Fill with nothing if there is not 20 images to put.
		for(int i=count;i<nofRow*nofCol;i++) {
			imgsIco[i].setIcon(null);
			imgsIco[i].setText("");
		}
	}
	
	protected void 	specialSubClassAction(JLabel l, int index) {
		//Nothing in this class ;) See the subclass
	}
	
	protected void manageStartPos(int startPos) {
		if(startPos >= 0 && startPos < nofPhotos) {
			
			//Disable/Enable Previous button
			if(startPos >= nofRow*nofCol) previousB.setEnabled(true);
			else previousB.setEnabled(false);
			
			//Disable/Enable Next Button
			if(startPos < nofPhotos-nofRow*nofCol) nextB.setEnabled(true);
			else nextB.setEnabled(false);
			
			//Set the txt field
			actualTf.setText(""+startPos);
			showImage(startPos);
		}
	}
	
	protected void openPhoto(int index) {
			new WinImgDb(photosID[index]);
			
	}
	

	
	//------------- Listener on the elements ----------------
	
	public void mouseClicked(MouseEvent e) {
		if(e.getClickCount() > 1) {
			if(e.getSource() instanceof JLabel) {
				String strId = ((JLabel)e.getSource()).getName();
				int index = (new Integer(strId)).intValue();
				openPhoto(index); 
			}
		}
	}

	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();
		if(o.equals(previousB)) {
			int startPos = (new Integer(actualTf.getText())).intValue();
			manageStartPos(startPos-nofCol*nofRow);
		}
		if(o.equals(nextB)) {
			int startPos = (new Integer(actualTf.getText())).intValue();
			manageStartPos(startPos+nofCol*nofRow);
		}
		
	}
	
	public void keyTyped(KeyEvent e) {
		if(e.getKeyChar() == KeyEvent.VK_ENTER) {
			int startPos = (new Integer(actualTf.getText())).intValue();
			manageStartPos(startPos);
		}
	}

	public void keyPressed(KeyEvent e) {}
	public void keyReleased(KeyEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	
	

}
