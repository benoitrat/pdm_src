/**
 * 
 */
package es.ugr.siar.gui.thumbs;

import java.awt.Color;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import es.ugr.siar.tools.IndexedGeneric;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 30 janv. 08
 *
 */
@SuppressWarnings("serial")
public class WinThumbsDist extends WinThumbsMain {
	

	
	public WinThumbsDist(String title,List<IndexedGeneric<Float>> list) {
		super(title);
		int[] photosIdSorted = new int[list.size()];
		String [] photosTxt = new String[list.size()];
		for(int i=0;i<list.size();i++) {
			photosIdSorted[i] = list.get(i).getId();
			photosTxt[i] = list.get(i).toString();
		}
		setPhotosID(photosIdSorted);
		setPhotosTxt(photosTxt);
		init();
	}
	
	@Override
	protected void 	specialSubClassAction(JLabel l, int index) {
		l.setBorder(BorderFactory.createLineBorder(Color.black));
	}
	
}
