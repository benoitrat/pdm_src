/**
 * 
 */
package es.ugr.siar.gui.thumbs;

import java.awt.Color;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import es.ugr.siar.db.APNDBManager;
import es.ugr.siar.db.DatabaseManager;
import es.ugr.siar.tools.IndexedGeneric;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 30 janv. 08
 *
 */
@SuppressWarnings("serial")
public class WinThumbsProba extends WinThumbsMain {
	
	public WinThumbsProba(String title,List<IndexedGeneric<ResultProperties>> list) {
		super(title);
		setResultPropList(list);
		int[] photosIdSorted = new int[list.size()];
		String [] photosTxt = new String[list.size()];
		for(int i=0;i<list.size();i++) {
			photosIdSorted[i] = list.get(i).getId();
			photosTxt[i] = list.get(i).toString();
		}
		setPhotosID(photosIdSorted);
		setPhotosTxt(photosTxt);
		init();
	}
	
	@Override
	protected void 	specialSubClassAction(JLabel l, int index) {
		ResultProperties rProp=rPropList.get(index).getGenericObject(); 
		if(rProp.isFP(1)) 
			l.setBorder(BorderFactory.createLineBorder(Color.red));
		else 
			l.setBorder(null);
		if(rProp.isFP(0))
			l.setBorder(BorderFactory.createLineBorder(Color.orange));
	}
	
	@Override
	protected void openPhoto(int index) {
		ResultProperties rProp=rPropList.get(index).getGenericObject();
		new WinImgDb(photosID[index],rProp);
		int pID=photosID[index];
		String print="\\centering \\includegraphics[width=5cm]{images/results/samples/"+pID+"} & \\cr \\\\\n";
		print+=rProp.getCompactStrAnnotations();
		System.out.println(print);
		DatabaseManager dbMan = new APNDBManager();
		
		System.out.println("cp "+dbMan.ImgDbPath(pID)+" ./"
				+dbMan.ImgDbFile(pID).getName());
			
		if(rProp.isFP(1))
			System.out.println("FP_s1 = ClassSetted 0 and classGuessed 1");
		if(rProp.isFP(0))
			System.out.println("FP_s0 = ClassExpected 1 and classGuessed 0");
		if(rProp.isTP(1))
			System.out.println("TP_s1 = ClassExpected 1 and classGuessed 1");
		if(rProp.isTP(0))
			System.out.println("TP_s0 = ClassExpected 0 and classGuessed 0");
	}
	
}
