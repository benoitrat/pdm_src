/**
 * 
 */
package es.ugr.siar.gui;

import java.awt.BorderLayout;
import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import es.ugr.siar.db.APNDBManager;
import es.ugr.siar.db.DatabaseManager;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 10 janv. 08
 *
 */
public class WinImgDb extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	JPanel mainPanel;
	JPanel jPanCenter;
	JPanel jPanBottom;
	int photoID;
	DatabaseManager dbManager;
	
	public WinImgDb(int photoID) {
		super(""+photoID);
		this.photoID=photoID;
		init();
		dbManager = new APNDBManager();
		loadImage(photoID);
		loadData(photoID);
	}
	
	

	private void init() {
		setSize(600,600);
		mainPanel = new JPanel();
		setContentPane(mainPanel);
		this.jPanCenter = new JPanel();
		this.jPanBottom = new JPanel();
		
		mainPanel.setLayout(new BorderLayout());
		mainPanel.add(jPanBottom, BorderLayout.SOUTH);
		mainPanel.add(jPanCenter, BorderLayout.CENTER);
		this.setVisible(true);
	}
	


	/**
	 * @param photoID
	 */
	private void loadImage(int photoID) {
		//TODO: Make database independant
		File fImg = dbManager.ImgDbFile(photoID);
		JLabel imLabel = new JLabel();
		if(fImg != null) {
			ImageIcon imIco = new ImageIcon(fImg.getAbsolutePath());
			imLabel.setIcon(imIco);
		}
		jPanCenter.add(imLabel);
	}
	
	/**
	 * @param photoID
	 */
	private void loadData(int photoID) {
		JLabel jl1 = new JLabel("Keywords");
		JTextArea jta1 = new JTextArea(10,30);
		jta1.setAutoscrolls(true);
		jta1.setText(dbManager.getKeywords(photoID));
		jPanBottom.add(jl1);
		jPanBottom.add(jta1);
		JButton jb = new JButton("Go to MySQL");
		jb.addActionListener(this);
		jPanBottom.add(jb);
	}



	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JButton) {
			if(MainGui.sqlToken != null) {
				try {
					Runtime.getRuntime().exec("firefox http://localhost/phpmyadmin/sql.php?db="+my.Config.dbDefaultName+
							"&table=Keywords&token="+MainGui.sqlToken+"&pos=0"+
							"&sql_query=SELECT+%2AFROM+Keywords%20WHERE%20Photo_ID="+photoID);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			else {
				System.err.println("Set MySQL Token in\n Edit> Set SQL Token");
			}
		}	
		
	}
}
