package es.ugr.siar.gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.JPanel;

import es.ugr.siar.ip.ImageJMR;

public class JImagePanel extends JPanel implements MouseListener {

	/**
	 * Serial for serializable class 
	 */
	private static final long serialVersionUID = 3168088414417846824L;

//	private Graphics gBuffer = null;
//	private Graphics gShow = null;
	private BufferedImage img = null;
	private File file = null;
	
	private int maxImSize = 600;
	
	/**
	 * Constructeur by default: 
	 * The panel will draw noimage text !bipolaire
	 */
	public JImagePanel() {
	}
	/**
	 * Constructeur with image
	 * @param img
	 */
	public JImagePanel(BufferedImage img) {
		this.img = img;
		this.file = null;
	}
	/**
	 * Constructeur with image
	 * @param img
	 */
	public JImagePanel(BufferedImage img,File fname) {
		this.img = img;
		this.file = fname;
	}
	
	/**
	 * Set & Get Function
	 * @param img
	 */
	public void setImage(BufferedImage img) {
		this.img = scaleImage(img,this.img,this.maxImSize);
	}
	public void setImage(BufferedImage img,File fname) {
		this.setImage(img);
		this.file = fname;
	}
	public void setImage(ImageJMR img) {
		this.img = (BufferedImage)img;
		this.file = img.getSrcfile();
	}
	
	
	public BufferedImage getImage() {
		return this.img;
	}
	
	public File getFile() {
		return this.file;
	}
	
	public ImageJMR getImageJMR() {
		return new ImageJMR(this.img,this.file);
	}
	
    private BufferedImage scaleImage(BufferedImage src, BufferedImage dst, int maxImSize) {
		double scaleRatio = 1.0;
		AffineTransform imResize = new AffineTransform();
		
		//Find Maximum Size & Scaling Factio
		int imSize = (int)Math.max(src.getHeight(),src.getWidth());
		if (imSize > maxImSize) {
			 scaleRatio = (double)maxImSize/(double)imSize;
			//Perform affine transformation (scaling)
			imResize.setToScale(scaleRatio, scaleRatio);
			AffineTransformOp imResizeOp = new AffineTransformOp(imResize,AffineTransformOp.TYPE_BILINEAR );
			return imResizeOp.filter(src,dst);
		}
		else {
			maxImSize = imSize;
			return dst;
		}

    }
	
	
	public void paint(Graphics g )   {
	   // don’t call super.paint().  update() has already cleared the screen.
	   // You might want to override update to just call paint() if that clearing is not needed.

		super.paintComponent(g);
		
		if(img != null) {
			Dimension d = getSize();
			//System.out.println("size panel = "+d.getWidth()+","+d.getHeight());
			int maxPanSize = (int)Math.max(d.getHeight(),d.getWidth());

			//Image bigger than pannel -> resize image
			if(maxPanSize <= maxImSize) {
				g.drawImage(img,0,0,d.width,d.height,this);
			}
			//Pannel bigger than image -> center image
			else {
				int startX = (d.width-img.getWidth())/2;
				int startY = (d.height - img.getHeight())/2;
				g.drawImage(img,startX,startY,this);
			}
		}
	}
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	public void mouseEntered(MouseEvent e) {
		System.out.println("rgb @ x:"+e.getX()+",y:"+e.getY());
		
	}
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	public void mousePressed(MouseEvent e) {
		System.out.println("pressed rgb @ x:"+e.getX()+",y:"+e.getY());
		
	}
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	

	
//End object	
}
