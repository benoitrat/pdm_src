/**
 * 
 */
package es.ugr.siar.gui;

import es.ugr.siar.ip.colspace.ColorSpaceJMR;

/**
 * @author neub
 *
 */
public class ColorSpaceNode {

	private static final long serialVersionUID = 1L;
	int colorSpaceType;
    
	ColorSpaceNode(int colorSpaceType) {
		this.colorSpaceType = colorSpaceType;
		
	}
	
	public String toString() {
		return "to "+ColorSpaceJMR.getColorSpaceName(colorSpaceType);
	}
	
}
