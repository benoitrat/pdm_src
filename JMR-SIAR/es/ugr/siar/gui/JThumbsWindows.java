/**
 * 
 */
package es.ugr.siar.gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.File;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import es.ugr.siar.db.ScoreVDescID;
import es.ugr.siar.ip.ImageJMR;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 18 déc. 07
 *
 */
public class JThumbsWindows extends JFrame {

	/**     */
	private static final long serialVersionUID = -6991245891153972889L;
	private JPanel jContentPane;
	int maxImage = 20;
	
	/**
	 * 
	 */
	public JThumbsWindows() {
		super();
		setTitle("Result");
		setVisible(true);
		setSize(new Dimension(600, 600));  // Generated
		jContentPane = getJContentPane();
		setContentPane(jContentPane); 
	}
	
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			GridLayout gridLayout = new GridLayout();
			gridLayout.setRows(5);  // Generated
			gridLayout.setColumns(4);  // Generated
			jContentPane = new JPanel();
			jContentPane.setName("Result");  // Generated
			jContentPane.setLayout(gridLayout);  // Generated
		}
		return jContentPane;
	}
	
	public void setThumbsImage(List l) {
		int maxShow = Math.min(maxImage,l.size());
		ThumbImage tIm = null;
		for(int i=0;i<maxShow;i++) {
			if(l.get(i) instanceof ScoreVDescID) {
				ScoreVDescID tmp = (ScoreVDescID)l.get(i);
				ImageJMR im = ImageJMR.loadFromFile(new File(tmp.getFileName()));
				tIm = new ThumbImage(im,new my.PrintfFormat("%2d").sprintf(i)+"# | "+
						new my.PrintfFormat("%3.3f").sprintf((float)(100*tmp.getScore()))+" %");
			}
			else {
				continue; //Jump the loop
			}
			jContentPane.add(tIm);
		}

	}

}
