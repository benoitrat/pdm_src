/**
 * 
 */
package es.ugr.siar.gui;

import java.awt.image.BufferedImage;

import javax.swing.JTabbedPane;

import es.ugr.siar.ip.ImageJMR;
import es.ugr.siar.ip.desc.VisualDescriptor;

/**
 * @author neub
 *
 */
public class VDescriptorNode {

	private static final long serialVersionUID = 1L;
	int descType;
    
	VDescriptorNode(int descType) {
		this.descType = descType;
		
	}
	
	public String toString() {
		return "to "+VisualDescriptor.getSmallName(descType);
	}
	
	
	public void run(JTabbedPane tPan,BufferedImage imSrc) {

		ImageJMR imDst = new ImageJMR(imSrc);
		VisualDescriptor desc = VisualDescriptor.getInstance(descType);
		desc.extract(imDst);
		System.out.println(desc.toString());
		
	}
}
