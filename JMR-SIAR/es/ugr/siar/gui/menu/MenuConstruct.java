package es.ugr.siar.gui.menu;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.List;

import es.ugr.siar.db.*;
import es.ugr.siar.gui.actionwin.*;
import es.ugr.siar.ip.desc.*;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.tools.IndexedGeneric;

import javax.swing.*;

public class MenuConstruct {

	static final String [] subMenuTxt = {
		"Create Tables",		//0
		"Extract MPEG7",		//1
		"Extract specific MEPG7", //2
		"Make Global Set",		//3
		"Randomize Train/Test Set",	//4
		"Make Class Model",		//5
		"Make Global SQL (bad)",		//6
		"Make Distance Stat",	//7
		"Dump in ARFF (bad)",			//8


	};

	public static void fillJMenu(JMenu menu) {
		menu.removeAll();
		for(int i=0;i<subMenuTxt.length;i++) {
			menu.add(builJMenuItem(i));
		}
	}

	private static JMenuItem builJMenuItem(final int funcType) {
		JMenuItem item = new JMenuItem(subMenuTxt[funcType]);
		item.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(ActionEvent e) {
				switch(funcType) {
				case 0:
					MenuConstruct.createTables(); 	break;
				case 1:
					MenuConstruct.extractMPEG7(); 		break;
				case 2: 
					MenuConstruct.specificExtractMPEG7(); break;
				case 3:
					MenuConstruct.makeGlobalSet();		break;
				case 4:
					MenuConstruct.makeClassTrainValidSet(); break;
				case 5:
					MenuConstruct.makeClassModel();		break;
				case 6:
					MenuConstruct.makeGlobalSQL();		break;
				case 7:
					MenuConstruct.makeDistStat(); break;
				case 8:
					MenuConstruct.dumpInFile(); break;
					
				}

			}

		});
		return item;
	}

	/**
	 * 
	 */
	protected static void dumpInFile() {
		
		//Generate windows
		WinDumpInFile actionWin = new WinDumpInFile("Dump in File",400,200,BoxLayout.Y_AXIS);
		actionWin.setBorder(true);
		
		//Retriev the classList on the db
		DatabaseManager dbManager = new APNDBManager();
		List<IndexedGeneric<String>> classList = dbManager.getClassesSyntax();
		
		//Read the File with the actual global set
		String[] cNameSelected = SetsFileTools.readGlobalClassNames();
		if(cNameSelected !=  null) Arrays.sort(cNameSelected);
		
		//Put the RadioButton for each className
		for(IndexedGeneric<String> igS: classList) {
			JRadioButton jrb = new JRadioButton(igS.toString());
			jrb.setActionCommand(""+igS.getId());
			if(cNameSelected != null) {
				if(Arrays.binarySearch(cNameSelected,igS.toString()) >= 0) 
					jrb.setSelected(true);
				else 
					jrb.setEnabled(false);
			}
			actionWin.addCenter(jrb,0);
		}
		
		//Put the One-VS-Others choice
		JCheckBox jcb1vsO = new JCheckBox("One VS Others");
		jcb1vsO.addActionListener(actionWin);
		actionWin.addCenter(jcb1vsO,1);
		
		//Put the Descriptor by checkBox
		boolean[] implvDesc = VisualDescriptor.IMPLEMENTED_DESC;
		for(int i=0;i<implvDesc.length;i++) {
			if(implvDesc[i]) {
				JCheckBox jcb = new JCheckBox(VisualDescriptor.getSmallName(i));
				jcb.setActionCommand(""+i);
				if(i != VisualDescriptor.DESC_MPEG7_SCD)
					jcb.setSelected(true);
				actionWin.addCenter(jcb,2);
			}
		}
		JRadioButton cDescExif = new JRadioButton("Exif");
		cDescExif.setActionCommand("EXIF");
		cDescExif.setSelected(true);
		actionWin.addCenter(cDescExif,2);
		
		//Use on training or testing
//		ButtonGroup gTrainTest = new ButtonGroup();
//		JRadioButton jrbTrain =  new JRadioButton("Training Set");
//		jrbTrain.setActionCommand(SetsFileTools.SetType.TRAIN.getExt());
//		JRadioButton jrbTest =  new JRadioButton("Testing Set");
//		jrbTest.setActionCommand(SetsFileTools.SetType.TEST.getExt());	
//		jrbTrain.setSelected(true);
//		gTrainTest.add(jrbTrain);
//		gTrainTest.add(jrbTest);
//		actionWin.addCenter(jrbTrain,3);
//		actionWin.addCenter(jrbTest,3);
		
	}

	/**
	 * 
	 */
	protected static void makeGlobalSQL() {
		DatabaseManager dbManager = new APNDBManager();
		List<IndexedGeneric<String>> classList = dbManager.getClassesSyntax();
		String[] cNameSelected = SetsFileTools.readGlobalClassNames();
		if(cNameSelected !=  null) Arrays.sort(cNameSelected);
		
		String sql= "CREATE TABLE PhotoClass(";
		boolean first=true;
		for(IndexedGeneric<String> igS: classList) {
			if(cNameSelected != null) {
				if(Arrays.binarySearch(cNameSelected,igS.toString()) >= 0)  {
					if(!first) sql+="\n\nUNION\n\n";
					sql+=igS.getGenericObject();
					first=false;
				}
			}
		}
		sql+=");";
		System.out.println(sql);
		
	}

	/**
	 * 
	 */
	protected static void makeGlobalSet() {
		WinMakeGlobalClass actionWin = new WinMakeGlobalClass("Make Global Set",new Dimension(400,200),BoxLayout.PAGE_AXIS);
		actionWin.setBorder(true);
		DatabaseManager dbManager = new APNDBManager();
		List<IndexedGeneric<String>> classList = dbManager.getClassesSyntax();
		String[] cNameSelected = SetsFileTools.readGlobalClassNames();
		if(cNameSelected !=  null) Arrays.sort(cNameSelected);
		
		for(IndexedGeneric<String> igS: classList) {
			JCheckBox jcb = new JCheckBox(igS.toString());
			jcb.setActionCommand(""+igS.getId());
			if(cNameSelected != null) {
				if(Arrays.binarySearch(cNameSelected,igS.toString()) >= 0) 
					jcb.setSelected(true);
			}
			actionWin.addCenter(jcb,0);
		}
		
		
	}

	/**
	 * 
	 */
	protected static void makeDistStat() {
				
		WinMakeDistStats actionWin = new WinMakeDistStats("Make Distance Stats",400,300,BoxLayout.PAGE_AXIS);
		actionWin.setBorder(true);
		DatabaseManager dbManager = new APNDBManager();
		List<IndexedGeneric<String>> classList = dbManager.getClassesSyntax();
		String[] cNameSelected = SetsFileTools.readGlobalClassNames();
		if(cNameSelected !=  null) Arrays.sort(cNameSelected);
		
		for(IndexedGeneric<String> igS: classList) {
			JCheckBox jcb = new JCheckBox(igS.toString());
			jcb.setActionCommand(""+igS.getId());
			if(cNameSelected != null) {
				if(Arrays.binarySearch(cNameSelected,igS.toString()) >= 0) { 
					jcb.setEnabled(true);
					jcb.setSelected(true);
				}
				else
					jcb.setEnabled(false);
			}
			actionWin.addCenter(jcb,0);
		}
		boolean[] implvDesc = VisualDescriptor.IMPLEMENTED_DESC;
		for(int i=0;i<implvDesc.length;i++) {
			if(implvDesc[i]) {
				JCheckBox jcb = new JCheckBox(VisualDescriptor.getSmallName(i));
				jcb.setActionCommand(""+i);
				jcb.setSelected(true);
				actionWin.addCenter(jcb,1);
			}
		}
		JCheckBox cDescExif = new JCheckBox("Exif");
		cDescExif.setActionCommand(""+16);
		actionWin.addCenter(cDescExif,2);
		
	}

	/**
	 * 
	 */
	protected static void specificExtractMPEG7() {
		DatabaseManager dbManager = new APNDBManager();
		MySQL db = dbManager.getDataBase();	
		db.queryResult("SELECT * from Classes");
		ResultSetJMR res = db.getResultSetJMR();
		
		
		String sql;
		while(res.nextRow()) {
			sql = res.getValueS("SyntaxSQL");
			dbManager.extractMPEG7(sql);
		}

		
	}

	/**
	 * Extract MPEG7 Descriptors from images to MySQL database.
	 */
	protected static void extractMPEG7() {


		//Obtain the fileName of the image for all_photographers_now
		String sql ="SELECT  Id 	FROM Photo, ExifData "+
		"WHERE ExifData.goodExif >= '1' "+
		"AND NOT Photo.Keywords = 'NULL' "+
		"AND ExifData.Photo_ID = Photo.Id "+
		"AND ExifData.Photo_ID >= 0000 "+
		"ORDER BY `Photo`.`Id` ASC "; 
		
		//Call the database with this syntax
		DatabaseManager dbManager = new APNDBManager();
		dbManager.extractMPEG7(sql);
			
	}


	/**
	 * 
	 */
	protected static void makeClassModel() {
		WinMakeClassesModels actionWin = new WinMakeClassesModels("MakeModel",new Dimension(250,200),BoxLayout.PAGE_AXIS);
		actionWin.setBorder(true);
		DatabaseManager dbManager = new APNDBManager();
		List<IndexedGeneric<String>> classList = dbManager.getClassesSyntax();
		String[] cNameSelected = SetsFileTools.readGlobalClassNames();
		if(cNameSelected !=  null) Arrays.sort(cNameSelected);
		
		for(IndexedGeneric<String> igS: classList) {
			JCheckBox jcb = new JCheckBox(igS.toString());
			jcb.setActionCommand(""+igS.getId());
			if(cNameSelected != null) {
				if(Arrays.binarySearch(cNameSelected,igS.toString()) >= 0) 
					jcb.setSelected(true);
				else
					jcb.setEnabled(false);
			}

			actionWin.addCenter(jcb,0);
		}
		boolean[] implvDesc = VisualDescriptor.IMPLEMENTED_DESC;
		for(int i=0;i<implvDesc.length;i++) {
			if(implvDesc[i]) {
				JCheckBox jcb = new JCheckBox(VisualDescriptor.getSmallName(i));
				jcb.setActionCommand(""+i);
				jcb.setSelected(true);
				actionWin.addCenter(jcb,1);
			}
		}
		JCheckBox cDescExif = new JCheckBox("Exif");
		cDescExif.setActionCommand(""+16);
		actionWin.addCenter(cDescExif,2);
	}


	protected static void makeClassTrainValidSet() {
		
		WinMakeClassesSets actionWin = new WinMakeClassesSets("MakeModel",new Dimension(250,200),BoxLayout.PAGE_AXIS);
		actionWin.setBorder(true);
		DatabaseManager dbManager = new APNDBManager();
		List<IndexedGeneric<String>> classList = dbManager.getClassesSyntax();
		
		for(IndexedGeneric<String> igS: classList) {
			JCheckBox jcb = new JCheckBox(igS.toString());
			jcb.setActionCommand(""+igS.getId());
			jcb.setSelected(true);
			actionWin.addCenter(jcb,0);
		}
		JSlider js = new JSlider(0,100,60);
		js.setMajorTickSpacing(50);
		js.setMinorTickSpacing(10);
		js.setPaintTicks(true);
		js.setPaintLabels(true);
		actionWin.addCenter(js,1);
		JLabel jl= new JLabel("Percent of training set VS test set");
		actionWin.addCenter(jl,1);
	}

	/**
	 * 
	 */
	protected static void createTables() {
		boolean[] implementedDesc = VisualDescriptor.IMPLEMENTED_DESC;
		for(int i=0;i<implementedDesc.length;i++) {
			if(implementedDesc[i]) {
				VisualDescriptor vDesc = VisualDescriptor.getInstance(i);
				System.out.println("\n---------------------------\n\n" +
						"Create MySQL Table for "+vDesc.getName()+"\n");
				System.out.println(vDesc.createTable());
			}
		}
	}

}
