package es.ugr.siar.gui.menu;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.List;

import es.ugr.siar.db.*;
import es.ugr.siar.gui.*;
import es.ugr.siar.ip.*;
import es.ugr.siar.ip.desc.*;
import javax.swing.*;

public class MenuMySQL {

	static final String [] subMenuTxtTab = {
		"SQL compare",			//1
		"SQL VS Image desc",	//2
	};

	public static void fillJMenu(JMenu menu) {
		menu.removeAll();
		for(int i=0;i<subMenuTxtTab.length;i++) {
			menu.add(builJMenuItem(i));
		}
	}

	private static JMenuItem builJMenuItem(final int funcType) {
		JMenuItem item = new JMenuItem(subMenuTxtTab[funcType]);
		item.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(ActionEvent e) {
				switch(funcType) {
				case 0:
					MenuMySQL.sqlScore();			break;
				case 1:
					MenuMySQL.compareSQLImage();	break;
				}

			}

		});
		return item;
	}

	//------------------------------------------

	/**
	 * 
	 */
	protected static void compareSQLImage() {
		ImageJMR im = null;
		int photoID=200006;
		DatabaseManager dbManager = new APNDBManager();
		MySQL db = dbManager.getDataBase();	

		//Obtain the fileName of the image for all_photographers_now
		String sql ="SELECT Id 	FROM Photo, ExifData "+
		"WHERE ExifData.hasExif = '1' "+
		"AND NOT Photo.Keywords = 'NULL' "+
		"AND ExifData.Photo_ID = Photo.Id "+
		" ORDER BY `Photo`.`Id` ASC "; 

		db.queryOneRowResult(sql);
		String fName = db.getValueS("FilePath");
		db.closeResultSet();

		//Load the Image
		File imFile = new File(my.Config.dbDefaultPath+fName);
		im = ImageJMR.loadFromFile(imFile);

		//Iterate other the descriptors.
		boolean[] vDescImpl = VisualDescriptor.IMPLEMENTED_DESC;
		for(int dType=0;dType<vDescImpl.length;dType++) {
			if(vDescImpl[dType]) {
				VisualDescriptor vDescSQL = VisualDescriptor.getInstance(dType);
				VisualDescriptor vDescIm = VisualDescriptor.getInstance(dType);
				//If the vDescSQL exist compute vDescImg and compare them.
				if(vDescSQL.exist(photoID,db,true)) {
					vDescSQL.fromMySQL(photoID, db);
					vDescIm.extract(im);
					System.out.println("SQL\t= "+vDescSQL.toString());
					System.out.println("Im\t= "+vDescIm.toString());
					System.out.println("distance="+vDescSQL.compare(vDescIm)+"\n");

				} 
			}
		}


	}

	/**
	 * 
	 */
	protected static void sqlScore() {
		int descType = VisualDescriptor.DESC_MPEG7_CSD;
		DatabaseManager dbManager = new APNDBManager(); 
		List l = dbManager.compare(descType, 10005);
		JThumbsWindows thumbsWin = new JThumbsWindows();
		thumbsWin.setTitle("Result for "+VisualDescriptor.getSmallName(descType));
		thumbsWin.setThumbsImage(l);

	}

}
