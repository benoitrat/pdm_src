package es.ugr.siar.gui.menu;

import java.awt.event.ActionEvent;
import java.util.List;

import es.ugr.siar.db.APNDBManager;
import es.ugr.siar.db.DatabaseManager;
import es.ugr.siar.ip.colspace.*;
import es.ugr.siar.ip.desc.*;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.SetsFileTools.SetType;
import es.ugr.siar.ml.weka.CptExifCorrected;
import es.ugr.siar.ml.weka.CptsFullViewer;
import es.ugr.siar.ml.weka.WekaTools;
import es.ugr.siar.ml.weka.cptinst.CptInstFullProba;
import es.ugr.siar.tools.IndexedGeneric;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

public class MenuTest {

	static final String [] testArray = {
		"Hello word",		//0
		"Max Lab",			//1	
		"Max Luv",			//2
		"White Point",		//3
		"Debug Context",	//4
		"List Directory",	//5
		"QuantFunc",		//6
		"Check Train/Test Set", //7
		"Test Train XP",		//8

	};
	static JMenuItem test;


	public static void fillJMenu(JMenu menu) {
		menu.removeAll();
		for(int i=0;i<testArray.length;i++) {
			menu.add(builJMenuItem(i));
		}
	}

	private static JMenuItem builJMenuItem(final int funcType) {
		JMenuItem item = new JMenuItem(testArray[funcType]);
		item.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(ActionEvent e) {
				switch(funcType) {
				case 0:
					MenuTest.funcHelloWord();	break;
				case 1:
					MenuTest.maxCIELAB();		break;
				case 2:
					MenuTest.maxCIELUV();		break;
				case 3:
					MenuTest.whitePoint();		break;
				case 4:
					MenuTest.debugContext(); break;
				case 5:
					MenuTest.listDirectory(); 	break;
				case 6:
					MenuTest.quantCSD(); 		break;
				case 7:
					MenuTest.listTrainingSet(); 	break;
				case 8:
					MenuTest.trainTest(); 		break;
				}
			}

		});
		return item;
	}



	/**
	 * 
	 */
	protected static void trainTest() {
		CptInstFullProba cptInst = new CptInstFullProba("Beach",SetsFileTools.readGlobalClassNames(),WekaTools.getFullDescTypes(),SetType.TEST);
		cptInst.loadInstances();
		Instance inst = cptInst.getInstances().firstInstance();
		for(int i=0;i <inst.numAttributes(); i++) {
			String name = inst.attribute(i).name();
			if(name.length() >= 13)
				name="Pr["+name.substring(0,3)+","+name.substring(10, 13)+"]";
			String val= new my.PrintfFormat("%0.2f").sprintf(inst.value(i));
			val = val.replace(',', '.');
			System.out.print(name+"="+val+"; ");
		}
		
	}

	/**
	 * 
	 */
	protected static void debugContext() {
		String[] cNames = {"Beach","Sunset","Macro"};
		DatabaseManager dbMan = new APNDBManager();
		
		for(String cName : cNames) {
			int [] photoIDs = SetsFileTools.readPhotosID(cName,SetType.TEST);
			for(int photoID : photoIDs) {
				String tmp = dbMan.ImgDbPath(photoID);
				System.out.println("cp "+tmp+" .");
			}
		}
		
//		ExifDescriptor ed= new ExifDescriptor();
//		DatabaseManager dbMan = new APNDBManager();
//		ed.fromMySQL(129, dbMan.getDataBase());
	}

	/**
	 * 
	 */
	protected static void listTrainingSet() {
		String cName="Sunset";
		int[] s1 = SetsFileTools.readPhotosID(cName,SetsFileTools.SetType.TRAIN);
		int[] s2 = SetsFileTools.readOthersPhotoID(cName, SetsFileTools.SetType.TEST);
		SetsFileTools.printIntersection(s1, s2);
	}

	/**
	 * 
	 */
	protected static void quantCSD() {
		double [] x ={ 0.0010,    0.0013,    0.0016,    0.0020 ,   0.0025 ,   0.0032  ,  0.0040 , 
				0.0050  ,  0.0063,    0.0079,    0.0100,    0.0126,    0.0158,    0.0200,    0.0251,
				0.0316  ,  0.0398,    0.0501,    0.0631, 0.0794 , 0.1000 ,  0.1259,  0.1585, 0.1995, 
				0.2512,  0.3162,  0.32, 0.3981, 0.5012,  0.6310, 0.7943, 1.0000};
		my.Debug.printTab(x);
		int  [] y = new int[x.length];
		for(int i=0;i<x.length;i++) {
			y[i] = MPEG7ColorStructure.quantFunc(x[i]);
		}
		my.Debug.printTab(y);

	}


	public static void listDirectory() {
		//SerializeDescTools.listDirectory(new File(my.Config.openDir));
	}


	protected static void whitePoint() {
		WhitePoint wP;
		wP = WhitePoint.getInstance(WhitePoint.WP_TYPE_D65,false);
		my.Debug.print(wP.toString());
		wP = WhitePoint.getInstance(WhitePoint.WP_TYPE_D65,true);
		my.Debug.print(wP.toString());
		wP = WhitePoint.getInstance(WhitePoint.WP_TYPE_D50,false);
		my.Debug.print(wP.toString());
		wP = WhitePoint.getInstance(WhitePoint.WP_TYPE_D50,true);
		my.Debug.print(wP.toString());
	}

	protected static void maxCIELAB() {
		ColorSpaceLab cS = new ColorSpaceLab(new WhitePoint(1f,1f,1f));
		float[] test;
		test = cS.fromCIEXYZ(new float [] {1,1,0});
		my.Debug.printTab(test);
		test = cS.fromCIEXYZ(new float [] {1,0,0});
		my.Debug.printTab(test);
		test = cS.fromCIEXYZ(new float [] {0,1,1});
		my.Debug.printTab(test);
		test = cS.fromCIEXYZ(new float [] {0,0,1});
		my.Debug.printTab(test);

	}

	protected static void maxCIELUV() {
		ColorSpaceLuv cS = new ColorSpaceLuv(new WhitePoint(1f,1f,1f));
		float[] test;
		for(int i=0;i<2;i++) 
			for(int j=0;j<2;j++) 
				for(int k=0;k<2;k++) {
					test = cS.fromCIEXYZ(new float [] {i,j,k});
					my.Debug.printTab(test);
				}

	}

	protected static void funcHelloWord() {
		System.out.println("Hello World");
	}


}
