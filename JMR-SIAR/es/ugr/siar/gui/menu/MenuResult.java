package es.ugr.siar.gui.menu;

import java.awt.event.ActionEvent;
import javax.swing.*;

import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.SetsFileTools.SetType;
import es.ugr.siar.ml.weka.CptsConfusionMatrix;
import es.ugr.siar.ml.weka.CptsBiasCurve;
import es.ugr.siar.ml.weka.CptsROCCurve;
import es.ugr.siar.ml.weka.StatsStructure;
import es.ugr.siar.ml.weka.WekaTools;
import es.ugr.siar.ml.weka.cptinst.CptInstFullProba;
import es.ugr.siar.ml.weka.cptinst.CptInstGlobal.CptInstGlobalType;
import es.ugr.siar.ml.weka.trainerxp.CptsTrainerXPMultiDesc;
import es.ugr.siar.ml.weka.trainerxp.CptsTrainerXp;
import es.ugr.siar.ml.weka.trainerxp.CptsTrainerXpFullProba;
import es.ugr.siar.ml.weka.trainerxp.CptsTrainerXpIndDesc;
import es.ugr.siar.ml.weka.trainerxp.CptsTrainerXpMixedDist;
import es.ugr.siar.ml.weka.trainerxp.CptsTrainerXpMixedProba;

public class MenuResult {

	static final String [][] subMenuTxtTab = {{
		"Combined Desc",			//0
		"1-Distance CombDesc",		//1
		"M-Distance CombDesc",		//2
		"Indep. SVM Desc", 			//3
		"Indep. BayNet Desc", 		//4
		"Mixed 1-Proba NvBay",		//5
		"Mixed M-Proba NvBay",		//6
		"Full 1-Proba NvBay",		//7
		"Full M-Proba NvBay",		//8
	},{
		"Confusion Matrix",			//0
		"ROC Curves",				//1
		"Best Attributes",			//2
		"Resample Stats",			//3
	}};

	public static void fillJMenu(JMenu menu) {
		menu.removeAll();
		for(int i=0;i<subMenuTxtTab.length;i++) {
			for(int j=0;j<subMenuTxtTab[i].length;j++)
				menu.add(builJMenuItem(i,j));
			if(subMenuTxtTab.length > 1 && i < subMenuTxtTab.length-1) {
				menu.addSeparator();
			}
		}
	}

	private static JMenuItem builJMenuItem(final int i, final int funcType) {
		JMenuItem item = new JMenuItem(subMenuTxtTab[i][funcType]);
		item.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if(i==0) {
					switch(funcType) {
					case 0:
						MenuResult.combinedSimpleDesc();	break;
					case 1:
						MenuResult.oneDistCombDesc(); 		break;
					case 2:
						MenuResult.multDistCombDesc(); 		break;
					case 3:
						MenuResult.indDescSVM(); 			break;
					case 4:
						MenuResult.indDescBayesNet();		break;
					case 5:
						MenuResult.oneMixedProbaNBay();		break;
					case 6:
						MenuResult.multiMixedProbaNBay();	break;
					case 7:
						MenuResult.oneFullProbaNBay();		break;
					case 8:
						MenuResult.multiFullProbaNBay();	break;
					}
				}
				if(i==1) {
					switch(funcType) {
					case 0:
						MenuResult.confusionMx();		break;
					case 1:
						MenuResult.curvesROC();			break;
					case 2:
						MenuResult.bestAttributes();	break;
					case 3:
						MenuResult.resampleStats();		break;			
					}
				}
			}

		});
		return item;
	}









	/**
	 * Combined direct output of the different classifier with naive Bayes.
	 * Print result in a row format.
	 * Version with and without EXIF using {@link WekaTools#getDescMx()}.
	 */
	protected static void combinedSimpleDesc() {
		DescriptorType[][] descMx = WekaTools.getDescMx();
		CptsTrainerXPMultiDesc multiDescXp;
		StatsStructure[][] sStatMx = new StatsStructure[1][];
		String[] xpString = new String[1];

//		multiDescXp = new CptsTrainerXPMultiDesc(new DescriptorType[]{DescriptorType.C_EXF});
//		multiDescXp.setInstOnlyFF(false);
//		multiDescXp.setClsOnlyFF(false);
//		multiDescXp.setResampleParam(0.20f, 100f);
//		sStatMx[0] = multiDescXp.getStatsList();
//		xpString[0]= "\\Aset_\\acro{EXF}$";
//
//		multiDescXp = new CptsTrainerXPMultiDesc(descMx[1]);
//		multiDescXp.setInstOnlyFF(false);
//		multiDescXp.setClsOnlyFF(false);
//		sStatMx[1] = multiDescXp.getStatsList();
//		xpString[1]= "$\\Aset_\\acro{CSD},\\Aset_\\acro{SCD},\\Aset_\\acro{EHD}$";
//
//		multiDescXp = new CptsTrainerXPMultiDesc(descMx[0]);
//		multiDescXp.setInstOnlyFF(false);
//		multiDescXp.setClsOnlyFF(false);
//		multiDescXp.setResampleParam(0.33f, 100f);
//		sStatMx[2] = multiDescXp.getStatsList();
//		xpString[2]= "$\\Aset_\\acro{CSD},\\Aset_\\acro{SCD},\\Aset_\\acro{EHD},\\Aset_\\acro{EXF}$";;

		
		multiDescXp = new CptsTrainerXPMultiDesc(descMx[0]);
		multiDescXp.setInstOnlyFF(false);
		multiDescXp.setClsOnlyFF(false);
		multiDescXp.setClsOverwrite(true);
		multiDescXp.setResampleParam(0.33f, 100f);
		sStatMx[0] = multiDescXp.getStatsList();
		xpString[0]= "$\\Aset_\\acro{CSD},\\Aset_\\acro{SCD},\\Aset_\\acro{EHD},\\Aset_\\acro{EXF}$";

		CptsTrainerXp.printResultTexCol(sStatMx, xpString);
	}




	/**
	 * 
	 */
	protected static void oneDistCombDesc() {

		String[] cptNames=SetsFileTools.readGlobalClassNames();
		DescriptorType[][] dTypesMx = WekaTools.getDescMx();
		StatsStructure[][] sStatMx = new StatsStructure[2][];
		String[] xpString = new String[2];
		CptsTrainerXpMixedDist cptsXp;

		cptsXp = new CptsTrainerXpMixedDist(cptNames,null,dTypesMx[0]);
		cptsXp.makeInstances();
		sStatMx[1] = cptsXp.getStatsList();
		xpString[1] = CptsTrainerXp.getXpString(dTypesMx[1], "Sim", true);
		xpString[1] += ", \\acro{EXF} outputs";
		
		
		cptsXp = new CptsTrainerXpMixedDist(cptNames,null,dTypesMx[1]);
		cptsXp.setInstOnlyFF(true);
		cptsXp.setClsOnlyFF(false);
		sStatMx[0] = cptsXp.getStatsList();
		xpString[0]=CptsTrainerXp.getXpString(dTypesMx[1], "Sim", true);
		
		CptsTrainerXp.printResultTexRow(sStatMx, dTypesMx);
	}

	/**
	 * 
	 */
	protected static void multDistCombDesc() {
		DescriptorType[][] dTypesMx = WekaTools.getDescMx();
		StatsStructure[][] sStatMx = new StatsStructure[2][];
		String[] xpString = new String[2];
		CptsTrainerXp ctXp;


		ctXp = new CptsTrainerXpMixedDist(dTypesMx[0]);
		sStatMx[1] = ctXp.getStatsList();
		xpString[1] = CptsTrainerXp.getXpString(dTypesMx[1], "Sim", true);
		xpString[1] += ", \\acro{EXF} outputs";

		ctXp = new CptsTrainerXpMixedDist(dTypesMx[1]);
		ctXp.setInstOnlyFF(true);
		ctXp.setClsOnlyFF(false);
		sStatMx[0] = ctXp.getStatsList();
		xpString[0]=CptsTrainerXp.getXpString(dTypesMx[1], "Sim", true);

		CptsTrainerXp.printResultTexCol(sStatMx, xpString);
	}


	/**
	 * 
	 */
	protected static void indDescSVM() {
		CptsTrainerXpIndDesc indDescXP = new CptsTrainerXpIndDesc();
		StatsStructure[][] statsMx =indDescXP.getStatsMx();
		CptsTrainerXp.printResultTexRow(statsMx, WekaTools.getVisualDescTypes());
		//indDescXP.printResultInLatex();
	}

	/**
	 * 
	 */
	protected static void indDescBayesNet() {
		String[] cNames = SetsFileTools.readGlobalClassNames();
		DescriptorType[][] descMx = new DescriptorType[][]{{
				DescriptorType.C_EXF}};
		
		CptsTrainerXpIndDesc cidTXP = new CptsTrainerXpIndDesc(cNames,cNames,descMx[0]);
		StatsStructure[][] statsMx =cidTXP.getStatsMx();
		CptsTrainerXp.printResultTexRow(statsMx, descMx);
		
		
		//cidTXP.printResultInLatex();	
	}

	/**
	 * 
	 */
	protected static void oneMixedProbaNBay() {
		// TODO Auto-generated method stub

	}

	/**
	 * 
	 */
	protected static void multiMixedProbaNBay() {

		DescriptorType[][] dTypesMx = WekaTools.getDescMx();
		String[] cNames = SetsFileTools.readGlobalClassNames();
		String[] xpString = new String[2];
		StatsStructure[][] sStatMx = new StatsStructure[2][];
		CptsTrainerXp ctXp;

		ctXp = new CptsTrainerXpMixedProba(cNames,cNames,dTypesMx[0]);
		ctXp.setInstOnlyFF(true);
		ctXp.setClsOnlyFF(false);
		ctXp.setResample(false);
		sStatMx[0] = ctXp.getStatsList();
		xpString[0] = CptsTrainerXp.getXpString(dTypesMx[1], "SVM", true);
		xpString[0] += ", \\acro{EXF} ";


		ctXp = new CptsTrainerXpMixedProba(cNames,cNames,dTypesMx[1]);
		ctXp.setInstOnlyFF(false);
		ctXp.setClsOnlyFF(false);
		ctXp.setResample(false);
		sStatMx[1] = ctXp.getStatsList();
		xpString[1] = CptsTrainerXp.getXpString(dTypesMx[1], "SVM", true);

		CptsTrainerXp.printResultTexCol(sStatMx, xpString);


	}

	/**
	 * 
	 */
	protected static void oneFullProbaNBay() {
		DescriptorType[][] dTypesMx = WekaTools.getDescMx();
		String[] cNames = SetsFileTools.readGlobalClassNames();
		String[] xpString = new String[2];
		StatsStructure[][] sStatMx = new StatsStructure[2][];
		CptsTrainerXp ctXp;

		ctXp = new CptsTrainerXpFullProba(cNames,null,dTypesMx[0]);
		ctXp.setInstOnlyFF(true);
		ctXp.setClsOverwrite(true);
		ctXp.setResampleParam(0.6f, 100f);
		ctXp.setClsOnlyFF(false);
		sStatMx[1] = ctXp.getStatsList();
		xpString[1] = CptsTrainerXp.getXpString(dTypesMx[1], "SVM", false);
		xpString[1] += ","+CptsTrainerXp.getXpString(new DescriptorType[]{DescriptorType.C_EXF}, "BayN", true);


		ctXp = new CptsTrainerXpFullProba(cNames,null,dTypesMx[1]);
		ctXp.setInstOnlyFF(false);
		ctXp.setClsOverwrite(true);
		ctXp.setResampleParam(0.5f, 100f);
		ctXp.setClsOnlyFF(false);
		sStatMx[0] = ctXp.getStatsList();
		xpString[0] = CptsTrainerXp.getXpString(dTypesMx[1], "SVM", false);

		CptsTrainerXp.printResultTexCol(sStatMx, xpString);
		
	}

	/**
	 * 
	 */
	protected static void multiFullProbaNBay() {
		DescriptorType[][] dTypesMx = WekaTools.getDescMx();
		String[] cNames = SetsFileTools.readGlobalClassNames();
		String[] xpString = new String[2];
		StatsStructure[][] sStatMx = new StatsStructure[2][];
		CptsTrainerXp ctXp;

		ctXp = new CptsTrainerXpFullProba(cNames,cNames,dTypesMx[0]);

		ctXp.setInstOnlyFF(true);
		ctXp.setClsOnlyFF(false);
		ctXp.setResample(false);
		sStatMx[1] = ctXp.getStatsList();
		xpString[1] = CptsTrainerXp.getXpString(dTypesMx[1], "SVM", true);
		xpString[1] += ","+CptsTrainerXp.getXpString(new DescriptorType[]{DescriptorType.C_EXF}, "BayN", true);


		ctXp = new CptsTrainerXpFullProba(cNames,cNames,dTypesMx[1]);
		ctXp.setInstOnlyFF(false);
		ctXp.setClsOnlyFF(false);
		ctXp.setResample(false);
		sStatMx[0] = ctXp.getStatsList();
		xpString[0] = CptsTrainerXp.getXpString(dTypesMx[1], "SVM", true);

		CptsTrainerXp.printResultTexCol(sStatMx, xpString);
	}

	
	//---------------------------------------------------------------------------
	
	protected static void confusionMx() {
		String cNames[]= SetsFileTools.readGlobalClassNames();
		CptsConfusionMatrix confMxXp = new CptsConfusionMatrix(cNames);
		confMxXp.printConfusionMx();
		
	}

	protected static void curvesROC() {
		String[] cNames = SetsFileTools.readGlobalClassNames();
		CptsROCCurve cROC = new CptsROCCurve(cNames,CptInstGlobalType.FullProba);
		cROC.startROC();
	}
	

	/**
	 * 
	 */
	protected static void bestAttributes() {
		CptsTrainerXpMixedProba probaXp =new CptsTrainerXpMixedProba();
		probaXp.best10AttrInLatex();

	}
	
	/**
	 * 
	 */
	protected static void resampleStats() {
		String[] cNames = SetsFileTools.readGlobalClassNames();
		DescriptorType[] dTypes = WekaTools.getFullDescTypes();
		
		
		CptInstFullProba cptInstTrain = new CptInstFullProba("Beach",cNames,dTypes,SetType.TRAIN);
		CptInstFullProba cptInstTest = new CptInstFullProba("Beach",cNames,dTypes,SetType.TEST);
		
		CptsBiasCurve cBias= new CptsBiasCurve(cptInstTrain,cptInstTest);
		cBias.start();
		
	}
}
