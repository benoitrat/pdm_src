package es.ugr.siar.gui.menu;

import java.awt.event.ActionEvent;
import javax.swing.*;

import es.ugr.siar.ip.desc.DescriptorValues.DescriptorType;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.weka.ParamSearchSVM;
import es.ugr.siar.ml.weka.WekaTools;
import es.ugr.siar.ml.weka.cptinst.CptInstDist;
import es.ugr.siar.ml.weka.trainerxp.CptsTrainerXPMultiDesc;
import es.ugr.siar.ml.weka.trainerxp.CptsTrainerXpFullProba;
import es.ugr.siar.ml.weka.trainerxp.CptsTrainerXpGlobal;
import es.ugr.siar.ml.weka.trainerxp.CptsTrainerXpIndDesc;
import es.ugr.siar.ml.weka.trainerxp.CptsTrainerXpMixedProba;

public class MenuTrain {


	static final String [][] subMenuTxtTab = {{
		"DirectOutput Desc",	//0
		"Similarities Inst",	//1
	},{
		"Make IndDesc Inst", 		//0
		"CrossValidation SVM",		//1
		"Train IndDesc Cls", 		//2
		"ExifDesc Inst + Class",	//3
	},{
		"Make MultiProba Inst", 	//0
		"Train MultiProba Cls", 	//1
		"Make FullProba Inst",		//2
	}};

	public static void fillJMenu(JMenu menu) {
		menu.removeAll();
		for(int i=0;i<subMenuTxtTab.length;i++) {
			for(int j=0;j<subMenuTxtTab[i].length;j++)
				menu.add(builJMenuItem(i,j));
			if(subMenuTxtTab.length > 1 && i < subMenuTxtTab.length-1) {
				menu.addSeparator();
			}
		}
	}

	private static JMenuItem builJMenuItem(final int i, final int funcType) {
		JMenuItem item = new JMenuItem(subMenuTxtTab[i][funcType]);
		item.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(ActionEvent e) {
				//Make and train simple combination 
				if(i==0) {
					switch(funcType) {
					case 0:
						MenuTrain.makeDirectOutputDesc();	break;
					case 1:
						MenuTrain.makeDistInst();			break;
					}
				}
				//Training independant descriptor
				else if(i==1) {
					switch(funcType){
					case 0:
						MenuTrain.makeIndDescInst(); 		break;
					case 1:
						MenuTrain.trainGridSearchSVM(); 	break;
					case 2:
						MenuTrain.trainIndDescCls(); 		break;
					case 3:
						MenuTrain.exifMakeAndTrain();		break;
					}
				}
				//Make and train complex combination
				else if(i==2) {
					switch(funcType){
					case 0:
						MenuTrain.makeMultiProbaInst(); 	break;
					case 1:
						MenuTrain.trainMultiProbaCls(); 	break;
					case 2:
						MenuTrain.makeFullProbaInst(); 		break;
					}
				}
			}

		});
		return item;
	}

	//---------------------------------------------------------------------------

	/**
	 * 
	 */
	protected static void makeDirectOutputDesc() {
		CptsTrainerXPMultiDesc multiDescXp;
		DescriptorType[][] descMx = WekaTools.getDescMx();
		multiDescXp = new CptsTrainerXPMultiDesc(new DescriptorType[]{DescriptorType.C_EXF});
		multiDescXp.makeInstances();
		multiDescXp = new CptsTrainerXPMultiDesc(descMx[0]);
		multiDescXp.makeInstances();
		multiDescXp = new CptsTrainerXPMultiDesc(descMx[1]);
		multiDescXp.makeInstances();
	}

	/**
	 * 
	 */
	protected static void makeDistInst() {
		DescriptorType[][] descTypeMx = WekaTools.getDescMx();
		String[] cNames = SetsFileTools.readGlobalClassNames();
		CptInstDist ciDist;
		for(String cName : cNames) {
			//Only 1 distance
			ciDist = new CptInstDist(cName,new String[]{cName},descTypeMx[0],SetsFileTools.SetType.TRAIN);
			ciDist.loadInstances();
			ciDist.keepOnlyDesc(descTypeMx[1]);
			ciDist.saveInstancesInArff();
			ciDist = new CptInstDist(cName,new String[]{cName},descTypeMx[0],SetsFileTools.SetType.TEST);
			ciDist.loadInstances();
			ciDist.keepOnlyDesc(descTypeMx[1]);
			ciDist.saveInstancesInArff();
			//Multiple distance
			ciDist = new CptInstDist(cName,cNames,descTypeMx[0],SetsFileTools.SetType.TRAIN);
			ciDist.loadInstances();
			ciDist.keepOnlyDesc(descTypeMx[1]);
			ciDist.saveInstancesInArff();
			ciDist = new CptInstDist(cName,cNames,descTypeMx[0],SetsFileTools.SetType.TEST);
			ciDist.loadInstances();
			ciDist.keepOnlyDesc(descTypeMx[1]);
			ciDist.saveInstancesInArff();
		}

	}
	
	
	//---------------------------------------------------------------------------
	
	/**
	 * 
	 */
	protected static void trainGridSearchSVM() {
		//String[] cptNames = SetsFileTools.readGlobalClassNames();
		DescriptorType[] descTypes = WekaTools.getDescMx()[1];
		String[] cptNames = new String[]{"Flower","Sunset","Night","Portrait","Beach","Snow","Sky","Tree","Landscape"};
		for(String cptName : cptNames) {
			ParamSearchSVM gridSearch= new ParamSearchSVM(cptName,descTypes);
			gridSearch.setGridParamC(-3, 5,2);
			gridSearch.setGridParamGamma(-5,3, 2);
			gridSearch.start();
		}

	}
	
	/**
	 * 
	 */
	protected static void makeIndDescInst() {
		//make Instance for MPEG7 desc
		CptsTrainerXpIndDesc indDescXP = new CptsTrainerXpIndDesc();
		indDescXP.makeInstances();
		
		//Male instances for EXF desc
		String[] cNames = SetsFileTools.readGlobalClassNames();
		CptsTrainerXpIndDesc cidTXP = new CptsTrainerXpIndDesc(cNames,cNames,new DescriptorType[]{
				DescriptorType.C_EXF});
		cidTXP.makeInstances();

	}
	
	/**
	 * 
	 */
	protected static void exifMakeAndTrain() {
		String[] cNames = SetsFileTools.readGlobalClassNames();
		CptsTrainerXpIndDesc cidTXP = new CptsTrainerXpIndDesc(cNames,cNames,new DescriptorType[]{
				DescriptorType.C_EXF});
		cidTXP.makeClassifiers();
	}
	

	/**
	 * 
	 */
	protected static void trainIndDescCls() {
		CptsTrainerXpIndDesc indDescXP = new CptsTrainerXpIndDesc();
		int r = JOptionPane.showConfirmDialog(null,"Overwrite classifiers");
		indDescXP.setClsOverwrite(r==0);
		indDescXP.setClsOnlyFF(true);
		indDescXP.makeClassifiers();
	}

//	---------------------------------------------------------------------



	/**
	 * 
	 */
	protected static void makeFullProbaInst() {

		DescriptorType[][] dTypesMx = WekaTools.getDescMx();
		String[] cNames = SetsFileTools.readGlobalClassNames();

		//M concept previously learned
		CptsTrainerXpFullProba ctXp = new CptsTrainerXpFullProba(cNames,cNames,dTypesMx[0]);
		ctXp.makeInstances();
		ctXp.makeFilteredInstances(dTypesMx[1]);

		//1 concept previously learned
		ctXp = new CptsTrainerXpFullProba(cNames,null,dTypesMx[0]);
		ctXp.makeInstances();
		ctXp.makeFilteredInstances(dTypesMx[1]);
	}



//	---------------------------------------------------------------------




	/**
	 * 
	 */
	protected static void makeMultiProbaInst() {
		CptsTrainerXpGlobal ctXp;
		DescriptorType[][] dTypesMx = WekaTools.getDescMx();
		String[] cNames = SetsFileTools.readGlobalClassNames();
		
		//M concept learned.
		ctXp = new CptsTrainerXpMixedProba(cNames,cNames,dTypesMx[0]);
		ctXp.makeInstances();
		ctXp.makeFilteredInstances(dTypesMx[1]);
		
		//1 concept previously learned
		ctXp = new CptsTrainerXpFullProba(cNames,null,dTypesMx[0]);
		ctXp.makeInstances();
		ctXp.makeFilteredInstances(dTypesMx[1]);
	}

	/**
	 * 
	 */
	protected static void trainMultiProbaCls() {
		CptsTrainerXpGlobal ctXp;
		DescriptorType[][] dTypesMx = WekaTools.getDescMx();
		String[] cNames = SetsFileTools.readGlobalClassNames();
		
		//M concept learned.
		ctXp = new CptsTrainerXpMixedProba(cNames,cNames,dTypesMx[0]);
		ctXp.makeClassifiers();
		ctXp.makeFilteredInstances(dTypesMx[1]);
		
		//1 concept previously learned
//		ctXp = new CptsTrainerXpFullProba(cNames,null,dTypesMx[0]);
//		ctXp.makeInstances();
//		ctXp.makeFilteredInstances(dTypesMx[1]);
	}
}
