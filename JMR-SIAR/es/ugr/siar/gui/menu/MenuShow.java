package es.ugr.siar.gui.menu;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import es.ugr.siar.db.*;
import es.ugr.siar.gui.*;
import es.ugr.siar.gui.actionwin.WinShowClassDist;
import es.ugr.siar.gui.actionwin.WinShowClassPrediction;
import es.ugr.siar.ip.desc.VisualDescriptor;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.SetsFileTools.SetType;
import es.ugr.siar.ml.weka.CptExifCorrected;
import es.ugr.siar.ml.weka.CptsConfusionMatrix;
import es.ugr.siar.ml.weka.CptsFullViewer;
import es.ugr.siar.ml.weka.cptinst.CptInstGlobal.CptInstGlobalType;
import es.ugr.siar.tools.*;

import javax.swing.*;

public class MenuShow {

	static final String [] subMenuTxtTab = {
		"Exif Tags", 				//0
		"Keywords Count",			//1	
		"Keywords Correlation",		//2
		"Photos from 1 Class",		//3
		"PhotoDist. from 1 Class",	//4
		"Photo Prediction",			//5
		"Improved by Exif",			//6

	};

	public static void fillJMenu(JMenu menu) {
		menu.removeAll();
		for(int i=0;i<subMenuTxtTab.length;i++) {
			menu.add(builJMenuItem(i));
		}
	}

	private static JMenuItem builJMenuItem(final int funcType) {
		JMenuItem item = new JMenuItem(subMenuTxtTab[funcType]);
		item.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(ActionEvent e) {
				switch(funcType) {
				case 0:
					MenuShow.showExif(); 	break;
				case 1:
					MenuShow.showKeywords(); 	break;
				case 2:
					MenuShow.keywordsCorr(); 	break;
				case 3:
					MenuShow.showPhotoClass(); 	break;
				case 4:
					MenuShow.showDistOneClass(); break;
				case 5:
					MenuShow.showPrediction(); break;
				case 6:
					MenuShow.improveExif(); break;

				}

			}

		});
		return item;
	}

	/**
	 * 
	 */
	protected static void improveExif() {
		String cptNames[] = SetsFileTools.readGlobalClassNames();
		String cptName = (String)JOptionPane.showInputDialog(null,
				"Select a class type:\n","Question ?",JOptionPane.PLAIN_MESSAGE,
				null,cptNames,cptNames[3]);
		if(cptName == null ) return;
		
		CptExifCorrected tmp = new CptExifCorrected(); 
		tmp.visualizeImproved(cptName);
		
	}

	/**
	 * 
	 */
	protected static void showPrediction() {
		String[] cNameSelected = SetsFileTools.readGlobalClassNames();

		WinShowClassPrediction actionWin = new WinShowClassPrediction("Class Prediction",new Dimension(200,350));
		actionWin.setBorder(true);
		ButtonGroup gClass = new ButtonGroup();
		for(String className : cNameSelected) {
			JRadioButton jrb = new JRadioButton(className);
			actionWin.addCenter(jrb,0);
			gClass.add(jrb);
		}

		CptsFullViewer.ViewType[] vTypes = CptsFullViewer.ViewType.values();
		ButtonGroup gViewType = new ButtonGroup();
		for(int i=0;i<vTypes.length;i++) {
			JRadioButton jrb = new JRadioButton(vTypes[i].toString());
			jrb.setActionCommand(""+i);
			if(i==0) jrb.setSelected(true);
			actionWin.addCenter(jrb,1);
			gViewType.add(jrb);
		}		

		CptInstGlobalType[] gTypes = CptInstGlobalType.values();
		ButtonGroup gGlobalType = new ButtonGroup();
		for(int i=0;i<vTypes.length;i++) {
			JRadioButton jrb = new JRadioButton(gTypes[i].toString());
			jrb.setActionCommand(""+i);
			actionWin.addCenter(jrb,3);
			gGlobalType.add(jrb);
		}	

		JCheckBox jcbConjugate =  new JCheckBox("Conjugate Set");
		actionWin.addCenter(jcbConjugate,2);




	}

	/**
	 * 
	 */
	protected static void keywordsCorr() {
		String[] cNames = SetsFileTools.readGlobalClassNames();
		List<HashSet<Integer>> cSets = new ArrayList<HashSet<Integer>>();
		//Put training and testing in the same set
		for(int i=0;i<cNames.length;i++) {
			//Train
			int[] train = SetsFileTools.readPhotosID(cNames[i], SetType.TRAIN);
			//Test
			int[] test = SetsFileTools.readPhotosID(cNames[i], SetType.TEST);

			int[] tmp = ArraysTools.arrayMerge(train,test);
			cSets.add((HashSet<Integer>)ArraysTools.toSet(tmp));
		}		

		double[][] corMx = new double[cNames.length][cNames.length];
		HashSet<Integer> tmpSet;
		double total=0,tmp=0;
		for(int i=0;i<cNames.length;i++) {
			total = cSets.get(i).size();
			for(int j=0;j<cNames.length;j++) {

				if(i==j) tmp=1.0;
				else {
					tmpSet = (HashSet<Integer>)cSets.get(i).clone();
					tmpSet.retainAll(cSets.get(j));
					tmp=tmpSet.size()/total;
				}
				corMx[i][j]=tmp;

			}
		}

		CptsConfusionMatrix.scoreMxToLatex(cNames, corMx);



	}


	/**
	 * 
	 */
	protected static void showDistOneClass() {

		//Obtain the classes Name and SQL
		DatabaseManager dbManager = new APNDBManager();
		List<IndexedGeneric<String>> classList = dbManager.getClassesSyntax();
		String[] cNameSelected = SetsFileTools.readGlobalClassNames();
		if(cNameSelected !=  null) Arrays.sort(cNameSelected);

		WinShowClassDist actionWin = new WinShowClassDist("MakeModel",new Dimension(200,350));
		actionWin.setBorder(true);
		ButtonGroup gClass = new ButtonGroup();
		for(IndexedGeneric<String> igS: classList) {
			JRadioButton jrb = new JRadioButton(igS.toString());
			jrb.setActionCommand(""+igS.getId());
			actionWin.addCenter(jrb,0);
			gClass.add(jrb);
			if(cNameSelected != null) {
				if(Arrays.binarySearch(cNameSelected,igS.toString()) >= 0) 
					jrb.setSelected(true);
				else
					jrb.setEnabled(false);
			}
		}
		ButtonGroup gVdesc = new ButtonGroup();
		boolean[] implvDesc = VisualDescriptor.IMPLEMENTED_DESC;
		for(int i=0;i<implvDesc.length;i++) {
			if(implvDesc[i]) {
				JRadioButton jrb = new JRadioButton(VisualDescriptor.getSmallName(i));
				jrb.setActionCommand(""+i);
				actionWin.addCenter(jrb,1);
				gVdesc.add(jrb);
				jrb.setSelected(true);
			}
		}
		JCheckBox jcbTrain =  new JCheckBox("Training Set");
		jcbTrain.setActionCommand(SetsFileTools.SetType.TRAIN.getExt());
		JCheckBox jcbTest =  new JCheckBox("Testing Set");
		jcbTest.setActionCommand(SetsFileTools.SetType.TEST.getExt());

		jcbTrain.setSelected(true);
		actionWin.addCenter(jcbTrain,2);
		actionWin.addCenter(jcbTest,2);

		JCheckBox jcbOtherTrain =  new JCheckBox("Other Training Set");
		jcbOtherTrain.setActionCommand(SetsFileTools.SetType.TRAIN.getExt());
		JCheckBox jcbOtherTest =  new JCheckBox("Other Testing Set");
		jcbOtherTest.setActionCommand(SetsFileTools.SetType.TEST.getExt());
		actionWin.addCenter(jcbOtherTrain,3);
		actionWin.addCenter(jcbOtherTest,3);
		
		actionWin.addCenter(new JLabel("Subsample length"),4);
		actionWin.addCenter(new JTextField("Full set"),4);

		//actionWin.pack();
	}

	//------------------------------------------

	/**
	 * 
	 */
	protected static void showPhotoClass() {
		DatabaseManager dbManager = new APNDBManager();
		List<IndexedGeneric<String>> classNames = dbManager.getClassesSyntax();

		IndexedGeneric indObj = (IndexedGeneric)JOptionPane.showInputDialog(null,
				"Select a class type:\n","Question ?",JOptionPane.PLAIN_MESSAGE,
				null,classNames.toArray(),classNames.get(0));
		if(indObj == null ) return;

		System.out.println("name="+indObj.toString()+" id="+indObj.getId());


		int[] photosID = dbManager.getOneClassPhotosID(""+indObj.getId());
		if(photosID != null) 
			new WinThumbsScroll("Result",photosID);
	}



	/**
	 * Simple Show Keywords Count
	 */
	protected static void showKeywords() {
		int limitStart=0;
		int limitEnd=100;
		DatabaseManager dbManager = new APNDBManager();
		MySQL db = dbManager.getDataBase();	

		String sql ="SELECT Keyword, count(*) \n"+
		" FROM Keywords "+
		" GROUP BY Keyword "+
		" ORDER BY `count(*)` DESC "+
		" LIMIT "+limitStart+","+limitEnd+";";
		db.queryResult(sql);

		int count=0;
		while(db.nextRow()) {
			System.out.println(count+"# :"+db.getValueS("Keyword")+" ->"+db.getValueInt("count(*)"));
			count++;
		}
		db.closeResultSet();
		db.close();

	}


	/**
	 * 
	 */
	protected static void showExif() {
		// TODO Nothing from a long time ago

	}

}
