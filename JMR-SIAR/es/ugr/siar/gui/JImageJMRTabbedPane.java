package es.ugr.siar.gui;

import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;

import javax.swing.JTabbedPane;

import es.ugr.siar.ip.ImageJMR;
import es.ugr.siar.ip.colspace.ColorSpaceJMR;


public class JImageJMRTabbedPane extends JTabbedPane {

	/**
	 * Serial for serializable class 
	 */
	private static final long serialVersionUID = 3168088414417846824L;

	private ImageJMR img = null;
	//private JImagePanel[] jIpArray = null;	
	
	/**
	 * Constructeur by default: 
	 * The panel will draw noimage text !bipolaire
	 */
	public JImageJMRTabbedPane() {
	}


	public JImageJMRTabbedPane(ImageJMR img) {
		this.img = img;
		initialize();
	}
		
	
	/**
	 * This method initializes this
				this.add(getJScrollPane(), null);  // Generated
	 */
	private void initialize() {
		this.setName("Image JMR Layered");
		JImagePanel jIP = null; 
		//ImageJMR test;
		if(img != null) {
			BufferedImage[] bImg = img.getLayeredByteImages();
			ColorSpace cS = img.getColorModel().getColorSpace();
			for(int i=0;i<bImg.length;i++) {
				//test = new ImageJMR(bImg[i]);
				//System.out.println(test.toString());			
				jIP = new JImagePanel(bImg[i]);
				this.addTab(ColorSpaceJMR.getName(cS,i),jIP);
				jIP.repaint();
			}
		}
		
	}
	/**
	 * Set & Get Function
	 * @param img
	 */
	public void setImage(ImageJMR img) {
		this.img = img;
	}
	public BufferedImage getImage() {
		return this.img;
	}
	
}  //  @jve:decl-index=0:visual-constraint="10,10"
