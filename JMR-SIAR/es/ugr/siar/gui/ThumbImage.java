/**
 * 
 */
package es.ugr.siar.gui;

import java.awt.Graphics;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 4 déc. 07
 *
 */
public class ThumbImage extends JComponent {

	/**     */
	private static final long serialVersionUID = 1L;
	private static final int startX=10;
	private static final int startY=10;
	
	private static int maxSize=128;
	private BufferedImage bg;
	private String txt = "";
    

	public ThumbImage(BufferedImage bg) {
		this.bg=rescale(bg);
	}
	/**
	 * 
	 */
	public ThumbImage(BufferedImage bg, String txt) {
		this.bg=rescale(bg);
		this.txt=txt;
	}
	
	private BufferedImage rescale(BufferedImage src) {
		//Find Maximum Size & Scaling Factio
		AffineTransform imTransfo = new AffineTransform();
		int imSize = (int)Math.max(src.getHeight(),src.getWidth());
		
		//Rotation & Scaling
		if(imSize > maxSize) {
			if(src.getHeight() > src.getWidth())  imTransfo.rotate(Math.toRadians(90));
			double scaleRatio = (double)maxSize/(double)imSize;
			imTransfo.setToScale(scaleRatio, scaleRatio);
			AffineTransformOp imResizeOp = new AffineTransformOp(imTransfo,AffineTransformOp.TYPE_BILINEAR );
			return imResizeOp.filter(src,this.bg);
		}
		
		//Only Rotation
		if(src.getHeight() > src.getWidth())  {
			imTransfo.rotate(Math.toRadians(90));
			AffineTransformOp imRotateOp = new AffineTransformOp(imTransfo,AffineTransformOp.TYPE_BILINEAR );
			this.bg = new BufferedImage(src.getHeight(), src.getWidth(),src.getType());
			return imRotateOp.filter(src,this.bg);
		}
		
		//Nothing to do
		return src;

	}

    /** Surcharge de la fonction paintComponent() pour afficher notre image */
    public void paintComponent(Graphics g) {
    	//g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);  
    	
           g.drawImage(bg,startX,startY,null);
           if(!txt.equals(""))
        	   g.drawString(txt,startX+10,maxSize+startY+10);
   } 
	
	
}
