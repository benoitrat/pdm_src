/**
 * 
 */
package es.ugr.siar.gui.actionwin;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import es.ugr.siar.db.APNDBManager;
import es.ugr.siar.db.DatabaseManager;
import es.ugr.siar.gui.WinThumbsScroll;
import es.ugr.siar.gui.thumbs.WinThumbsDist;
import es.ugr.siar.gui.thumbs.WinThumbsMain;
import es.ugr.siar.ip.desc.VisualDescriptor;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.tools.ArraysTools;
import es.ugr.siar.tools.IndexedGeneric;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 11 janv. 08
 *
 */
public class WinShowClassDist extends JFrameParam implements ActionListener {

	/**     */
	private static final long serialVersionUID = 1L;

	public WinShowClassDist(String title, Dimension dim) {
		super(title, dim,BoxLayout.PAGE_AXIS);
		addActionButton();
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JButton) {

			//Database Manager
			DatabaseManager dbMan = new APNDBManager();

			//Obtain the classID Selected
			JRadioButton jrbC = getSelectedJRadioButton(0);
			String className = jrbC.getText();
			int classID = (new Integer(jrbC.getActionCommand())).intValue();

			//Obtain the Descriptor Selected
			JRadioButton jrbD = getSelectedJRadioButton(1);
			int descType = (new Integer(jrbD.getActionCommand())).intValue();

			//Obtain the Class Training Testing Sets Selected 
			JCheckBox cbVec[] = getSelectedJCheckBoxes(2);
			int [] train= null; int [] test= null;
			if(cbVec != null) {
				for(JCheckBox jcb : cbVec) {
					if(jcb.getActionCommand().equals(SetsFileTools.SetType.TRAIN.getExt())) {
						train = SetsFileTools.readPhotosID(className,SetsFileTools.SetType.TRAIN);
						System.out.println("Class train set");
					}
					if(jcb.getActionCommand().equals(SetsFileTools.SetType.TEST.getExt())) {
						test = SetsFileTools.readPhotosID(className,SetsFileTools.SetType.TEST);
						System.out.println("Class train set");
					}
				}
			}
			//Obtain the Other Training Testing Sets Selected 
			JCheckBox cbVecOther[] = getSelectedJCheckBoxes(3);
			int [] trainOther= null; int [] testOther= null;
			if(cbVecOther != null) {
				for(JCheckBox jcb : cbVecOther) {
					if(jcb.getActionCommand().equals(SetsFileTools.SetType.TRAIN.getExt())) {
						trainOther = SetsFileTools.readOthersPhotoID(className,SetsFileTools.SetType.TRAIN);
						System.out.println("Others train set");
					}
					if(jcb.getActionCommand().equals(SetsFileTools.SetType.TEST.getExt())) {
						testOther = SetsFileTools.readOthersPhotoID(className,SetsFileTools.SetType.TEST);
						System.out.println("Others test set");
					}
				}
			}

			//Get all files for this class ID
			int[] photosID = ArraysTools.arrayMerge(test,train,testOther,trainOther);

			if(photosID.length==0) return;

			Component cVec = this.getComponents(4)[1];
			if(cVec instanceof JTextField) {
				try{
					String tField = ((JTextField)cVec).getText();
					int lastIndex = new Integer(tField);
					System.out.println("Subsample with "+lastIndex +" instances");
					if(lastIndex>0 && lastIndex < photosID.length) {
						List<Integer> pIdList = ArraysTools.toList(photosID);
						Collections.shuffle(pIdList);
						photosID=ArraysTools.toPrimitiveVec(pIdList.subList(0, lastIndex));
					}
				}
				catch(NumberFormatException e2) {
					//System.out.println("Not good subsample number");
				}
			}
			System.out.println("Distance from "+className+" class model using "+jrbD.getText());

			//Get the model visual descriptor for this classID
			VisualDescriptor vDescRef = dbMan.getVisualDescriptor(descType, classID);


			//Obtain the index list with score (Float)
			List<IndexedGeneric<Float>> listId = dbMan.compare(vDescRef, photosID);
			if(listId == null) return;

//			int[] photosIdSorted = new int[listId.size()];
//			String [] photosTxt = new String[listId.size()];
//			for(int i=0;i<listId.size();i++) {
//			photosIdSorted[i] = listId.get(i).getId();
//			photosTxt[i] = listId.get(i).toString();
//			//System.out.println(photosTxt[i]);
//			}
//			new WinThumbsScroll("Distance of "+jrbC.getText()+" class using "+jrbD.getText(),photosIdSorted,photosTxt);
			new WinThumbsDist("Distance of "+jrbC.getText()+" class using "+jrbD.getText(),listId);

		}
	}

	/* (non-Javadoc)
	 * @see es.ugr.siar.gui.actionwin.JFrameParam#setAction()
	 */
	protected void addActionButton() {
		ok.addActionListener(this);

	}

}
