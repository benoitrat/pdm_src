/**
 * 
 */
package es.ugr.siar.gui.actionwin;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;

import es.ugr.siar.db.APNDBManager;
import es.ugr.siar.ml.Dump2ARFF;
import es.ugr.siar.ml.Dump2File;
import es.ugr.siar.ml.SetsFileTools.SetType;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 9 janv. 08
 *
 */
public class WinDumpInFile extends JFrameParam implements ActionListener {

	/**     */
	private static final long serialVersionUID = 1L;
	private ButtonGroup gClass = new ButtonGroup();

	public WinDumpInFile(String title,int w, int h, int boxLayOrientation) {
		super(title, new Dimension(w,h),boxLayOrientation);
		addActionButton();
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JButton) {

			//Get the class Names
			String[] classNames;
			if(getSelectedJCheckBoxes(1) == null) {
				//Only Selected
				JRadioButton[] classIdVec = getSelectedJRadioButtons(0);
				classNames =new String[classIdVec.length];
				for(int i=0;i<classIdVec.length;i++) {
					classNames[i] = classIdVec[i].getText();
				}
			}
			else {
				//One Versus Others 
				classNames = new String[2];
				classNames[0]=Dump2File.othersName;
				classNames[1]=getSelectedJRadioButton(0).getText();
			}

			//Get visual Descriptor Selected
			JCheckBox[] vDescVec = getSelectedJCheckBoxes(2);
			int[] descTypes = new int[vDescVec.length];
			for(int i=0;i<vDescVec.length;i++) descTypes[i]=new Integer(vDescVec[i].getActionCommand());

			//Get Exif Data or not
			JRadioButton jrb = getSelectedJRadioButton(2);
			boolean exif = (jrb!=null && jrb.getActionCommand().equals("EXIF"));
			
			//Get Testing or training
//			JRadioButton jrb2 = getSelectedJRadioButton(3);
//			SetsFileTools.SetType trainTestType;
//			if(jrb2.getActionCommand().equals(SetsFileTools.SetType.TRAIN.getExt())) {
//				trainTestType=SetsFileTools.SetType.TRAIN;
//			}
//			else {
//				//if(jrb.getActionCommand().equals(SetsFileTools.SetType.TEST.getExt())) {
//				trainTestType=SetsFileTools.SetType.TEST;
//			}
			


			
			Dump2File d2f = new Dump2ARFF(new APNDBManager(),classNames,descTypes,exif);
			if(!d2f.getFile(SetType.TRAIN).exists()) d2f.print(SetType.TRAIN);
			if(!d2f.getFile(SetType.TEST).exists()) d2f.print(SetType.TEST);
			
			System.out.println("The file has been dumped\n\n");
		}
		
		
		if(e.getSource() instanceof JCheckBox) {
			
			JCheckBox oVSa =(JCheckBox)e.getSource();
			boolean grouped=oVSa.isSelected();
			//Add to groups
			Component[] cVec = getComponents(0);
			for(Component c : cVec) {
				if(c instanceof JRadioButton) {
					JRadioButton jrb = (JRadioButton)c;
					if(grouped) gClass.add(jrb);
					else 		gClass.remove(jrb);
				}
			}
		}
}

/* (non-Javadoc)
 * @see es.ugr.siar.gui.actionwin.JFrameParam#setAction()
 */
protected void addActionButton() {
	ok.addActionListener(this);

}

}
