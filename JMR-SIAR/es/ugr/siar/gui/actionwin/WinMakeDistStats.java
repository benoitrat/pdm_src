/**
 * 
 */
package es.ugr.siar.gui.actionwin;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;

import es.ugr.siar.db.APNDBManager;
import es.ugr.siar.db.DatabaseManager;
import es.ugr.siar.ip.desc.VisualDescriptor;
import es.ugr.siar.ml.DistanceStats;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.tools.IndexedGeneric;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 9 janv. 08
 *
 */
public class WinMakeDistStats extends JFrameParam implements ActionListener {

	/**     */
	private static final long serialVersionUID = 1L;

	public WinMakeDistStats(String title,int w, int h, int boxLayOrientation) {
		super(title, new Dimension(w,h),boxLayOrientation);
		addActionButton();
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JButton) {
			DatabaseManager dbMan = new APNDBManager();

			//Get ClassID Selected
			JCheckBox[] classIdVec = getSelectedJCheckBoxes(0);

			//Get visual Descriptor Selected
			JCheckBox[] vDescVec = getSelectedJCheckBoxes(1);

	
			if(classIdVec != null && vDescVec != null) {
				//Iterate over each class ID
				for(JCheckBox classId : classIdVec) {
					String className = classId.getText();
					int classID = (new Integer(classId.getActionCommand()));
					
					//Select if the statistic are on the training or testing learning set
					SetsFileTools.SetType learnSetType = SetsFileTools.SetType.TRAIN;
					
					//Obtain the photoID of the training set and the opposite training set.
					int [] photosIdClass = SetsFileTools.readPhotosID(className,learnSetType);
					int [] photosIdOther = SetsFileTools.readOthersPhotoID(className,learnSetType);
					
					//Iterate over each visual Descriptor
					for(JCheckBox vDesc : vDescVec) {
						int vDescType = (new Integer(vDesc.getActionCommand()));
						
						// Get the model visual descriptor for this classID to be compare
						VisualDescriptor vDescRef = dbMan.getVisualDescriptor(vDescType, classID);
						
						//	Obtain the score for the class and the others photosID
						System.out.println("Compute distance stats for "+
								className+" class using "+vDesc.getText());
						List<IndexedGeneric<Float>> scoreClassList,scoreOtherList;
						scoreClassList = dbMan.compare(vDescRef, photosIdClass);
						scoreOtherList = dbMan.compare(vDescRef, photosIdOther);
						if(scoreClassList == null || scoreOtherList == null) return;

						
						//Write this in a file to make stats using Matlab
						DistanceStats.writeDistancePhotosID(className,learnSetType,vDescType, scoreClassList, true);
						DistanceStats.writeDistancePhotosID(className,learnSetType,vDescType, scoreOtherList, false);
					}
					System.out.println("All statistique distance files for "+className+" are correctly written\n\n");
				}
			}
		}
		this.setVisible(false);
	}

	/* (non-Javadoc)
	 * @see es.ugr.siar.gui.actionwin.JFrameParam#setAction()
	 */
	protected void addActionButton() {
		ok.addActionListener(this);

	}

}
