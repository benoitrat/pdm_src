/**
 * 
 */
package es.ugr.siar.gui.actionwin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 9 janv. 08
 *
 */
public abstract class JFrameParam extends JFrame  {

	/**     */
	private static final long serialVersionUID = 1L;

	JPanel jPanCenter;
	List<JPanel> panels = new ArrayList<JPanel>();
	JButton ok;
	boolean border=false;

	public JFrameParam(String title, Dimension dim, int boxOrientation) {
		super(title);
		setSize(dim);
		JPanel mainPanel = new JPanel();
		setContentPane(mainPanel);
		this.jPanCenter = new JPanel();
		ok = new JButton("OK");

		mainPanel.setLayout(new BorderLayout());
		mainPanel.add(ok, BorderLayout.SOUTH);
		mainPanel.add(jPanCenter, BorderLayout.CENTER);
		BoxLayout blayout = new BoxLayout(jPanCenter,boxOrientation);
		jPanCenter.setLayout(blayout);
		this.setVisible(true);
		this.setMaximumSize(new Dimension(300,500));
	}


	public void addCenter(Component c) {
		JPanel tmp = (JPanel)panels.get(panels.size()-1);
		tmp.add(c);
	}

	public void addCenter(Component c,int id) {
		if(panels.size() == id) {
			JPanel newPan = new JPanel();
			if(border) newPan.setBorder(BorderFactory.createLineBorder(Color.black));
			panels.add(newPan);
			newPan.validate();
			jPanCenter.add(newPan,id);
			newPan.add(c);
		}
		if(panels.size() > id) {
			panels.get(id).add(c);
			panels.get(id).validate();
		}
	}

	public Component[] getComponents(int id) {
		return panels.get(id).getComponents();
	}

	public JRadioButton getSelectedJRadioButton(int id) {
		Component[] cList = getComponents(id);
		if(cList != null) { 
			for(int i=0;i<cList.length;i++) {
				if(cList[i] instanceof JRadioButton) {
					JRadioButton tmp = (JRadioButton)cList[i];
					if(tmp.isSelected()) return tmp; 
				}
			}
		}
		return null;
	}

	public JRadioButton[] getSelectedJRadioButtons(int id) {
		Component[] cList = getComponents(id);
		if(cList != null) {
			List<JRadioButton> jrbList = new ArrayList<JRadioButton>();
			for(int i=0;i<cList.length;i++) {
				if(cList[i] instanceof JRadioButton) {
					if(((JRadioButton)cList[i]).isSelected()) {
						jrbList.add((JRadioButton)cList[i]);
					}
				}
			}
			if(jrbList.size() != 0) {
				JRadioButton[] jrbVec = new JRadioButton[jrbList.size()];
				return jrbList.toArray(jrbVec);
			}
		}
		return null;
	}


	public JCheckBox[] getSelectedJCheckBoxes(int id) {
		Component[] cList = getComponents(id);
		int count=0;
		for(int i=0;i<cList.length;i++) {
			if(cList[i] instanceof JCheckBox) {
				if(((JCheckBox)cList[i]).isSelected()) {
					count++;
				}
			}
		}
		if(count>0) {
			JCheckBox tmp[] = new JCheckBox[count];
			count=0;
			for(int i=0;i<cList.length;i++) {
				if(cList[i] instanceof JCheckBox) {
					if(((JCheckBox)cList[i]).isSelected()) {
						tmp[count] = (JCheckBox)cList[i];
						count++;
					}
				}
			}
			return tmp;
		}

		return null;
	}





	public void setBorder(boolean b) {
		border=b;
	}

	protected abstract void addActionButton();

}
