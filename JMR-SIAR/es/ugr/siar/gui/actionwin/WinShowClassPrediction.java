/**
 * 
 */
package es.ugr.siar.gui.actionwin;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;

import es.ugr.siar.db.APNDBManager;
import es.ugr.siar.db.DatabaseManager;
import es.ugr.siar.gui.WinThumbsScroll;
import es.ugr.siar.gui.thumbs.WinThumbsDist;
import es.ugr.siar.gui.thumbs.WinThumbsMain;
import es.ugr.siar.ip.desc.VisualDescriptor;
import es.ugr.siar.ml.SetsFileTools;
import es.ugr.siar.ml.weka.CptsFullViewer;
import es.ugr.siar.ml.weka.cptinst.CptInstGlobal.CptInstGlobalType;
import es.ugr.siar.tools.ArraysTools;
import es.ugr.siar.tools.IndexedGeneric;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 11 janv. 08
 *
 */
public class WinShowClassPrediction extends JFrameParam implements ActionListener {

	/**     */
	private static final long serialVersionUID = 1L;

	public WinShowClassPrediction(String title, Dimension dim) {
		super(title, dim,BoxLayout.PAGE_AXIS);
		addActionButton();
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JButton) {
							
				//Obtain the classID Selected
				JRadioButton jrbC = getSelectedJRadioButton(0);
				String className = jrbC.getText();

				//Obtain view TYPE (Between TP,FP,ALL)
				JRadioButton jrbD = getSelectedJRadioButton(1);
				int viewTypeInd = new Integer(jrbD.getActionCommand());
				CptsFullViewer.ViewType viewType = (CptsFullViewer.ViewType.values())[viewTypeInd];
								
				//Obtain the Other Training Testing Sets Selected
				JCheckBox cbVec[] = getSelectedJCheckBoxes(2);
				boolean conjClass= (cbVec != null);
				
				System.out.println("Distance from "+className+" class model using "+jrbD.getText());
				
			
				CptsFullViewer cptViewer = new CptsFullViewer();
				cptViewer.show(className,viewType, CptInstGlobalType.MixedProba, conjClass);
				
		}
	}

	/* (non-Javadoc)
	 * @see es.ugr.siar.gui.actionwin.JFrameParam#setAction()
	 */
	protected void addActionButton() {
		ok.addActionListener(this);
		
	}
	
}
