/**
 * 
 */
package es.ugr.siar.gui.actionwin;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JSlider;

import es.ugr.siar.db.APNDBManager;
import es.ugr.siar.db.DatabaseManager;
import es.ugr.siar.ml.SetsFileTools;

/**
 * Windows to select the class name and to build the file training and testing classes.
 * 
 * It construct classname.test and classname.train where photosId are randomly mixed.
 * The percentage or training set is given by the specific row.
 * 
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 9 janv. 08
 *
 */
public class WinMakeClassesSets extends JFrameParam implements ActionListener {

	/**     */
	private static final long serialVersionUID = 1L;

	public WinMakeClassesSets(String title, Dimension dim, int boxLayOrientation) {
		super(title, dim,boxLayOrientation);
		addActionButton();
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JButton) {
				DatabaseManager dbMan = new APNDBManager();
				
				//Obtain the classID
				JSlider js = (JSlider)getComponents(1)[0];
				int trainPercent = js.getValue();

				//For each visual Descriptor Selected
				JCheckBox[] jcbVec = getSelectedJCheckBoxes(0);
				if(jcbVec != null) 
				for(int i=0;i<jcbVec.length;i++) {
					String className = jcbVec[i].getText();
					String classID = jcbVec[i].getActionCommand();
					int[] photosID = dbMan.getOneClassPhotosID(classID);
					SetsFileTools.writeTrainTestPhotosID(className,trainPercent,photosID);
				}
				
				System.out.println("Then write the global set");
				
				//Then write the files with all the photoID from the test files
				SetsFileTools.writeGlobalSet(SetsFileTools.SetType.TRAIN);
				SetsFileTools.writeGlobalSet(SetsFileTools.SetType.TEST);
		}
	}

	/* (non-Javadoc)
	 * @see es.ugr.siar.gui.actionwin.JFrameParam#setAction()
	 */
	protected void addActionButton() {
		ok.addActionListener(this);
		
	}
	
}
