/**
 * 
 */
package es.ugr.siar.gui.actionwin;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import es.ugr.siar.ml.SetsFileTools;

/**
 * Windows to select a set of class name to build the global class.
 * 
 * It construct global.test where all photosId are mixed
 * 
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 9 janv. 08
 *
 */
public class WinMakeGlobalClass extends JFrameParam implements ActionListener {

	/**     */
	private static final long serialVersionUID = 1L;

	public WinMakeGlobalClass(String title, Dimension dim, int boxLayOrientation) {
		super(title, dim,boxLayOrientation);
		addActionButton();
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JButton) {
				
				//Get each CheckBox Selected corresponding to a class.
				JCheckBox[] jcbVec = getSelectedJCheckBoxes(0);
				if(jcbVec == null) return;
				
				//Get the name of each class.
				String[] classNames = new String[jcbVec.length];
				for(int i=0;i<jcbVec.length;i++) {
					classNames[i]=jcbVec[i].getText();
				}
				
				//Write in a file which class construct the global set
				SetsFileTools.writeGlobalClassNames(classNames);
				
				//Then write the files with all the photoID from the test files
				SetsFileTools.writeGlobalSet(SetsFileTools.SetType.TRAIN);
				SetsFileTools.writeGlobalSet(SetsFileTools.SetType.TEST);
		}
	}

	/* (non-Javadoc)
	 * @see es.ugr.siar.gui.actionwin.JFrameParam#setAction()
	 */
	protected void addActionButton() {
		ok.addActionListener(this);
		
	}
	
}
