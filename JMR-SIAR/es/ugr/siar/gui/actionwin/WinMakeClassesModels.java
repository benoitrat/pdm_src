/**
 * 
 */
package es.ugr.siar.gui.actionwin;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;

import es.ugr.siar.db.APNDBManager;
import es.ugr.siar.db.DatabaseManager;
import es.ugr.siar.ml.SetsFileTools;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 9 janv. 08
 *
 */
public class WinMakeClassesModels extends JFrameParam implements ActionListener {

	/**     */
	private static final long serialVersionUID = 1L;

	public WinMakeClassesModels(String title, Dimension dim, int boxLayOrientation) {
		super(title, dim,boxLayOrientation);
		addActionButton();
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JButton) {
			DatabaseManager dbMan = new APNDBManager();

			//Get ClassID Selected
			JCheckBox[] classIdVec = getSelectedJCheckBoxes(0);

			//Get visual Descriptor Selected
			JCheckBox[] vDescVec = getSelectedJCheckBoxes(1);

	
			if(classIdVec != null && vDescVec != null) {
				//Iterate over each class ID
				for(JCheckBox classId : classIdVec) {
					String className = classId.getText();
					int classID = (new Integer(classId.getActionCommand()));
					//Iterate over each visual Descriptor
					for(JCheckBox vDesc : vDescVec) {
						int vDescID = (new Integer(vDesc.getActionCommand()));
						//Call the MakeModel 
						System.out.println("MakeModel for "+className+" class using "+vDesc.getText());
						int[] photosID = SetsFileTools.readPhotosID(className,SetsFileTools.SetType.TRAIN);
						dbMan.MakeModel(vDescID, classID,photosID);
					}
				}
			}
		}
		this.setVisible(false);
	}

	/* (non-Javadoc)
	 * @see es.ugr.siar.gui.actionwin.JFrameParam#setAction()
	 */
	protected void addActionButton() {
		ok.addActionListener(this);

	}

}
