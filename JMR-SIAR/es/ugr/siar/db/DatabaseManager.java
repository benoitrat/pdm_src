/**
 * 
 */
package es.ugr.siar.db;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import es.ugr.siar.ip.ImageJMR;
import es.ugr.siar.ip.desc.ExifDescriptor;
import es.ugr.siar.ip.desc.VisualDescriptor;
import es.ugr.siar.tools.IndexedGeneric;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 18 dec. 07
 *
 */
public abstract class DatabaseManager {

	private String [] dbParams = null;
	private VisualDescriptor vDescRef;
	private int maxResult=20;
	private boolean[] vDescImpl = VisualDescriptor.IMPLEMENTED_DESC;

	
	/**
	 * Create the paramaters to connect to the dabase.
	 * dbParams = new String[] {dbServer,dbUser,dbPass,dbName};
	 * 
	 * @param dbParams Array of parameters
	 */
	public DatabaseManager(String[] dbParams) {
		this.dbParams = dbParams;
	}



	public List compare(int descType, int imID) {
		MySQL db = new MySQL(dbParams);

		this.vDescRef = VisualDescriptor.getInstance(descType);
		vDescRef.fromMySQL(imID, db);

		db.close();

		return compare();

	}

	public List compare(VisualDescriptor vDescRef) {
		this.vDescRef = vDescRef;
		return compare();
	}

	
	/**
	 * 
	 * @deprecated
	 */
	private List compare() {

		if(vDescRef == null) {
			System.err.println("The Descriptor to compare is not available");
			return null;
		}
		int descType = vDescRef.getType();
		MySQL db = new MySQL(dbParams);

		String sql ="SELECT "+vDescRef.getTableName()+".Photo_ID \n"+
		"FROM "+vDescRef.getTableName()+
		"LIMIT 0,1000"; //TODO:Change this value
		db.queryResult(sql);

		int[] listID = db.getColumnInt("Photo_ID");
		db.closeResultSet();
		List vDescList = new ArrayList();

		for(int i=0; i<listID.length;i++) {
			int photoID = listID[i];
			ScoreVDescID tmp = new ScoreVDescID(photoID);
			tmp.setDescriptor(descType, db); //Create descriptor (Slow Version because it make a SQL query each time)
			tmp.setScore(vDescRef);
			vDescList.add(tmp);
		}

		Collections.sort(vDescList); //Sort best Score.

		for(int i=0;i<maxResult;i++) {
			ScoreVDescID tmp = (ScoreVDescID)vDescList.get(i);
			System.out.println("#"+i+": "+tmp.getImID()+" with score "+
					new my.PrintfFormat("%3.3f").sprintf((float)(100*tmp.getScore())));

		}
		return vDescList;
	}

	public List<IndexedGeneric<Float>> compare(VisualDescriptor vDescRef, int [] photosID) {
				
		if(vDescRef == null) {
			System.err.println("The Descriptor to compare is not available");
			return null;
		}
		//Create the list of indexed Float
		List<IndexedGeneric<Float>> vDescList = new ArrayList<IndexedGeneric<Float>>();

		//Obtain the distance list according to the photosID list.
		float dist[] = getDistance(vDescRef, photosID);
		
		//Fill the List of Indexed Float
		String percent;
		for(int i=0;i<photosID.length;i++) {
			percent = new my.PrintfFormat("%3.3f").sprintf(dist[i]*100)+"%";
			vDescList.add(new IndexedGeneric<Float>(photosID[i],"#"+photosID[i]+", d="+percent,dist[i]));
		}

		//Sort from the smallest to the highest
		Collections.sort(vDescList); 

		return vDescList;
	}
	
	public void extractMPEG7(String sql) {
		//Query the MySQL database
		MySQL db = getDataBase(); 
		db.queryResult(sql);
		int[] photosID = db.getIndexes();
		//Return the database
		db.close();

		this.extractMPEG7(photosID);
	}
	
	public void extractMPEG7(int[] photosID) {
		MySQL db = getDataBase();
		
		for(int photoID : photosID) {
			
//			Set image at null for each new image
			ImageJMR im = null;
			for(int dType=0;dType<vDescImpl.length;dType++) {
				if(vDescImpl[dType]) {
					
					//Create an instance of the VisualDescriptor object and check if it exist in db
					VisualDescriptor vDesc = VisualDescriptor.getInstance(dType);
					if(!vDesc.exist(photoID,db,true)) {
						//Try to load the image if it doesn't exist
						if(im == null) {
							File imFile = this.ImgDbFile(photoID);
							if(imFile != null) im = ImageJMR.loadFromFile(imFile);
						}
						//If image is loaded previously
						if(im != null) {
							vDesc.extract(im);
							vDesc.toMySQL(photoID, db);
							db.closeResultSet();
						}
					} 
				}
			}//End of vDesc corresponding to dType
		} //End of loop on all the photosID.

	}

	public VisualDescriptor getVisualDescriptor(int typeDesc,int photoID) {
		MySQL db = getDataBase();
		VisualDescriptor vDesc = VisualDescriptor.getInstance(typeDesc);
		if(vDesc.exist(photoID, db, true)) { 
			vDesc.fromMySQL(photoID,db);
			return vDesc;
		}
		else {
			System.err.println("Descriptor not found in database");
			return null;
		}
	}
	
	public int[] getOneClassPhotosID(String classID) {
		MySQL db = getDataBase();

		//Select the SQL Syntax to retrieve groupes
		String sql ="SELECT SyntaxSQL FROM Classes WHERE ID = "+classID;
		db.queryOneRowResult(sql);
		ResultSetJMR res = db.getResultSetJMR();
		sql = res.getValueS("SyntaxSQL");
		db.closeResultSet();

		db.queryResult(sql);
		int photosID[] =  db.getResultSetJMR().getColumnInt("Photo_ID");
		db.closeResultSet();
		db.close();

		return photosID;
	}

	public void MakeModel(int descType, int classID, int[] photosID) {
		
		if(classID > startClassIndex()) {
			MySQL db = getDataBase();
			//Compute the model for this descriptor
			VisualDescriptor vDescMean = vDescMean(descType, photosID);
			
			
			//Erase the previous model in the database
			db.queryUpdate("DELETE FROM "+vDescMean.getTableName()+" WHERE `Photo_ID` = "+classID);
			
			//Save under the classID (like if it was an image)
			vDescMean.toMySQL(classID, db);
			db.close();
			
		}
	}

	public VisualDescriptor vDescMean(int descType, int[] photosID) {
		MySQL db = getDataBase();

		VisualDescriptor vDescRef=VisualDescriptor.getInstance(descType);
		vDescRef.fromMySQL(photosID[0], db);
		int count=1;

		VisualDescriptor vDescOther = VisualDescriptor.getInstance(vDescRef.getType());
		for(int i=1; i<photosID.length;i++) {
			if(vDescOther.fromMySQL(photosID[i], db)) {
				vDescRef.sum(vDescOther);
				count++;
			}
		}
		if(count != photosID.length)
			System.out.println("nof_add/nof_exist="+count+"/"+photosID.length);
		else 
			System.out.println("count="+count);
		vDescRef.multiply(1.0f/count);

		return vDescRef;
	}

	public float[] getDistance(VisualDescriptor vRef, int[] photosID) {
		MySQL db = getDataBase();
		VisualDescriptor vDescOther = VisualDescriptor.getInstance(vRef.getType());
		float[] d = new float[photosID.length]; 
		
		for(int i=0;i<photosID.length;i++ ) {
			vDescOther.fromMySQL(photosID[i],db);
			d[i] = vRef.compare(vDescOther);
		}
		return d;
	}
	
	public String getKeywords(int photoID) {
		String kw ="";
		MySQL db = getDataBase();

		//Select the SQL Syntax to retrieve groupes
		String sql ="SELECT Keyword FROM Keywords WHERE Photo_ID="+photoID;
		db.queryResult(sql);
		ResultSetJMR res = db.getResultSetJMR();
		int count=1;
		while(res.nextRow()) {
			kw +=res.getValueS("Keyword")+", ";
			//if(count%5==0) kw+="\n";
			count++;
		}
		db.closeResultSet();
		return kw.substring(0,kw.length()-2);
	}
	
	
	public String getExif(int photoID) {
		String kw ="";
		MySQL db = getDataBase();

		//Select the SQL Syntax to retrieve groupes
//		String sql ="SELECT ";
//		sql+=" (DAYOFYEAR( DateTime )-1)/30.5 MoY, TIME_TO_SEC( TIME( DateTime ) ) /3600 HoD, ";
//		sql+=" FlashOn,  FlashOn & 0x01 FFi, FlashOn >> 3 & 0x03 FM, FlashOn >> 6 & 0x01 ReM, ";
//		sql+=" 2 * LOG2( Fnumber ) CAP, ";
//		sql+=" -1 * LOG2( ExpTime ) CSS, ExpProgra EpP ,ExpMode EpM, ";
//		sql+=" LightSource LS,MeteringMode MM, ISOSpeed ISR, ";
//		sql+=" FocalLength FL, SubDistRange SDR, SceneCapType SCT ";
//		sql+="FROM ExifData WHERE Photo_ID="+photoID;
	
		ExifDescriptor eDesc = new ExifDescriptor();
		eDesc.fromMySQL(photoID, db);
		return eDesc.toString();
		
	}
	
	/**
	 * Transform a className in a classID making a SQL request.
	 * 
	 * @param className the className
	 * @return			the classID
	 */
	public int getClassIdFromName(String className) {
		int cID=-1;
		MySQL db = getDataBase();

		//Select the SQL Syntax to retrieve groupes
		String sql ="SELECT ID FROM Classes WHERE Name = '"+className+"';";
		db.queryOneRowResult(sql);
		cID = db.getValueInt("ID");
		return cID;
	}

	public abstract MySQL getDataBase();
	public abstract List<IndexedGeneric<String>> getClassesSyntax();
	
	public abstract File ImgDbThumbsFile(int photoID);
	public abstract String ImgDbThumbsPath(int photoID);
	public abstract File ImgDbFile(int photoID);
	public abstract String ImgDbPath(int photoID);
	public abstract int startClassIndex();
	
}
