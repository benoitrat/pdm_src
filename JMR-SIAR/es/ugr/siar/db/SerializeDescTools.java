/**
 * 
 */
package es.ugr.siar.db;

import java.io.File;
//import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import es.ugr.siar.ip.ImageJMR;
import es.ugr.siar.ip.desc.VisualDescriptor;


/**
 * A set of tools used to set an Array of {@link VisualDescriptor} serialized 
 * and to recover it from a file.
 * The name in the method give how you can use it. 
 * 
 * 
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 1.0, 28 nov. 07
 * @deprecated This class is not anymore used since 18 dec. 07
 *
 */
public class SerializeDescTools {


	public static void toSerialize(String fname,Object obj) {
		FileOutputStream fos;
		ObjectOutputStream oos;
		try {
			fos = new FileOutputStream(fname);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(obj);		
			oos.close();
			System.out.println("Object Correctly Serialized in :"+fname);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//FileNotFoundException
			System.err.println("System could not write the file or something like this");
			e.printStackTrace();
		}
	}

	public static VisualDescriptor[] fromSerializable(String fname) {
		VisualDescriptor[] descArray = null;
		try {
			FileInputStream fis = new FileInputStream(fname);
			ObjectInputStream ois = new ObjectInputStream(fis);	

			Object obj=ois.readObject();
			if(obj instanceof VisualDescriptor[]) 	descArray = (VisualDescriptor[])obj;
			ois.close();
		}
		catch (Exception e) {
			if(e instanceof IOException)
				System.err.println("System could not read the file or something like this");
			else 
				e.printStackTrace();
		}

		return descArray;
	}


	public static String imgNameToDescName(File file) {
		//Remove extension
		String fname=file.getAbsolutePath();
		fname = fname.substring(0,fname.lastIndexOf('.'));
		//Add
		return fname+".desc";
	}

	public static void computeDirDescriptors(File directory) {
		//File directory = new File("/home/neub/pdm/db/myphotos/");
		System.out.println ( directory.getAbsolutePath());
		if ( directory.isDirectory ( ) ) {
			File[] list = directory.listFiles();
			if (list != null){
				for ( int i = 0; i < list.length; i++) {
					computeImgDescriptors(list[i]);
				} 
			} else {
				System.err.println(directory + " : Directory can't be read.");
			}
			System.out.println("----------------------------------------------------------------");
			System.out.println("End of descriptors's computation and serialization of all images");
			System.out.println("----------------------------------------------------------------");
		}
	}
	
	public static List compareDirDescriptor(File directory,File im,int descType) {
		//File directory = new File("/home/neub/pdm/db/myphotos/");
		System.out.println ( directory.getAbsolutePath());
		
		VisualDescriptor desc1 = fromSerializable(imgNameToDescName(im))[descType];
		VisualDescriptor desc2;
		List scoreList = new ArrayList();
				
		if ( directory.isDirectory ( ) ) {
			File[] list = directory.listFiles();
			if (list != null){
				for ( int i = 0; i < list.length; i++) {
					if(!im.equals(list[i])) {
						if(list[i].isFile() && isJpegFile(list[i])) {
							File fdesc = new File(imgNameToDescName(list[i]));
							if(!fdesc.isFile()) {
								System.err.println("File description not found, you may process it first");
							}
							else {
								desc2 = fromSerializable(fdesc.getAbsolutePath())[descType];
								scoreList.add(new FileScore(list[i],desc1.compare(desc2)));
							}
						}
					}
				} 
			} else {
				System.err.println(directory + " : Directory can't be read.");
			}
			System.out.println("----------------------------------------------------------------");
			System.out.println("End of descriptors's computation and serialization of all images");
			System.out.println("----------------------------------------------------------------");
			
			Collections.sort(scoreList);
			
			for(int i=0;i<scoreList.size();i++) {
				FileScore tmp = (FileScore)scoreList.get(i);
				System.out.println(i+". File :"+tmp.getName()+" with score="+tmp.getScore());
			}
		}
		return scoreList;
	}

	public static float[] compareDescriptors(VisualDescriptor[] desc1, VisualDescriptor[] desc2) {
		
		//Load the descriptors computed
		boolean [] descTypes = new boolean[VisualDescriptor.NOF_DESCRIPTORS];
		float [] dist = new float[VisualDescriptor.NOF_DESCRIPTORS];
		for(int i=0; i<descTypes.length; i++) descTypes[i] = false;
		descTypes[VisualDescriptor.DESC_MPEG7_SCD]= true;
		descTypes[VisualDescriptor.DESC_MPEG7_CSD]= true;
		descTypes[VisualDescriptor.DESC_MPEG7_EHD]= true;
		
		for(int i=0; i<descTypes.length; i++) {
			if(descTypes[i]){
				dist[i] = desc1[i].compare(desc2[i]);
			}
			else dist[i] = Float.NaN;
		}
		return dist;
	}

	public static void computeImgDescriptors(File file) {
		if(file.isFile()) {
			if(isJpegFile(file)) {
				ImageJMR imSrc = ImageJMR.loadFromFile(file);
				DescImFile descImF = new DescImFile(imSrc);
				if(imSrc == null) return;  //Dont compute a imageSrc that can't be open.
				System.out.println("Opening "+file+"...");
				File descFile = new File(imgNameToDescName(file));
				//If there is a description File, load the description file
				boolean [] descTypes = new boolean[VisualDescriptor.NOF_DESCRIPTORS];
				for(int i=0; i<descTypes.length; i++) descTypes[i] = false;
				// Prepare the descriptors that are going to be used.
				if(descFile.exists()) {
					
					descImF.setVisualDescriptorVec(fromSerializable(descFile.getAbsolutePath()));
					if(descImF.getDescriptor(VisualDescriptor.DESC_MPEG7_SCD) == null) {
						descTypes[VisualDescriptor.DESC_MPEG7_SCD]= true;
					}
					else System.out.println("Descriptors SCD is found");
					if(descImF.getDescriptor(VisualDescriptor.DESC_MPEG7_CSD) == null) {
						descTypes[VisualDescriptor.DESC_MPEG7_CSD]= true;
					}
					else System.out.println("Descriptors CSD is found");	
					if(descImF.getDescriptor(VisualDescriptor.DESC_MPEG7_EHD) == null) {
						descTypes[VisualDescriptor.DESC_MPEG7_EHD]= true;
					}
					else System.out.println("Descriptors EHD is found");
					
				}
				else {
					descTypes[VisualDescriptor.DESC_MPEG7_SCD]= true;
					descTypes[VisualDescriptor.DESC_MPEG7_CSD]= true;
					descTypes[VisualDescriptor.DESC_MPEG7_EHD]= true;

				}
				//Compute the missing descriptors
				VisualDescriptor vDesc;
				for(int i=0; i<descTypes.length; i++) {
					if(descTypes[i]){
						vDesc = VisualDescriptor.getInstance(i, imSrc);
						if(vDesc == null) System.out.println("You are using an unknown descriptor type");
						else descImF.setDescriptor(vDesc);
					}
				}
				//Serialize the descArray to the descriptor file.
				toSerialize(descFile.getAbsolutePath(),descImF.getVisualDescriptorVec());
				System.out.println();
			}
		}
	}


	public static boolean isJpegFile(File file) {
		String MIME = getMIMEType(file);
		return (MIME.equals("image/jpeg"));
	}

	public static String getMIMEType(File file){
		if(file.isDirectory()){return "repertoire";}
		if(!file.exists()){return "fichier inexistant";}
		try{
			URL url = (file.toURI()).toURL();
			URLConnection connection = url.openConnection();
			return connection.getContentType();
		}catch(MalformedURLException mue){
			return mue.getMessage();
		}catch(IOException ioe){
			return ioe.getMessage();
		}
	}



}
