/**
 * 
 */
package es.ugr.siar.db;

import java.io.File;


import es.ugr.siar.ip.ImageJMR;
import es.ugr.siar.ip.desc.VisualDescriptor;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 18 dec. 07
 *
 */
public class ScoreVDescID implements java.lang.Comparable<ScoreVDescID> {
	
	private int imID;
	private VisualDescriptor vDesc;
	private float score;
	private boolean computeEmpty;
	
	public ScoreVDescID(int imID) {
		super();
		this.imID = imID;
		this.computeEmpty = false;
	} 
	
	public ScoreVDescID(int imID, VisualDescriptor vDesc) {
		super();
		this.imID = imID;
		this.vDesc = vDesc;
		this.computeEmpty = false;
	} 
	
	public void setComputeEmpty(boolean val) {
		this.computeEmpty = val;
	}
	

	public void setDescriptor(int descType, MySQL db) {
		if(0 <descType && descType < VisualDescriptor.NOF_DESCRIPTORS) {
			this.vDesc = VisualDescriptor.getInstance(descType);
			if(computeEmpty && !vDesc.exist(imID,db,true)) {
				//TODO: Maybe make this an option to compute or not descriptors that don't exist
				String fname = getFileName();
				ImageJMR im = ImageJMR.loadFromFile(new File(fname));
				vDesc.extract(im);
				vDesc.toMySQL(imID, db);
				db.closeResultSet(); //This is maybe not usefull.
			}
			vDesc.fromMySQL(imID, db);
			db.closeResultSet();
		}
	}
	
	public void setScore(VisualDescriptor vDesc2) {
		//Check if both descriptor exist
		if(vDesc != null && vDesc2 != null && vDesc.isComputed() && vDesc2.isComputed()) {
			//Check if both descriptors are the same type.
			if(vDesc.getType() == vDesc2.getType())
				score = vDesc.compare(vDesc2);
		}
	}
	
	
	public int compareTo(ScoreVDescID obj) {
		if(obj instanceof ScoreVDescID) {
			ScoreVDescID other = (ScoreVDescID)obj;
			if (other.score > this.score)  return -1; 
			else if(other.score == this.score) return 0; 
			else return 1; 
		}
		return 0;
	} 
	
	
	//TODO: Should depend on the database used.
	public String getFileName() {
		String fname = "/home/neub/pdm/db/waap/good/";
		fname += new my.PrintfFormat("%5d").sprintf(imID)+".jpg";
		return fname;
	}

	/**
	 * @return the imID
	 */
	public int getImID() {
		return imID;
	}

	/**
	 * @return the score
	 */
	public float getScore() {
		return score;
	}

	
	
	
	
	
	
	
}
