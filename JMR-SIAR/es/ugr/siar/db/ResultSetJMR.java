/**
 * 
 */
package es.ugr.siar.db;

import java.sql.SQLException;
import java.sql.ResultSet;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 9 janv. 08
 *
 */
public class ResultSetJMR {
	
	private ResultSet result;
	private int id=-1;
	private int count=0;
	
	//---------------------------
	
	public ResultSetJMR(ResultSet result) {
		this.result=result;
	}
	
	public ResultSetJMR(ResultSet r, int ID) {
		this.id = ID;
	}

	//---------------------------
	
	
	/**
	 * Method that read next register (Row) in ResultSet
	 * <p>We can use this function in a while loop: 
	 * while(sql.nextRS()) { sql.getResultSet() }
	 * </p>
	 * 
	 * @see #queryResult(String)
	 * @return True value if the operation was done correctly
	 */
	public boolean nextRow(){
		try {
			if (result.next()) {
				count++;
				return true;
			}
		}
		catch (SQLException e) {
			return errorManager("Bad Next Result Operation",e);
		}
		catch(NullPointerException e){
				return errorManager("Not good ID",e);
		}
		return false;
	}

	/**
	 * Method that read previous register (Row) in ResultSet
	 *  
	 * @see #queryResult(String)
	 * @return True value if the operation was done correctly
	 */
	public boolean previousRow(){
		boolean ret=false;
		try {
			if (result.previous()) {
				count--;
				ret=true;
			}
		}
		catch (SQLException e) {
			errorManager("getValue Error", e);
		}
		return ret;
	}
	
	public int getCount() {
		return count;
	}
	

	//------------------

	/**
	 * get the value of a specific column/attribute of the actual row.
	 * 
	 * @param 	attribute 	Name of the column of the attribute to read
	 * @see #queryResult(String)
	 * @see #nextRow()
	 * @see #previousRow()
	 * @return the result for this column in <b>String</b> format
	 */
	public String getValueS(String attribute){
		String ret=null;
		if(result == null)
			ret=null;
		else{
			try {
				ret= result.getString(attribute);
			}
			catch (Exception e) {
				errorManager("getValue Error", e);
			}
		}
		return ret;
	}

	/**
	 * get the value of a specific column/attribute of the actual row.
	 * 
	 * @param 	attribute 	Name of the column of the attribute to read
	 * @see #queryResult(String)
	 * @see #nextRow()
	 * @see #previousRow()
	 * @return the result for this column in <b>Int</b> format
	 */
	public int getValueInt(String attribute){
		int ret=-1;
		if(result == null)
			ret= -1;
		else{
			try {
				ret= result.getInt(attribute);
			}
			catch (Exception e) {
				
				errorManager("Attribute:"+attribute+" error or bad Row Selection", e);
			}
		}
		return ret;
	}

	/**
	 * get the value of a specific column/attribute of the actual row.
	 * 
	 * @param 	attribute 	Name of the column of the attribute to read
	 * @see #queryResult(String)
	 * @see #nextRow()
	 * @see #previousRow()
	 * @return the result for this column in <b>Double</b> format
	 */
	public double getValueDouble(String attribute){
		double ret=-1;
		if(result == null)
			ret= -1;
		else{
			try {
				ret= result.getDouble(attribute);
			}
			catch (Exception e) {
				errorManager("Attribute:"+attribute+" error", e);
			}
		}
		return ret;
	}
	
	
	public byte[] getBytes(String attribute) {
		byte [] ret = null;
		if(result == null) return ret;
		else{
			try {
				ret= result.getBytes(attribute);
			}
			catch (Exception e) {
				errorManager("Attribute:"+attribute+" error", e);
			}
		}
		return ret;
	}
	


	public int[] getColumnInt(String attribute) {

		int [] ret = null;
		try {
			if(result.last()){
				int nof_rows = result.getRow();
				result.first();
				ret = new int[nof_rows];
				int count=0;
				do {
					ret[count++] = result.getInt(attribute);
				}
				while(result.next());
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return ret;
	}
	
	
	//----------------------------------------
	
	public int getID() {
		return id;
	}
	
	public void setID(int id) {
		this.id = id;
	}
	
	
	/**
	 * Function to manage error message due to bad MySQL syntax or connection problem
	 * 
	 * 
	 * @param msg 	A string message give by the user
	 * @param e		The automatic generated exception
	 * @return		A false value meaning that the system have found an error.
	 */
	private boolean errorManager(String msg,Exception e) {
		if(e instanceof SQLException) {
			System.err.println(msg+"\n"+e.getMessage().toString());
		}
		else {
			e.printStackTrace();
		}
//		JOptionPane.showMessageDialog(this, ex.getMessage().toString(), "Error...", JOptionPane.ERROR_MESSAGE);
		return false;

	}

}
