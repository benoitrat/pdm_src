/**
 * 
 */
package es.ugr.siar.db;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import es.ugr.siar.tools.IndexedGeneric;

/**
 * 
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 10 janv. 08
 *
 */
public class APNDBManager extends DatabaseManager {

	public static String 	dbServerHost 	= my.Config.dbServer;
	public static String 	dbUserName 		= my.Config.dbUser;
	public static String 	dbPassword		= my.Config.dbPassword;
	public static String 	dbName 			= my.Config.dbDefaultName;
	public static int 		dbMaxImageIndex	= my.Config.dbMaxImageIndex;

	/**
	 * @param dbName
	 */
	public APNDBManager() {
		super(new String[] {dbServerHost,dbUserName,dbPassword,dbName});
	}


	@Override
	public File ImgDbThumbsFile(int photoID) {
		File f = new File(ImgDbThumbsPath(photoID));
		if(f.exists()) return f;
		else return null;
	}

	@Override
	public String ImgDbThumbsPath(int photoID) {
		String path="";
		String subsetID=new my.PrintfFormat("%02d").sprintf(photoID/1000);
		path=my.Config.dbThumbsPath+subsetID+my.Config.OS_Slash+my.Config.dbThumbsPrefix;
		path+=new my.PrintfFormat("%05d").sprintf(photoID);
		path+=my.Config.dbThumbsSuffix+".jpg";
		return path;
	}

	@Override
	public File ImgDbFile(int photoID) {
		File f = new File(ImgDbPath(photoID));
		System.out.println(f.getAbsolutePath());
		if(f.exists()) return f;
		else return null;
	}

	@Override
	public String ImgDbPath(int photoID) {
		String path="";
		String subsetID=new my.PrintfFormat("%02d").sprintf(photoID/1000);
		path=my.Config.dbDefaultPath+subsetID+my.Config.OS_Slash;
		path+=new my.PrintfFormat("%05d").sprintf(photoID);
		path+=".jpg";
		return path;
	}


	@Override
	/**
	 * Constructor that create a connection on the APN Database using the parameters in my.Config
	 * <ul>
	 * <li>host=localhost</li>
	 * <li>user=pdm</li>
	 * <li>pass=bnel7n</li>
	 * </ul>
	 *  
	 * @param 	db 	name of the database
	 */
	public MySQL getDataBase() {
		return new MySQL(dbServerHost,dbUserName,dbPassword,dbName);
	}


	public List<IndexedGeneric<String>> getClassesSyntax() {

		List<IndexedGeneric<String>> list = new ArrayList<IndexedGeneric<String>>();
		MySQL db = getDataBase();
		db.queryResult("SELECT * from Classes");
		while(db.nextRow()) {
			int id=db.getValueInt("Id");
			String name=db.getValueS("Name");
			String syntax=db.getValueS("SyntaxSQL");
			list.add(new IndexedGeneric<String>(id,name,syntax));
		}

		return list;
	}
	
	
	public int startClassIndex() {
		int d=10;
		while(d<100000000) {
			if(dbMaxImageIndex < d) break;
			else d*=10;
		}
		return d;
	}



}
