/**
 * 
 */
package es.ugr.siar.db;

import java.io.File;
import java.util.Collections;

/**
 * The FileScore is a subclass of File and contains a score.
 * It also implements a method {@link #compareTo(Object)} that permit 
 * to use {@link Collections#sort(java.util.List)}.
 * This class is design to manage the compare test between one image descriptors and 
 * others image descriptors. 
 * 
 * 
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 3 déc. 07
 * @deprecated This class is not anymore used since 18 dec. 2007
 *
 */
public class FileScore extends File  {


	/**     */
	private static final long serialVersionUID = 1L;
	private float score;

	/**
	 * @param pathname
	 */
	public FileScore(String pathname,float score) {
		super(pathname);
		this.score=score;
	}

	public FileScore(File file,float score) {
		super(file.getAbsolutePath());
		this.score = score;
	}

	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}


	public int compareTo(FileScore other) {

		if (other.score > this.score)  return -1; 
		else if(other.score == this.score) return 0; 
		else return 1; 
	} 


}
