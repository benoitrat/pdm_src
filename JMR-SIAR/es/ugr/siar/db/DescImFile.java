/**
 * 
 */
package es.ugr.siar.db;

import java.io.File;

import es.ugr.siar.ip.ImageJMR;
import es.ugr.siar.ip.desc.VisualDescriptor;

/**
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 18 déc. 07
 *
 */
public class DescImFile {
	
	
	private int imID;
	private File imFname;
	private ImageJMR im; 
	
	/**  Visual Descriptors Vector   */
	private VisualDescriptor[] vDescVec = new VisualDescriptor[VisualDescriptor.NOF_DESCRIPTORS];
	
	
	//################################################
	// Constructors
	//################################################
	
	
	public DescImFile(int imID) {
		this.imID=imID;
		this.imFname=null;
		this.im=null;
	}
	
	/**
	 * 
	 */
	public DescImFile(ImageJMR im) {
		this.imID=-1;
		this.imFname=im.getSrcfile();
		this.im=im;
	}
	
	public DescImFile(int imID, ImageJMR im) {
		this.imID=imID;
		this.imFname=im.getSrcfile();
		this.im=im;
	}
	
	public DescImFile(int imID, File imFname) {
		this.imID=imID;
		this.imFname=imFname;
	}
	
	
	//################################################
	// Functions
	//################################################
	
	
	/**
	 * Set a Descriptor in this specific Array.
	 * 
	 * @param desc
	 */
	public void setDescriptor(VisualDescriptor desc) {
		if(desc.isComputed()) {
			int i = desc.getType();
			//Check if this descriptor wasn't set previously
			if(vDescVec[i] == null) {
				this.vDescVec[i] = desc;
			}
			else {
				//If the descriptor exist check we have the same type.
				if(vDescVec[i].getType() == desc.getType()) {
					//Look if it is a higher version and replace it.
					if(desc.getVersion() > vDescVec[i].getVersion()) {
						vDescVec[i] = desc;
					}
				}
			}
		}
	}

	
	/** 
	 * Get a descriptor from the {@link #vDescVec} according to the type sent.
	 * 
	 * @param 	descType	The Type find in {@link VisualDescriptor}
	 * @return				A visual descriptor of type <code>descType</code>
	 */
	public VisualDescriptor getDescriptor(int descType) {
		if(0 <= descType && descType < VisualDescriptor.NOF_DESCRIPTORS )
			return vDescVec[descType];
		else
			return null;
	}

	//################################################
	// Getters & Setters
	//################################################

	/**
	 * @return the im
	 */
	public ImageJMR getIm() {
		return im;
	}

	/**
	 * @param im the im to set
	 */
	public void setIm(ImageJMR im) {
		this.im = im;
	}

	/**
	 * @return the imFname
	 */
	public File getImFname() {
		return imFname;
	}

	/**
	 * @param imFname the imFname to set
	 */
	public void setImFname(File imFname) {
		this.imFname = imFname;
	}

	/**
	 * @return the imID
	 */
	public int getImID() {
		return imID;
	}

	/**
	 * @param imID the imID to set
	 */
	public void setImID(int imID) {
		this.imID = imID;
	}

	/**
	 * @return the vDescVec
	 */
	public VisualDescriptor[] getVisualDescriptorVec() {
		return vDescVec;
	}

	/**
	 * @param vDescVec  the VisualDescriptor Vector to set
	 */
	public void setVisualDescriptorVec(VisualDescriptor[] vDescVec) {
		this.vDescVec = vDescVec;
	}
	
	
	


}
