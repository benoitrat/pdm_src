package my;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;

public class Debug {
	
	public static int count=0;
	public static int maxCount=0;
	public static boolean debugTag=true;

	public static void printTab(Object[] array) {
		if(array.length > 10) {
			System.out.println("NofElement="+array.length+": "); 
		}
		System.out.print(array[0]);
		for(int i=1;i<array.length;i++) {
			System.out.print(","+array[i]);
			if(i%25==0) System.out.println("");
		}
		System.out.println();
	}
	
	public static void printTab(float[] array) {
		Float[] fl=new Float[array.length];
		for(int i=0;i<array.length;i++) {
			fl[i] = new Float(array[i]);
		}
		printTab(fl);
	}
	
	public static void printTab(int[] array) {
		Integer[] fl=new Integer[array.length];
		for(int i=0;i<array.length;i++) {
			fl[i] = new Integer(array[i]);
		}
		printTab(fl);
	}
	
	public static void printTab(double[] array) {
		Double[] fl=new Double[array.length];
		for(int i=0;i<array.length;i++) {
			fl[i] = new Double(array[i]);
		}
		printTab(fl);
	}
	
	public static void print(String str) {
		System.out.print(str);
	}
	
	public static void printCount(String str) {
		if(debugTag) {
			System.out.println(str);
			count++;
			if(count > maxCount) debugTag = false;
		}
	}
	public static void resetCounter() {
		debugTag=true;
		count=0;
	}
	
	public static void printImg(BufferedImage im) {
		Raster imRst = im.getRaster();
		float [] pix = new float[3];

	       System.out.println("Start");
	        for (int x = 0; x < im.getWidth(); x++) {
	        	imRst.getPixel(x, 0,pix);
	        	System.out.print((int)(255*pix[0]/360)+","+(int)(255*pix[1])+","+(int)(255*pix[2])+"\t");
	        	if((x+1)%7==0) System.out.println();
	        }
	}
	
	static public void printMx(int[][] mx) {
		for(int[] row: mx) printTab(row);
	}
	static public void printMx(float[][] mx) {
		for(float[] row: mx) printTab(row);
	}
	static public void printMx(double[][] mx) {
		for(double[] row: mx) printTab(row);
	}
	
	static public void printSmall3DMx(float[][][] Mx3d) {
	
			for (int k = 0; k < Mx3d.length; k++) {
				System.out.println();
				for (int l = 0; l < Mx3d[k].length; l++) {
					System.out.println();
					for (int m = 0; m < Mx3d[k][l].length; m++)
						System.out.print("Mx3d["+k+"]["+l+"]["+m+"]="+Mx3d[k][l][m]+";\t");
				}
			}
	
		}
	
	static public void printSmall3DMx(int[][][] Mx3d) {
		
		for (int k = 0; k < Mx3d.length; k++) {
			System.out.println();
			for (int l = 0; l < Mx3d[k].length; l++) {
				System.out.println();
				for (int m = 0; m < Mx3d[k][l].length; m++)
					System.out.print("Mx3d["+k+"]["+l+"]["+m+"]="+Mx3d[k][l][m]+";\t");
			}
		}

	}

}
