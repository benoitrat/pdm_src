/**
 * 
 */
package my;

/**
 * Template file for my.Config.java. 
 * 
 * Config.java is not included in SVN because it is different for each computer.
 * This template file should be copy, to make the project work. 
 * 
 * 
 * @author  RAT Benoit <br/>
 * (<a href="http://ivrg.epfl.ch" target="about_blank">IVRG-LCAV-EPFL</a> &
 *  <a href="http://decsai.ugr.es/vip" target="about_blank">VIP-DECSAI-UGR</a>)  
 * @version 1.0
 * @since 19 dec. 07
 *
 */
public class ConfigTemplate {

	//Working system and path
	public static final String OS_Slash="\\";
	public static final String browserCmd="firefox";
	public static final String rootDir = "/home/neub/pdm/";
	
	//Image used for default display and openDir
	public static String openImage = rootDir+"db/myphotos/IMG_2541.JPG";
	public static String openDir = rootDir+"db/corel30k/";
	public static String serializedDir=rootDir+"db/myphotos/";
	
	//MySQL Server Information
	public static String dbDefaultName= "all_photographers_now";
	public static String dbServer="192.168.1.222";
	public static String dbUser="pdm";
	public static String dbPassword="bnel7n";
	
	//Training/Test Sets
	public static String dbTrainTestSetPath=rootDir+"db/waap/good/traintest/";
	
	//Directory where the image are
	public static String dbDefaultPath=rootDir+"db/waap/good/800x600/";
	public static String dbThumbsPath=rootDir+"db/waap/good/thumbs/";
	public static String dbThumbsPrefix="TN_";
	public static String dbThumbsSuffix="";
}
